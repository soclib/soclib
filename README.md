#                                  SOCLIB
#   Virtual Platform Prototyping Framework for Multiprocessor System-on-Chip

## Introduction

This gitlab repository is a fork of the original SVN repository in
https://www.soclib.fr/svn/trunk/soclib.

The original documentation of SoCLib is in http://www.soclib.fr/trac/dev. Some
interesting and quite complete documentation is hosted in this website and
readers are encouraged to visit it.

We plan to move progressively some of the documentation in the original website
to this new site, and new documentation about new components or platforms will
be created directly is this Gitlab's repository.


## Installation

 1. **Install [dependencies](#dependencies)**
 2. **Clone this repository**
 3. **Add SoCLib's binaries to the PATH**
 ```bash
 export SOCLIB_HOME=<soclib_path>
 export PATH=${SOCLIB_HOME}/utils/bin:${PATH}
 ```
 4. **Compile SoCLib's utility tools**
```bash
cd utils/src
make
make install  # this command installs SoCLib tools locally.
              # It do not access system's directories)
```
 5. **Create your custom SoCLib's configuration file**

This file allows the user to define the toolchain configuration for its host machine. This configuration consists of: paths to libraries (e.g. SystemC), flags for libraries, path to the GCC compiler and compiler flags.

Your custom SoCLib's configuration file (`soclib.conf`) shall be installed into the [utils/conf](utils/conf) directory.

For convenience, you can find a template file that you can rename and modify in [utils/conf/soclib.conf.template](utils/conf/soclib.conf.template). This template can be used as is (after renaming it into `utils/conf/soclib.conf`) but you need to define the `SYSTEMC_HOME` environment variable which shall point to your local installation of the SystemC library.


## Platforms

Some of the platforms currently implemented in this repository are not anymore
maintained and are possibly broken (either they do not compile nor work). Of
course you can still try to use them, but it is up to you to get it working.

Hereafter you will find a list of the platforms that are being maintained:

 - [32-bits multi-core RISC-V platform](soclib/platform/topcells/caba-rv32-multi): `soclib/platform/topcells/caba-rv32-multi`


## Dependencies

 1. **GCC compiler for the host machine**

   You need to have a working GCC/G++ installation in your host machine. SoCLib is a library of C++ hardware models.

 2. **Python**

   SoCLib implements a Python-based compilation toolchain.

 3. [**SystemC library**](https://www.accellera.org/downloads/standards/systemc)

   Hardware models in SoCLib use the SystemC library.

 4. [**SDL2 library**](https://github.com/libsdl-org/SDL/releases)

   Some simulation platforms require the SDL library to be installed in the host machine to implement a framebuffer.

 5. **Cross-compiler for the guest instruction set architecture**

   SoCLib is a framework to build processor-based simulation platforms.
   You need to have a compilation toolchain for the target processor architecture to produce valid executables files to run on platforms.
