/* -*- c++ -*-
 *
 * SOCLIB_LGPL_HEADER_BEGIN
 * 
 * This file is part of SoCLib, GNU LGPLv2.1.
 * 
 * SoCLib is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; version 2.1 of the License.
 * 
 * SoCLib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with SoCLib; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 * 
 * SOCLIB_LGPL_HEADER_END
 *
 * Copyright (c) CEA, List
 *         Cesar Fuguet <cesar.fuguettortolero@cea.fr>, 2023
 *
 * Maintainers: cfuguet
 */

#include <strings.h>

#include "clint.h"
#include "alloc_elems.h"
#include "register.h"
#include "../include/vci_clint.h"

namespace soclib {
namespace caba {

using namespace soclib;

#define tmpl(t) template<typename vci_param> t VciClint<vci_param>

tmpl(bool)::on_write(int seg, typename vci_param::addr_t addr, typename vci_param::data_t data, int be)
{
    int reg;
    uint32_t lo, hi;
    uint32_t mask;

    for (int i = 0; i < 4; i++) {
        if ((be >> i) & 0x1) mask |= (0xfful << i*8);
    }

    if ((addr >= CLINT_MSIP) && (addr < (CLINT_MSIP + m_cores_count*4))) {
        reg = (int)((addr - CLINT_MSIP) / 4);
        r_msip[reg] = (r_msip[reg] & ~mask) | (data & mask);

#ifdef SOCLIB_MODULE_DEBUG
        std::cout << std::hex;
        std::cout << "on_write(): MSIP[" << reg << "] = " << r_msip[reg] << std::endl;
        std::cout << std::dec;
#endif
        return true;
    }
    if ((addr >= CLINT_MTIMECMP) && (addr < (CLINT_MTIMECMP + m_cores_count*8))) {
        reg = (int)((addr - CLINT_MTIMECMP) / 8);
        if (addr & 0x4) lo = (uint32_t)r_mtimecmp[reg];
        else            lo = ((uint32_t)r_mtimecmp[reg] & ~mask) | (data & mask);
        if (addr & 0x4) hi = (uint32_t)((r_mtimecmp[reg] >> 32) & ~mask) | (data & mask);
        else            hi = (uint32_t)(r_mtimecmp[reg] >> 32);
        r_mtimecmp[reg] = ((uint64_t)hi << 32) | (uint64_t)lo;

#ifdef SOCLIB_MODULE_DEBUG
        std::cout << std::hex;
        std::cout << "on_write(): MTIMECMP[" << reg << "] = " << r_mtimecmp[reg] << std::endl;
        std::cout << std::dec;
#endif
        return true;
    }
    if ((addr >= CLINT_MTIME) && (addr < (CLINT_MTIME + 8))) {
        if (addr & 0x4) lo = (uint32_t)r_mtime;
        else            lo = ((uint32_t)r_mtime & ~mask) | (data & mask);
        if (addr & 0x4) hi = ((uint32_t)(r_mtime >> 32) & ~mask) | (data & mask);
        else            hi = (uint32_t)(r_mtime >> 32);
        r_mtime = ((uint64_t)hi << 32) | (uint64_t)lo;

#ifdef SOCLIB_MODULE_DEBUG
        std::cout << std::hex;
        std::cout << "on_write(): MTIME = " << r_mtime << std::endl;
        std::cout << std::dec;
#endif
        return true;
    }

    return false;
}

tmpl(bool)::on_read(int seg, typename vci_param::addr_t addr, typename vci_param::data_t &data)
{
    data = 0x0badadd4;
    if ((addr >= CLINT_MSIP) && (addr < (CLINT_MSIP + m_cores_count*4))) {
        int reg = (int)((addr - CLINT_MSIP) / 4);
        data = (uint32_t)r_msip[reg];

#ifdef SOCLIB_MODULE_DEBUG
        std::cout << std::hex;
        std::cout << "on_read(): MSIP[" << reg << "] => " << data << std::endl;
        std::cout << std::dec;
#endif
        return true;
    }
    if ((addr >= CLINT_MTIMECMP) && (addr < (CLINT_MTIMECMP + m_cores_count*8))) {
        int reg = (int)((addr - CLINT_MTIMECMP) / 8);
        if (addr & 0x4) data = (uint32_t)(r_mtimecmp[reg] >> 32);
        else            data = (uint32_t)(r_mtimecmp[reg]);

#ifdef SOCLIB_MODULE_DEBUG
        std::cout << std::hex;
        std::cout << "on_read(): MTIMECMP[" << reg << "] => " << data << std::endl;
        std::cout << std::dec;
#endif
        return true;
    }
    if ((addr >= CLINT_MTIME) && (addr < (CLINT_MTIME + 8))) {
        if (addr & 0x4) data = (uint32_t)(r_mtime >> 32);
        else            data = (uint32_t)(r_mtime);

#ifdef SOCLIB_MODULE_DEBUG
        std::cout << std::hex;
        std::cout << "on_read(): MTIME => " << data << std::endl;
        std::cout << std::dec;
#endif
        return true;
    }
    return false;
}

tmpl(void)::print_trace()
{
    std::cout << name() << std::hex << std::endl;
    std::cout << " r_mtime = " << r_mtime.read() << std::endl;
    for (int i = 0; i < m_cores_count; i++) {
        std::cout << " r_msip[" << i << "] = " << r_msip[i].read();
        std::cout << " / r_mtimecmp[" << i << "] = " << r_mtimecmp[i].read();
        std::cout << std::endl;
    }
    std::cout << std::dec;
}

tmpl(void)::transition()
{
    if (!p_resetn.read()) {
        m_vci_fsm.reset();
        r_mtime = 0;
        for (int i = 0; i < m_cores_count; i++) {
            r_msip[i] = 0;
            r_mtimecmp[i] = 0;
        }
        return;
    }

    r_mtime = r_mtime.read() + 1;

    m_vci_fsm.transition();
}

tmpl(void)::genMoore()
{
    m_vci_fsm.genMoore();

    for (int i = 0; i < m_cores_count; i++) {
        //  Set machine software interrupt wire
        p_msip[i] = (r_msip[i].read() == 0x1);
        //  Set machine timer interrupt wire
        p_mtip[i] = (r_mtime.read() >= r_mtimecmp[i].read());
    }
}

tmpl(/**/)::VciClint(
    sc_module_name name,
    const MappingTable &mt,
    const IntTab &index,
    size_t ncores) :
        caba::BaseModule(name),
        m_vci_fsm(p_vci, mt.getSegmentList(index)),
        m_cores_count(ncores),
        r_msip(alloc_elems<sc_signal<uint32_t> >("r_msip" , ncores)),
        r_mtimecmp(alloc_elems<sc_signal<uint64_t> >("r_mtimecmp" , ncores)),
        r_mtime("mtime"),
        p_clk("clk"),
        p_resetn("resetn"),
        p_vci("vci"),
        p_msip(alloc_elems<sc_out<bool> >("p_msip" , ncores)),
        p_mtip(alloc_elems<sc_out<bool> >("p_mtip" , ncores))
{
    std::cout << "  - Building VciClint : " << name << std::endl;

    assert(vci_param::B == 4);

    m_vci_fsm.on_read_write(on_read, on_write);

    SC_METHOD(transition);
    dont_initialize();
    sensitive << p_clk.pos();

    SC_METHOD(genMoore);
    dont_initialize();
    sensitive << p_clk.neg();
}

tmpl(/**/)::~VciClint()
{
    soclib::common::dealloc_elems<sc_core::sc_signal<uint32_t> >(r_msip, m_cores_count);
    soclib::common::dealloc_elems<sc_core::sc_signal<uint64_t> >(r_mtimecmp, m_cores_count);
    soclib::common::dealloc_elems(p_msip, m_cores_count);
    soclib::common::dealloc_elems(p_mtip, m_cores_count);
}


}}

// Local Variables:
// tab-width: 4
// c-basic-offset: 4
// c-file-offsets:((innamespace . 0)(inline-open . 0))
// indent-tabs-mode: nil
// End:

// vim: filetype=cpp:expandtab:shiftwidth=4:tabstop=4:softtabstop=4

