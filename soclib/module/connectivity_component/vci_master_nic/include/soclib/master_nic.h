/*
 * SOCLIB_LGPL_HEADER_BEGIN
 *
 * This file is part of SoCLib, GNU LGPLv2.1.
 *
 * SoCLib is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; version 2.1 of the License.
 *
 * SoCLib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with SoCLib; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 * SOCLIB_LGPL_HEADER_END
 *
 * Copyright (c) UPMC, Lip6, Asim
 *         Alain Greiner <alain.greiner@lip6.fr>, 2012
 *
 * Maintainers: alain
 */

#ifndef MASTER_NIC_REGS_H
#define MASTER_NIC_REGS_H


#define NIC_GLOBAL_SPAN   64       // 256 bytes 
#define NIC_CHANNEL_SPAN  16       // 64 bytes per channel

enum SoclibMasterNicGlobalRegisters 
{
    NIC_G_CHANNELS                   = 1,   // Number of channels             (read only)
    NIC_G_BC_ENABLE                  = 2,   // Enable Broadcast if non zero
    NIC_G_PERIOD                     = 3,   // container status poll period
    NIC_G_MAC_4                      = 4,   // channel mac address 32 LSB bits 
    NIC_G_MAC_2                      = 5,   // channel mac address 16 MSB bits 
    NIC_G_NPKT_RESET                 = 6,   // reset packets counters         (write only)
 
    NIC_G_NPKT_RX_G2S_RECEIVED       = 10,  // number of packets received on GMII RX port
    NIC_G_NPKT_RX_G2S_DISCARDED      = 11,  // number of RX packets discarded by RX_G2S FSM

    NIC_G_NPKT_RX_DES_SUCCESS        = 12,  // number of RX packets transmited by RX_DES FSM
    NIC_G_NPKT_RX_DES_TOO_SMALL      = 13,  // number of discarded too small RX packets (<60B)
    NIC_G_NPKT_RX_DES_TOO_BIG        = 14,  // number of discarded too big RX packets (>1514B)
    NIC_G_NPKT_RX_DES_MFIFO_FULL     = 15,  // number of discarded RX packets if fifo full
    NIC_G_NPKT_RX_DES_CRC_FAIL       = 16,  // number of discarded RX packets if CRC32 failure

    NIC_G_NPKT_RX_DISPATCH_RECEIVED  = 17,  // number of packets received by RX_DISPATCH FSM
    NIC_G_NPKT_RX_DISPATCH_BROADCAST = 18,  // number of broadcast RX packets received
    NIC_G_NPKT_RX_DISPATCH_DST_FAIL  = 19,  // number of discarded RX packets if DST MAC not found
    NIC_G_NPKT_RX_DISPATCH_CH_FULL   = 20,  // number of discarded RX packets if channel full

    NIC_G_NPKT_TX_DISPATCH_RECEIVED  = 41,  // number of packets received by TX_DISPATCH FSM
    NIC_G_NPKT_TX_DISPATCH_TOO_SMALL = 42,  // number of discarded too small TX packets (<60B)
    NIC_G_NPKT_TX_DISPATCH_TOO_BIG   = 43,  // number of discarded too big TX packets (>1514B)
    NIC_G_NPKT_TX_DISPATCH_TRANSMIT  = 44,  // number of transmit TX packets
};


enum SoclibMasterNicChannelRegisters 
{
    NIC_RX_CHANNEL_RUN               = 0,   // RX channel activation          (Write only)
    NIC_RX_CHBUF_DESC_LO             = 1,   // RX chbuf descriptor low word   (Read/Write)
    NIC_RX_CHBUF_DESC_HI             = 2,   // RX chbuf descriptor high word  (Read/Write)
    NIC_RX_CHBUF_NBUFS               = 3,   // RX chbuf depth (buffers)       (Read/Write)
    NIC_RX_CHANNEL_STATE             = 4,   // RX channel status              (Read only)

    NIC_TX_CHANNEL_RUN               = 8,   // TX channel activation          (Write only)
    NIC_TX_CHBUF_DESC_LO             = 9,   // TX chbuf descriptor low word   (Read/Write)
    NIC_TX_CHBUF_DESC_HI             = 10,  // TX chbuf descriptor high word  (Read/Write)
    NIC_TX_CHBUF_NBUFS               = 11,  // TX chbuf depth (buffers)       (Read/Write)
    NIC_TX_CHANNEL_STATE             = 12,  // TX channel status              (Read only)
};


enum SoclibMasterNicStatusValues
{
    MASTER_NIC_CHANNEL_IDLE  = 0,
    MASTER_NIC_CHANNEL_ERROR = 1,
    MASTER_NIC_CHANNEL_BUSY  = 2,     // busy for any value >= 2
};



#endif

// Local Variables:
// tab-width: 4
// c-basic-offset: 4
// c-file-offsets:((innamespace . 0)(inline-open . 0))
// indent-tabs-mode: nil
// End:

// vim: filetype=cpp:expandtab:shiftwidth=4:tabstop=4:softtabstop=4


