/* -*- c++ -*-
 *
 * SOCLIB_LGPL_HEADER_BEGIN
 *
 * This file is part of SoCLib, GNU LGPLv2.1.
 *
 * SoCLib is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; version 2.1 of the License.
 *
 * SoCLib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with SoCLib; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 * SOCLIB_LGPL_HEADER_END
 *
 * Copyright (c) UPMC, Lip6
 *         Alain Greiner <alain.greiner@lip6.fr> July 2008
 *         Clement Devigne <clement.devigne@etu.upmc.fr>
 *         Sylvain Leroy <sylvain.leroy@lip6.fr>
 *         Cassio Fraga <cassio.fraga@lip6.fr>
 *
 * Maintainers: alain
 */

//////////////////////////////////////////////////////////////////////////////////
// File         : gmii_rx.h
// Date         : 01/06/2012
// Authors      : Alain Greiner
//////////////////////////////////////////////////////////////////////////////////
// This class implements a packet receiver, acting as a PHY component,
// and respecting the GMII protocol (one byte per cycle).
// The inter-packet gap is a constructor argument.
// It implements both the NIC_MODE_FILE backend & the NIC_MODE_SYNTHESIS backend,
// depending on the "use_file" constructor parameter.
// - In NIC_MODE_FILE , it reads packets in the file "nic_rx_file.txt", 
//   that must be stored in the same directory as the "top.ccp" file.
// - In NIC_MODE_SYNTHESIS, the packet format is a 42 bytes ETH/IP/UDP header,
//   a random length payload (between 18 and 1038 bytes), and a 4 bytes ETH CRC.
//   The DST MAC address, SRC MAC address, DST IP address, and DST port number
//   are constant values defined below.
//   The SRC IP address and SRC port number are random values.
//   The resulting Ethernet packet length is between 64 and 1084 bytes.
//////////////////////////////////////////////////////////////////////////////////
// It has 2 constructor parameters:
// - uint32_t gap      : number of cycles between packets.
// - bool     use_file : NIC_MODE_FILE when true / NIC_MODE_SYNTHESIS when false.
//////////////////////////////////////////////////////////////////////////////////

#ifndef SOCLIB_CABA_GMII_RX_H
#define SOCLIB_CABA_GMII_RX_H

#include <inttypes.h>
#include <systemc>
#include <assert.h>
#include <string>
#include <cstring>
#include <iostream>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <fstream>

#include "ethernet_crc.h"
#include "nic_rx_backend.h"


#define DST_MAC_5    0x11
#define DST_MAC_4    0x22
#define DST_MAC_3    0x33
#define DST_MAC_2    0x44
#define DST_MAC_1    0x55
#define DST_MAC_0    0x66

#define DST_IP       0x77777777

#define DST_PORT     0x8888

#define SRC_MAC_5    0xAA
#define SRC_MAC_4    0xBB
#define SRC_MAC_3    0xCC
#define SRC_MAC_2    0xDD
#define SRC_MAC_1    0xEE
#define SRC_MAC_0    0xFF

namespace soclib {
namespace caba {

using namespace sc_core;

/////////////////////////////////////
class NicRxGmii : public NicRxBackend
{
    // structure constants
    const std::string       m_name;          // Component name
    const uint32_t	        m_gap;           // inter packets gap (cycles)
    const bool              m_use_file;      // NIC_MODE_FILE when true
    std::ifstream           m_file;          // input file descriptor 
    EthernetChecksum        m_crc;           // checksum computer

    // registers
    bool                    r_fsm_gap;       // inter_packet state when true
    uint32_t                r_counter;       // cycles counter (both gap and plen)
    uint8_t 	            r_buffer[2048];  // local buffer containing one packet
    uint32_t                r_plen;          // Ethernet packet length (in bytes)
    uint32_t                r_pktid;         // packet index                     

    ///////////////////////////////////////////////////////////////////
    // This function is used to convert ascii to hexa.
    ///////////////////////////////////////////////////////////////////

    uint8_t atox (uint8_t el)
    {
        if((el >= 48) and (el <= 57))
            return (el - 48);
        else if ((el >= 65) and (el <= 70))
            return ((el - 65)+10);
        else if((el >= 97) and (el <= 102))
            return ((el-97)+10);
        else
            return -1;
    }

    ///////////////////////////////////////////////////////////////////
    // This function synthetize one packet, in NIC_MODE SYNTHESIS.
    ///////////////////////////////////////////////////////////////////
    virtual void build_one_packet()
    {
        uint32_t n;
        uint32_t payload;
        uint32_t rand = random();

        // define payload length (bytes)
        payload = (rand & 0x3FC) + 18; 

        // define SRC IP and SRC PORT
        uint32_t src_ip   = (rand & 0xC00) >> 10;
        uint32_t src_port = (rand & 0x3000) >> 12;

        // set MAC destination
        r_buffer[0]  = DST_MAC_5;
        r_buffer[1]  = DST_MAC_4;
        r_buffer[2]  = DST_MAC_3;
        r_buffer[3]  = DST_MAC_2;
        r_buffer[4]  = DST_MAC_1;
        r_buffer[5]  = DST_MAC_0;

        // set MAC source
        r_buffer[6]  = SRC_MAC_5; 
        r_buffer[7]  = SRC_MAC_4;
        r_buffer[8]  = SRC_MAC_3;
        r_buffer[9]  = SRC_MAC_2;
        r_buffer[10] = SRC_MAC_1;
        r_buffer[11] = SRC_MAC_0;

        // set ETHER_TYPE = IPV4 frame in ETH header
        r_buffer[12] = 0x8;
        r_buffer[13] = 0x00;

        // set VERS / IHL / DSCP / ECN in IP header
        r_buffer[14] = 0x45;
        r_buffer[15] = 0x00;
  
        // set IP_TOTAL_LENGTH (payload length + 28) in IP header
        r_buffer[16] = (payload + 28) >> 8;
        r_buffer[17] = (payload + 28 );

        // set IDENT in IP header
        r_buffer[18] = 0x00;
        r_buffer[19] = 0x00;

        // set FLAGS and FRAG_OFFSET in IP header
        r_buffer[20] = 0x00;
        r_buffer[21] = 0x00;

        // set TTL and PROTOCOL in IP header
        r_buffer[22] = 0x80;
        r_buffer[23] = 0x11;

        // set SRC_IP address in IP header
        r_buffer[26] = (src_ip >> 24) & 0xFF;
        r_buffer[27] = (src_ip >> 16) & 0xFF;
        r_buffer[28] = (src_ip >> 8 ) & 0xFF;
        r_buffer[29] = (src_ip      ) & 0xFF;

        // set DST_IP address in IP header
        r_buffer[30] = (DST_IP >> 24) & 0xFF;
        r_buffer[31] = (DST_IP >> 16) & 0xFF;
        r_buffer[32] = (DST_IP >> 8 ) & 0xFF;
        r_buffer[33] = (DST_IP      ) & 0xFF;

        // set IP_CHECKSUM in IP header
        uint16_t  * sht    = (uint16_t *)&r_buffer[14];
        uint32_t    ip_sum = (uint32_t)sht[0] +
                             (uint32_t)sht[1] +
                             (uint32_t)sht[2] +
                             (uint32_t)sht[3] +
                             (uint32_t)sht[4] +
                             (uint32_t)sht[5] +
                             (uint32_t)sht[6] +
                             (uint32_t)sht[7] +
                             (uint32_t)sht[8] +
                             (uint32_t)sht[9] ;
        ip_sum = ~((ip_sum & 0xFFFF) + (ip_sum >> 16));

        r_buffer[24] = (ip_sum >> 8) & 0xFF;
        r_buffer[25] = (ip_sum     ) & 0xFF;
   
        // set SRC_PORT in UDP header
        r_buffer[34] = (src_port >> 8) & 0xFF;
        r_buffer[35] = (src_port     ) & 0xFF;

        // set DST_PORT in UDP header
        r_buffer[36] = (DST_PORT >> 8) & 0xFF;
        r_buffer[37] = (DST_PORT     ) & 0xFF;

        // set UDP_TOTAL_LENGTH (payload length + 8) in UDP header
        r_buffer[38] = (payload + 8) >> 8;
        r_buffer[39] = (payload + 8);

        // set optionnal UDP_CHECKSUM in UDP header
        r_buffer[40] = 0x00;
        r_buffer[41] = 0x00;

        // store pktid in first 4 bytes of payload
        r_buffer[42] = (r_pktid >> 24) & 0xFF;
        r_buffer[43] = (r_pktid >> 16) & 0xFF;
        r_buffer[44] = (r_pktid >> 8 ) & 0xFF;
        r_buffer[45] = (r_pktid      ) & 0xFF;

        // store 0, 1, 2, 3 ... in next (payload-4) bytes
        for ( n = 0 ; n < (payload - 4) ; n++ ) 
        {
            r_buffer[46 + n] = n & 0xFF;
        }

        // compute ETH_CHEKSUM
        uint32_t checksum = 0;
        for ( n = 0 ; n < (42 + payload) ; n++ )
        {
            checksum = m_crc.update( checksum , (uint32_t)r_buffer[n] );
        }

        // set ETH_CHECKSUM
        r_buffer[payload + 42] = (checksum & 0xFF000000) >> 24;
        r_buffer[payload + 43] = (checksum & 0x00FF0000) >> 16;
        r_buffer[payload + 44] = (checksum & 0x0000FF00) >> 8;
        r_buffer[payload + 45] = (checksum & 0x000000FF);

        // return Ethernet packet length
        r_plen = payload + 46;

        // increment packet index
        r_pktid = r_pktid + 1;
    }

    ////////////////////////////////////////////////////////////////////////
    // This function is used to read one packet from the input file
    // in NIC_MODE_FILE. It updates the r_buffer and the r_plen variables.
    ////////////////////////////////////////////////////////////////////////
    virtual void read_one_packet()
    {
        uint32_t      data = 0;
        std::string   string;

        // string contains one packet and r_plen contains number of bytes
        m_file >> r_plen >> string;

        // check end of file and restart it
        if (m_file.eof())
        {
            m_file.clear();
            m_file.seekg(0, std::ios::beg);
            m_file >> r_plen >> string;
        }

        // Preamble consumption (at least 0xD5 as preamble is mandatory)
        size_t preamb_cpt = 0;  //counts bytes on the preamble

        while (atox(string[2*preamb_cpt])==0x5 and atox(string[2*preamb_cpt+1])==0x5)
        {
            preamb_cpt++;
        }
        // SFD consumption
        if (atox(string[2*preamb_cpt])==0xD and atox(string[2*preamb_cpt+1])==0x5)
        {
            preamb_cpt++;
        }
        else
        {
            // Packet without SFD
            std::cout << "RX_GMII ERROR: must contain SFD byte 0xD5" << std::endl;
            exit(0);
        }

        // convert two hexadecimal characters into one uint8_t
        for (size_t n = 0; n < (r_plen << 1) ; n++)
        {
            data = (data << 4)| atox(string[n+2*preamb_cpt]);
            if (n%2)
            {
                r_buffer[n>>1] = data;
                data = 0;
            }
        }
    } // end read_one packet()

public:

    ////////////////////
    virtual void reset()
    {
        r_fsm_gap   = true;
        r_counter   = m_gap;
        r_pktid     = 0;
        memset( r_buffer, 0, 2048 );
    }

    ///////////////////////////////////////////////////////////////////
    // To reach the 1 Gbyte/s throughput, this method must be called
    // by the NIC component at all cycles of a 125MHz clock.
    // It is therefore written as a transition and contains a
    // two states FSM to introduce the inter-packet waiting cycles.
    ///////////////////////////////////////////////////////////////////
    virtual void get( bool    * dv,         // data valid
                      bool    * er,         // data error
                      uint8_t * dt  )       // data value
    {
        if ( r_fsm_gap )    // inter-packet state
        {
            *dv = false;
            *er = false;
            *dt = 0;

            r_counter = r_counter - 1;

            if (r_counter == 0 ) // end of gap
            {
                r_fsm_gap = false;
                if ( m_use_file ) read_one_packet();
                else              build_one_packet();
            }
        }
        else    // running packet state
        {
            *dv = true;
            *er = false;
            *dt = r_buffer[r_counter];

            r_counter = r_counter + 1;

            if ( r_counter == r_plen ) // end of packet
            {
                r_counter   = m_gap;
                r_fsm_gap   = true;
            }
        }
    } // end get()


    ////////////////////////////////////
    //  constructor
    /////////////////////////////////////
    NicRxGmii( uint32_t           gap,
               bool               use_file )

        : m_gap( gap ),
          m_use_file( use_file ),
          m_file( "nic_rx_file.txt" ) 
    {
        if ( use_file and (m_file.is_open() == false) )
        {
            std::cout << "[NIC ERROR] in NicRxGmii : cannot open file nic_rx_file.txt"
                      << std::endl;
            exit(0);
        }
    } 

    //////////////////
    // destructor
    //////////////////
    virtual ~NicRxGmii()
    {
        m_file.close();
    }

}; // end NicRxGmii

}}

#endif

// Local Variables:
// tab-width: 4
// c-basic-offset: 4
// c-file-offsets:((innamespace . 0)(inline-open . 0))
// indent-tabs-mode: nil
// End:

// vim: filetype=cpp:expandtab:shiftwidth=4:tabstop=4:softtabstop=4



