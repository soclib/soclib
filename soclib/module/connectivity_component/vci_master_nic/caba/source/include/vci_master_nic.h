/* -*- c++ -*-
 *
 * SOCLIB_LGPL_HEADER_BEGIN
 *
 * This file is part of SoCLib, GNU LGPLv2.1.
 *
 * SoCLib is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; version 2.1 of the License.
 *
 * SoCLib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with SoCLib; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 * SOCLIB_LGPL_HEADER_END
 *
 * Copyright (c) UPMC, Lip6, Asim
 *         alain.greiner@lip6.fr
 *         Clement Devigne <clement.devigne@etu.upmc.fr>
 *         Sylvain Leroy <sylvain.leroy@lip6.fr>
 *         Cassio Fraga <cassio.fraga@lip6.fr>
 *
 * Maintainers: alain
 */

#ifndef SOCLIB_VCI_MASTER_NIC_H
#define SOCLIB_VCI_MASTER_NIC_H

#include <stdint.h>
#include <systemc>
#include "vci_target.h"
#include "vci_initiator.h"
#include "caba_base_module.h"
#include "mapping_table.h"
#include "fifo_multi_buffer.h"
#include "nic_rx_chbuf.h"
#include "nic_tx_chbuf.h"
#include "nic_rx_backend.h"
#include "nic_tx_backend.h"
#include "nic_rx_gmii.h"
#include "nic_tx_gmii.h"
#include "nic_rx_tap.h"
#include "nic_tx_tap.h"
#include "generic_fifo.h"
#include "ethernet_crc.h"

namespace soclib {
namespace caba {

using namespace sc_core;

template<typename vci_param>
class VciMasterNic : public caba::BaseModule
{
private:

    // methods
    void        transition();
    void        genMoore();
    uint32_t    read_global_register(uint32_t addr);
    uint32_t    read_channel_register(uint32_t addr);

#if !defined(__APPLE__) || !defined(__MACH__)
    int32_t     open_tap_fd();
#endif

    // Instrumentation registers
    sc_signal<uint32_t>   r_total_cycles;                 /*!< cycle counter                 */
    sc_signal<uint32_t>   r_total_len_rx_gmii;            /*!< Bytes receive by RX_GMII      */
    sc_signal<uint32_t>   r_total_len_rx_chan;            /*!< Bytes receive by RX_CHAN      */
    sc_signal<uint32_t>   r_total_len_tx_chan;            /*!< Bytes send    by TX_CHAN      */
    sc_signal<uint32_t>   r_total_len_tx_gmii;            /*!< Bytes send    by TX_GMII      */

    // Global registers
    sc_signal<bool>       r_global_bc_enable;             /*!< broadcast enabled             */
    sc_signal<uint32_t>   r_global_channels;              /*!< number of channels            */
    sc_signal<uint32_t>   r_global_period;                /*!< chbuf status polling period   */
    sc_signal<uint32_t>   r_global_mac_4;                 /*!< MAC address (32 LSB bits)     */
    sc_signal<uint32_t>   r_global_mac_2;                 /*!< MAC address (16 MSB bits)     */

    // VCI TGT FSM registers
    sc_signal<int>			               r_tgt_fsm;
    sc_signal<typename vci_param::srcid_t> r_tgt_srcid;   /*!< for rsrcid                    */
    sc_signal<typename vci_param::trdid_t> r_tgt_trdid;   /*!< for rtrdid                    */
    sc_signal<typename vci_param::pktid_t> r_tgt_pktid;   /*!< for rpktid                    */
    sc_signal<typename vci_param::data_t>  r_tgt_wdata;   /*!< for write burst               */
    sc_signal<typename vci_param::be_t>	   r_tgt_be;      /*!< for write burst               */
    sc_signal<uint32_t>                    r_tgt_address; /*!< vci_address                   */

    // VCI CMD registers
    sc_signal<int>        r_cmd_fsm;
    sc_signal<size_t>     r_cmd_bytes;	                 /*!< bytes counter in a burst       */
    sc_signal<uint64_t>	  r_cmd_address;	             /*!< VCI address                    */
    sc_signal<int>        r_cmd_type;                    /*!< command type                   */
    sc_signal<uint32_t>   r_cmd_channel;                 /*!< channel index                  */
    sc_signal<uint32_t>   r_cmd_burst;                   /*!< burst index                    */
    sc_signal<bool>       r_cmd_is_rx;                   /*!< RX queue access                */ 

    // VCI RSP registers
    sc_signal<int>        r_rsp_fsm;
    sc_signal<size_t>     r_rsp_bytes;	                 /*!< bytes counter in a burst       */
    sc_signal<uint32_t>   r_rsp_channel;	             /*!< channel index                  */
    sc_signal<uint32_t>   r_rsp_burst;                   /*!< burst index                    */
    sc_signal<bool>       r_rsp_is_rx;                   /*!< RX queue access                */ 

    // RX_G2S FSM registers
    sc_signal<int>        r_rx_g2s_fsm;
    sc_signal<uint32_t>   r_rx_g2s_checksum;              /*!< packet checksum               */
    sc_signal<uint8_t>    r_rx_g2s_dt0;                   /*!< local data buffer             */
    sc_signal<uint8_t>    r_rx_g2s_dt1;                   /*!< local data buffer             */
    sc_signal<uint8_t>    r_rx_g2s_dt2;                   /*!< local data buffer             */
    sc_signal<uint8_t>    r_rx_g2s_dt3;                   /*!< local data buffer             */
    sc_signal<uint8_t>    r_rx_g2s_dt4;                   /*!< local data buffer             */
    sc_signal<uint8_t>    r_rx_g2s_dt5;                   /*!< local data buffer             */
    sc_signal<size_t>     r_rx_g2s_delay;                 /*!< delay cycle counter           */

    sc_signal<uint32_t>   r_rx_g2s_npkt_received;         /*!< number of received packets    */
    sc_signal<uint32_t>   r_rx_g2s_npkt_discarded;        /*!< number of discarded packets   */

    // RX_DES FSM registers
    sc_signal<int>        r_rx_des_fsm;
    sc_signal<uint32_t>   r_rx_des_counter_bytes;         /*!< nb bytes in one packet        */
    sc_signal<uint32_t>   r_rx_des_padding;               /*!< padding                       */
    sc_signal<uint8_t>*   r_rx_des_data;                  /*!< array[4] bytes                */

    sc_signal<uint32_t>   r_rx_des_npkt_success;          /*!< transmit packets              */
    sc_signal<uint32_t>   r_rx_des_npkt_too_small;        /*!< discarded packets             */
    sc_signal<uint32_t>   r_rx_des_npkt_too_big;          /*!< discarded packets             */
    sc_signal<uint32_t>   r_rx_des_npkt_mfifo_full;       /*!< discarded packets             */
    sc_signal<uint32_t>   r_rx_des_npkt_cs_fail;          /*!< discarded packets             */

    // RX_DISPATCH registers
    sc_signal<int>        r_rx_dispatch_fsm;
    sc_signal<uint32_t>*  r_rx_dispatch_word;             /*!< array[7] words                */
    sc_signal<uint32_t>   r_rx_dispatch_index;            /*!< current word index            */
    sc_signal<uint32_t>   r_rx_dispatch_nbytes;           /*!< number of bytes to be written */
    sc_signal<uint32_t>   r_rx_dispatch_dest;             /*!< bit vector: 1 bit per channel */
    sc_signal<uint32_t>   r_rx_dispatch_channel;          /*!< selected channel index        */ 
    sc_signal<uint32_t>   r_rx_dispatch_plen;             /*!< number of bytes in packet     */

    sc_signal<uint32_t>   r_rx_dispatch_npkt_received;    /*!< received packets              */
    sc_signal<uint32_t>   r_rx_dispatch_npkt_broadcast;   /*!< broadcast packets             */
    sc_signal<uint32_t>   r_rx_dispatch_npkt_dst_fail;    /*!< discarded packets             */
    sc_signal<uint32_t>   r_rx_dispatch_npkt_full;        /*!< discarded packets             */

    // RX_CMA registers
    sc_signal<int>        r_rx_cma_fsm[4];
    sc_signal<bool>       r_rx_cma_run[4];               /*!< RX_CMA engine activated        */
    sc_signal<uint32_t>   r_rx_cma_src_index[4];         /*!< internal container index       */
    sc_signal<uint64_t>   r_rx_cma_dst_desc[4];          /*!< chbuf descriptor paddr         */
    sc_signal<uint32_t>   r_rx_cma_dst_nbufs[4];         /*!< number of containers in chbuf  */
    sc_signal<uint32_t>   r_rx_cma_dst_index[4];         /*!< DST container index            */
    sc_signal<uint32_t>   r_rx_cma_dst_buf[4];           /*!< DST container 32 LSB address   */
    sc_signal<uint32_t>   r_rx_cma_dst_sts[4];           /*!< DST status 32 LSB address      */
    sc_signal<uint32_t>   r_rx_cma_dst_ext[4];           /*!< common address extension       */
    sc_signal<bool>       r_rx_cma_dst_full[4];          /*!< DST container status value     */

    sc_signal<uint32_t>   r_rx_cma_timer[4];             /*!< cycle counter for polling      */
    sc_signal<uint32_t>   r_rx_cma_bytes_count[4];       /*!< number of bytes transfered     */
    sc_signal<uint32_t>   r_rx_cma_burst_count[4];       /*!< pipelined bursts counter       */
    sc_signal<bool>       r_rx_cma_data_error[4];        /*!< data error reported            */
 
    sc_signal<bool>       r_rx_cma_vci_req[4][4];        /*!< valid request to CMD FSM       */
    sc_signal<int> 	      r_rx_cma_vci_req_type[4][4];   /*!< request type  to CMD FSM       */

    sc_signal<bool>       r_rx_cma_vci_rsp[4][4];        /*!< valid response from RSP FSM    */
    sc_signal<bool>       r_rx_cma_vci_rsp_err[4][4];    /*!< response reports an error      */
 
    sc_signal<bool>       r_rx_irq[4];                   /*!< RX channel IRQ set             */

    // TX_CMA registers
    sc_signal<int>        r_tx_cma_fsm[4];
    sc_signal<bool>       r_tx_cma_run[4];               /*!< TX_CMA engine activated        */
    sc_signal<uint32_t>   r_tx_cma_dst_index[4];         /*!< internal container index       */
    sc_signal<uint64_t>   r_tx_cma_src_desc[4];          /*!< chbuf descriptor paddr         */
    sc_signal<uint32_t>   r_tx_cma_src_nbufs[4];         /*!< number of containers in chbuf  */
    sc_signal<uint32_t>   r_tx_cma_src_index[4];         /*!< SRC container index            */
    sc_signal<uint32_t>   r_tx_cma_src_buf[4];           /*!< SRC container 32 LSB address   */
    sc_signal<uint32_t>   r_tx_cma_src_sts[4];           /*!< SRC status 32 LSB address      */
    sc_signal<uint32_t>   r_tx_cma_src_ext[4];           /*!< common address extension       */
    sc_signal<bool>       r_tx_cma_src_full[4];          /*!< SRC container status value     */

    sc_signal<uint32_t>   r_tx_cma_timer[4];             /*!< cycle counter for polling      */
    sc_signal<uint32_t>   r_tx_cma_bytes_count[4];       /*!< number of bytes transfered     */
    sc_signal<uint32_t>   r_tx_cma_burst_count[4];       /*!< pipelined bursts counter       */
    sc_signal<bool>       r_tx_cma_data_error[4];        /*!< data error reported            */

    sc_signal<bool>       r_tx_cma_vci_req[4][4];        /*!< valid request to CMD FSM       */
    sc_signal<int> 	      r_tx_cma_vci_req_type[4][4];   /*!< request type  to CMD FSM       */

    sc_signal<bool>       r_tx_cma_vci_rsp[4][4];        /*!< valid response from RSP FSM    */
    sc_signal<bool>       r_tx_cma_vci_rsp_err[4][4];    /*!< response reports an error      */
 
    sc_signal<bool>       r_tx_irq[4];                   /*!< RX channel IRQ set             */

    // TX_DISPATCH registers
    sc_signal<int>        r_tx_dispatch_fsm;
    sc_signal<size_t>     r_tx_dispatch_channel;         /*!< selected channel index         */
    sc_signal<uint32_t>   r_tx_dispatch_word;            /*!< first word value               */
    sc_signal<uint32_t>   r_tx_dispatch_packets;         /*!< number of packets to send      */
    sc_signal<uint32_t>   r_tx_dispatch_nwords;          /*!< words counter in packet        */
    sc_signal<uint32_t>   r_tx_dispatch_bytes;           /*!< bytes in last word             */

    sc_signal<uint32_t>   r_tx_dispatch_npkt_received;   /*!< received packets               */
    sc_signal<uint32_t>   r_tx_dispatch_npkt_too_small;  /*!< discarded packets              */
    sc_signal<uint32_t>   r_tx_dispatch_npkt_too_big;    /*!< discarded packets              */
    sc_signal<uint32_t>   r_tx_dispatch_npkt_transmit;   /*!< transmit packets               */

    // TX_SER registers
    sc_signal<int>        r_tx_ser_fsm;
    sc_signal<uint32_t>   r_tx_ser_words;                /*!< number of words                */
    sc_signal<uint32_t>   r_tx_ser_bytes;                /*!< bytes in last word             */
    sc_signal<bool>       r_tx_ser_first;                /*!< first word                     */
    sc_signal<uint32_t>   r_tx_ser_ifg;                  /*!< inter-frame-gap counter        */
    sc_signal<uint32_t>   r_tx_ser_data;                 /*!< 32 bits buffer                 */


    // TX_S2G registers
    sc_signal<int>        r_tx_s2g_fsm;
    sc_signal<uint32_t>   r_tx_s2g_checksum;             /*!< packet checksum                */
    sc_signal<uint8_t>    r_tx_s2g_data;                 /*!< local data buffer              */
    sc_signal<size_t>     r_tx_s2g_index;                /*!< checksum byte index            */

    // channels
    NicRxChbuf          * r_rx_chbuf[4];                 /*!< embedded 2 slots RX chbuf      */
    NicTxChbuf          * r_tx_chbuf[4];                 /*!< embedded 2 slots TX chbuf      */

    // fifos
    GenericFifo<uint16_t> r_rx_fifo_stream;
    GenericFifo<uint16_t> r_tx_fifo_stream;

    FifoMultiBuffer       r_rx_fifo_multi;
    FifoMultiBuffer       r_tx_fifo_multi;

    // Packets in and out
    NicRxBackend*         r_backend_rx;
    NicTxBackend*         r_backend_tx;

#if !defined(__APPLE__) || !defined(__MACH__)
    // For TAP backend
    struct ifreq          m_tap_ifr;
#endif


    // sructural parameters
    std::list<soclib::common::Segment>  m_seglist;          /*!< allocated segment(s) */
    const uint32_t                      m_rx_srcid;         /*!< VCI SRCID for RX */ 
    const uint32_t                      m_tx_srcid;         /*!< VCI SRCID for TX */ 
    const uint32_t				        m_channels;		    /*!< no more than 8 channels */
    const uint32_t                      m_burst_length;     /*!< number of bytes */
    const uint32_t                      m_default_mac_4;    /*!< MAC address 32 LSB bits */
    const uint32_t                      m_default_mac_2;    /*!< MAC address 16 MSB bits */
    const uint32_t                      m_gap;              /*!< Inter frame gap */
    EthernetChecksum                    m_crc;              /*!< Ethernet checksum computer */

protected:

    SC_HAS_PROCESS(VciMasterNic);

public:

    enum vci_cmd_type_e 
    {
        REQ_READ_DESC,
        REQ_READ_STATUS,
        REQ_READ_DATA,
        REQ_WRITE_DATA,
        REQ_WRITE_STATUS,
    };

    enum vci_tgt_fsm_state_e 
    {
        TGT_IDLE,
        TGT_WRITE_GLOBAL_REG,
        TGT_WRITE_CHANNEL_REG,
        TGT_READ_GLOBAL_REG,
        TGT_READ_CHANNEL_REG,
        TGT_ERROR,
    };

    enum cmd_fsm_state_e
    {
        CMD_IDLE,
        CMD_READ,
        CMD_WRITE_STATUS,
        CMD_WRITE_DATA,
    };

    enum rsp_fsm_state_e
    {
        RSP_IDLE,
        RSP_READ_DESC,
        RSP_READ_STATUS,
        RSP_READ_DATA,
        RSP_WRITE,
    };

    enum rx_g2s_fsm_state_e 
    {
        RX_G2S_IDLE,
        RX_G2S_DELAY,
        RX_G2S_LOAD,
        RX_G2S_SOS,
        RX_G2S_LOOP,
        RX_G2S_END,
        RX_G2S_ERR,
        RX_G2S_FAIL,
    };

    enum rx_des_fsm_state_e 
    {
    	RX_DES_IDLE,
	    RX_DES_READ_1,
	    RX_DES_READ_2,
	    RX_DES_READ_3,
	    RX_DES_READ_WRITE_0,
	    RX_DES_READ_WRITE_1,
	    RX_DES_READ_WRITE_2,
	    RX_DES_READ_WRITE_3,
	    RX_DES_WRITE_LAST,
	    RX_DES_WRITE_CLEAR,
    };

    enum rx_dispatch_fsm_state_e 
    {
        RX_DISPATCH_IDLE,
        RX_DISPATCH_GET_PLEN,
        RX_DISPATCH_READ_HEADER,
        RX_DISPATCH_SELECT,
        RX_DISPATCH_PACKET_SKIP,
        RX_DISPATCH_CLOSE_CONT,
        RX_DISPATCH_WRITE_HEADER,
        RX_DISPATCH_READ_WRITE,
        RX_DISPATCH_WRITE_LAST,
    };

    enum rx_cma_fsm_state_e
    {
        RX_CMA_IDLE,
        RX_CMA_ERROR,
        RX_CMA_READ_DESC,
        RX_CMA_READ_DESC_WAIT,
        RX_CMA_READ_STATUS,
        RX_CMA_READ_STATUS_WAIT,
        RX_CMA_READ_STATUS_DELAY,
        RX_CMA_MOVE_DATA,
        RX_CMA_MOVE_DATA_WAIT,
        RX_CMA_MOVE_DATA_END, 
        RX_CMA_WRITE_STATUS,
        RX_CMA_WRITE_STATUS_WAIT,
        RX_CMA_NEXT,
    };

    enum tx_cma_fsm_state_e
    {
        TX_CMA_IDLE,
        TX_CMA_ERROR,
        TX_CMA_READ_DESC,
        TX_CMA_READ_DESC_WAIT,
        TX_CMA_READ_STATUS,
        TX_CMA_READ_STATUS_WAIT,
        TX_CMA_READ_STATUS_DELAY,
        TX_CMA_MOVE_DATA,
        TX_CMA_MOVE_DATA_WAIT,
        TX_CMA_MOVE_DATA_END, 
        TX_CMA_WRITE_STATUS,
        TX_CMA_WRITE_STATUS_WAIT,
        TX_CMA_NEXT,
    };

    enum tx_dispatch_fsm_state_e 
    {
        TX_DISPATCH_IDLE,
        TX_DISPATCH_GET_NPKT,
        TX_DISPATCH_GET_PLEN,
        TX_DISPATCH_SKIP_PKT,
        TX_DISPATCH_READ_FIRST,
        TX_DISPATCH_READ_WRITE,
        TX_DISPATCH_WRITE_LAST,
        TX_DISPATCH_RELEASE_CONT,
    };

    enum tx_ser_fsm_state_e 
    {
        TX_SER_IDLE,
        TX_SER_READ_FIRST,
        TX_SER_WRITE_B0,
        TX_SER_WRITE_B1,
        TX_SER_WRITE_B2,
        TX_SER_READ_WRITE,
        TX_SER_GAP,
    };

    enum tx_s2g_fsm_state_e {
        TX_S2G_IDLE,
        TX_S2G_WRITE_DATA,
        TX_S2G_WRITE_LAST_DATA,
        TX_S2G_WRITE_CS,
    };

    enum stream_type_e
    {
        STREAM_TYPE_SOS,     /*!< start of stream */
        STREAM_TYPE_EOS,     /*!< end of stream */
        STREAM_TYPE_ERR,     /*!< corrupted end of stream */
        STREAM_TYPE_NEV,     /*!< no special event */
    };

    enum nic_operation_modes 
    {
        NIC_MODE_FILE      = 0,
        NIC_MODE_SYNTHESIS = 1,
        NIC_MODE_TAP       = 2,
    };


    // ports
    sc_in<bool> 				            p_clk;
    sc_in<bool> 				            p_resetn;
    soclib::caba::VciTarget<vci_param> 		p_vci_tgt;
    soclib::caba::VciInitiator<vci_param>   p_vci_ini;
    sc_out<bool>                          * p_rx_irq;
    sc_out<bool>                          * p_tx_irq;

    uint32_t get_total_len_rx_gmii();
    uint32_t get_total_len_rx_chan();
    uint32_t get_total_len_tx_chan();
    uint32_t get_total_len_tx_gmii();

    /*!
     * \brief Debugging function printing trace from the whole VciMasterNic component
     *
     * \param mode to select the type/level of debug wished
     */
    void print_trace(uint32_t mode = 1);

    /*!
     * \brief Constructor for a VciMasterNic Ethernet controller
     *
     * \param name          Name of that instance of the class
     * \param mt            Mapping Table
     * \param rx_srcid      VCI SRCID for RX transactions 
     * \param rx_srcid      VCI SRCID for TX transactions 
     * \param tgtid         VCI TGTID for segment checking 
     * \param channels      Number of channels (no more than 4)
     * \param burst_length  Number of bytes in a DMA burst (power of 2 no larger than 64)
     * \param mac_4         32 LSB part of the Ethernet MAC address 
     * \param mac_2         16 MSB part of the Ethernet MAC address
     * \param mode          Back-end type : MODE_FILE / MODE_SYNTHESIS / MODE_TAP
     * \param gap           Delay between two packets (not smaller than 12 cycles)
     */
    VciMasterNic( sc_core::sc_module_name 		        name,
                  const soclib::common::MappingTable    &mt,
                  const soclib::common::IntTab          &rx_srcid,
                  const soclib::common::IntTab          &tx_srcid,
                  const soclib::common::IntTab          &tgtid,
                  const size_t 				            channels,
                  const uint32_t                        burst_length,
                  const uint32_t                        mac_4,
                  const uint32_t                        mac_2,
                  const int                             mode,
                  const uint32_t                        gap = 12 );

    ~VciMasterNic();

};

} // end namespace caba
} // end namespace soclib

#endif

// Local Variables:
// tab-width: 4
// c-basic-offset: 4
// c-file-offsets:((innamespace . 0)(inline-open . 0))
// indent-tabs-mode: nil
// End:

// vim: filetype=cpp:expandtab:shiftwidth=4:tabstop=4:softtabstop=4

