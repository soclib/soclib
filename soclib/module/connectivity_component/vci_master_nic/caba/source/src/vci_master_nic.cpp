/* -*- c++ -*-
 *
 * SOCLIB_LGPL_HEADER_BEGIN
 *
 * This file is part of SoCLib, GNU LGPLv2.1.
 *
 * SoCLib is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; version 2.1 of the License.
 *
 * SoCLib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with SoCLib; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 * SOCLIB_LGPL_HEADER_END
 *
 * Copyright (c) UPMC, Lip6
 *         Alain Greiner <alain.greiner@lip6.fr> Juin 2012
 */

//////////////////////////////////////////////////////////////////////////////////
// This component is a multi-channels, GMII compliant, NIC controller,
// with a built-in DMA capability.
//
// If the system clock frequency is larger or equal to the GMII clock
// frequency (ie 125 MHz), it can support a throughput of 1 Gigabit/s).
//
// To improve the throughput, this component supports up to 4 channels,
// indexed by the source IP address for the received (RX) packets, 
// and indexed by the destination IP address for the sent (TX) packets. 
// The actual number of channels is an hardware parameter.
// 
// Regarding the physical interface, this simulation model supports three modes
// of operation, defined by a constructor parameter:
// - NIC_MODE_FILE: Both the RX packets stream an the TX packets stream are 
//   read an writtent from / to dedicated files "nic_rx_file.txt" and 
//   "nic_tx_dile.txt", stored in teh same directory as the top.cpp file.
// - NIC_MODE_SYNTHESIS: The TX packet stream is still the "nic_tx_file.txt",
//   but the RX packet stream is synththised with pseudo-random packet
//   length (between 64 and 1538 bytes), and a pseudo random source  
//   MAC addresses (8 possible values).
// - NIC_MODE_TAP: The TX and RX packet streams are send and received to
//   and from the physical network controller of the workstation
//   running the simulation.
//
// The packet length can have any value, between 64 to 1542 bytes.
//
// The received packets (RX) and the sent packets (TX) are stored in 
// two memory mapped software FIFOs, implemented as chained buffers.
// Each slot in these FIFOs is a 4 Kbytes container. The number of containers,
// defining the queue depth, is a software defined parameter.
// The data transfer unit between software and the NIC is a 4K bytes container,
// containing an integer number of variable size packets.
// The max number of packets in a container is 66 packets.
// The first 34 words of a container are the container header :
//
//     word0  	| 	NB_WORDS	|	NB_PACKETS	|
//     word1	|	PLEN[0]		|	PLEN[1]		|
//      ...	    |	.......		|	........	|
//     word33	|	PLEN[64]	|	PLEN[65]	|
//
//  - NB_PACKETS is the actual number of packets in the container.
//	- NB_WORDS   is the number of useful words in the container.
//	- PLEN[i]    is the number of bytes for the packet[i].
//
// The packets are stored in the (1024 - 34) following words,
// and the packets are word-aligned.
//
// A container has only two states (full or empty), and the memory mapped
// container status is implemented as a SET/RESET synchronisation variable. 
//
// Each channel contains two internal RX containers and two internal
// TX containers, organised as 2 slots chained buffers.
// For each channel, a build-in RX_DMA engine moves the RX containers from
// the internal 2 slots chbuf to the external chbuf implementing the RX queue.
// Another build-in TX-DMA engine moves the TX containers from the external
// chbuf implementing the TX queue, to the internal TX 2 slots chbuf.
//
// To access both the containers state, and the data contained in the containers,
// these DMA engines use two physical addresses, that are packed in a 64 bits 
// "container descriptor". 
// - desc[25:0]  contain bits[31:6] of the "full" variable physical address.
// - desc[51:26] contain bits[31:6] of the "buffer" physical address.
// - desc[63:52] contain the common 12 physical address extension bits.
//
// For the DMA engines, a chbuf global state is described by a circular array 
// of containers descriptors containing the physical addresses of the
// "full[i]" and "cont[i]" variables. 
//
// To improve the throughput for one specific channel, The DMA engines use
// "pipelined bursts" : The burst length cannot be larger than 64 bytes, but each
// channel send 4 pipelined VCI transactions to mask the round-trip latency.
// Therefore, thi NIC controller can send up to 32 parallel VCI transactions
// (4 channels * 4 bursts * 2 directions).
// The CMD/RSP matching uses both the VCI TRDID and PKTID fields:
// - channel index is sent in TRDID[3:2]
// - burst   index is sent in TRDID[1:0]
// - is_rx   bit   is sent in SRCID     
///////////////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <ethernet_crc.h>
#include <cassert>
#include <cstdio>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <arpa/inet.h>

#include "alloc_elems.h"
#include "../include/vci_master_nic.h"
#include "../../../include/soclib/master_nic.h"

namespace soclib {
namespace caba {


////////////////////////////////////////////////////////////////
//   Hidden hardware parameters
////////////////////////////////////////////////////////////////

#define RX_TIMEOUT          100000
#define CMA_POLL_PERIOD     1000

////////////////////////////////////////////////////////////////
//   Detailed debug parameters
////////////////////////////////////////////////////////////////

#define RX_G2S_DEBUG        0
#define RX_DES_DEBUG        0
#define RX_DISPATCH_DEBUG   0
#define RX_CMA_DEBUG        0

#define TX_CMA_DEBUG        0
#define TX_DISPATCH_DEBUG   0
#define TX_SER_DEBUG        0
#define TX_S2G_DEBUG        0

////////////////////////////////////////////////////////////////
//  Macros
////////////////////////////////////////////////////////////////

#define tmpl(t) template<typename vci_param> t VciMasterNic<vci_param>

#define MY_HTONL( x ) (((x >> 24) & 0x000000FF) + \
                       ((x >> 8 ) & 0x0000FF00) + \
                       ((x << 8 ) & 0x00FF0000) + \
                       ((x << 24) & 0xFF000000))

////////////////////////////////////////////////////////////////
// return total number Bytes count in rx_gmii
////////////////////////////////////////////////////////////////
tmpl(uint32_t)::get_total_len_rx_gmii()
{
    return r_total_len_rx_gmii.read();
}

////////////////////////////////////////////////////////////////
// return total number Bytes successfully writen in rx_chbuf
////////////////////////////////////////////////////////////////
tmpl(uint32_t)::get_total_len_rx_chan()
{
    return r_total_len_rx_chan.read();
}

////////////////////////////////////////////////////////////////
// return total number Bytes writen in the output_file
////////////////////////////////////////////////////////////////
tmpl(uint32_t)::get_total_len_tx_gmii()
{
    return r_total_len_tx_gmii.read();
}

///////////////////////////////////////////////////////////////
// return total number Bytes read from a tx_chbuf
///////////////////////////////////////////////////////////////
tmpl(uint32_t)::get_total_len_tx_chan()
{
    return r_total_len_tx_chan.read();
}

///////////////////////////////////////////////////////////////
// This function returns value stored in global registers
///////////////////////////////////////////////////////////////
tmpl(uint32_t)::read_global_register(uint32_t addr)
{
    uint32_t word = (addr & 0x00000FF) >> 2;
    uint32_t data = 0;

    switch(word)
    {
        case NIC_G_CHANNELS:
            data = r_global_channels.read();
        break;
        case NIC_G_BC_ENABLE:
            data = r_global_bc_enable.read();
        break;
        case NIC_G_PERIOD:
            data = r_global_period.read();
        break;
        case NIC_G_MAC_4:
            data = r_global_mac_4.read();
        break;
        case NIC_G_MAC_2:
            data = r_global_mac_2.read();
        break;

        case NIC_G_NPKT_RX_G2S_RECEIVED :
            data = r_rx_g2s_npkt_received.read();
        break;
        case NIC_G_NPKT_RX_G2S_DISCARDED :
            data = r_rx_g2s_npkt_discarded.read();
        break;

        case NIC_G_NPKT_RX_DES_SUCCESS :
            data = r_rx_des_npkt_success.read();
        break;
        case NIC_G_NPKT_RX_DES_TOO_SMALL :
            data = r_rx_des_npkt_too_small.read();
        break;
        case NIC_G_NPKT_RX_DES_TOO_BIG :
            data = r_rx_des_npkt_too_big.read();
        break;
        case NIC_G_NPKT_RX_DES_MFIFO_FULL :
            data = r_rx_des_npkt_mfifo_full.read();
        break;
        case NIC_G_NPKT_RX_DES_CRC_FAIL :
            data = r_rx_des_npkt_cs_fail.read();
        break;

        case NIC_G_NPKT_RX_DISPATCH_RECEIVED :
            data = r_rx_dispatch_npkt_received.read();
        break;
        case NIC_G_NPKT_RX_DISPATCH_BROADCAST :
            data = r_rx_dispatch_npkt_broadcast.read();
        break;
        case NIC_G_NPKT_RX_DISPATCH_DST_FAIL :
            data = r_rx_dispatch_npkt_dst_fail.read();
        break;
        case NIC_G_NPKT_RX_DISPATCH_CH_FULL :
            data = r_rx_dispatch_npkt_full.read();
        break;

        case NIC_G_NPKT_TX_DISPATCH_RECEIVED :
            data = r_tx_dispatch_npkt_received.read();
        break;
        case NIC_G_NPKT_TX_DISPATCH_TOO_SMALL :
            data = r_tx_dispatch_npkt_too_small.read();
        break;
        case NIC_G_NPKT_TX_DISPATCH_TOO_BIG :
            data = r_tx_dispatch_npkt_too_big.read();
        break;
        case NIC_G_NPKT_TX_DISPATCH_TRANSMIT :
            data = r_tx_dispatch_npkt_transmit.read();
        break;

        default:
            std::cout << "ERROR in VCI_MASTER_NIC : illegal global register read"
                      << " : address = " << std::hex << addr << std::endl;
    }
    return data;
} // end read_global_register()

///////////////////////////////////////////////////////////////////////
// This function returns value stored in channel registers
///////////////////////////////////////////////////////////////////////
tmpl(uint32_t)::read_channel_register(uint32_t addr)
{

    uint32_t channel = (addr & 0x000000C0) >> 6;
    uint32_t word    = (addr & 0x0000003F) >> 2;
    uint32_t data    = 0;

    switch(word)
    {
        case NIC_RX_CHANNEL_STATE:
            data = (uint32_t)r_rx_cma_fsm[channel].read();
            break;
        case NIC_RX_CHBUF_DESC_LO:
            data = (uint32_t)r_rx_cma_dst_desc[channel].read();
            break;            
        case NIC_RX_CHBUF_DESC_HI:
            data = (uint32_t)(r_rx_cma_dst_desc[channel].read()>>32);
            break;
        case NIC_RX_CHBUF_NBUFS:
            data = (uint32_t)r_rx_cma_dst_nbufs[channel].read();
            break;

        case NIC_TX_CHANNEL_STATE:
            data = (uint32_t)r_tx_cma_fsm[channel].read();
            break;
        case NIC_TX_CHBUF_DESC_LO:
            data = (uint32_t)r_tx_cma_src_desc[channel].read();
            break;            
        case NIC_TX_CHBUF_DESC_HI:
            data = (uint32_t)(r_tx_cma_src_desc[channel].read()>>32);
            break;
        case NIC_TX_CHBUF_NBUFS:
            data = (uint32_t)r_tx_cma_src_nbufs[channel].read();
            break;

        default:
            std::cout << "ERROR in VCI_MASTER_NIC : illegal channel register read"
                      << " : address = " << std::hex << addr << std::endl;
            exit(0);
    }
    return data;
} // end read_channel_register()

/////////////////////////
tmpl(void)::transition()
/////////////////////////
{
    if (!p_resetn)
    {
        r_global_bc_enable              = false;
        r_global_channels               = m_channels;
        r_global_period                 = CMA_POLL_PERIOD;
        r_global_mac_4                  = m_default_mac_4;
        r_global_mac_2                  = m_default_mac_2;
    
        r_tgt_fsm                       = TGT_IDLE;

        r_rx_g2s_fsm                    = RX_G2S_IDLE;
        r_rx_g2s_npkt_received          = 0;
        r_rx_g2s_npkt_discarded         = 0;

        r_cmd_fsm                       = CMD_IDLE;

        r_rsp_fsm                       = RSP_IDLE;

        r_rx_des_fsm                    = RX_DES_IDLE;
        r_rx_des_npkt_success           = 0;
        r_rx_des_npkt_too_small         = 0;
        r_rx_des_npkt_too_big           = 0;
        r_rx_des_npkt_mfifo_full        = 0;
        r_rx_des_npkt_cs_fail           = 0;

        r_rx_dispatch_fsm               = RX_DISPATCH_IDLE;
        r_rx_dispatch_npkt_received     = 0;
        r_rx_dispatch_npkt_broadcast    = 0;
        r_rx_dispatch_npkt_dst_fail     = 0;
        r_rx_dispatch_npkt_full         = 0;

        for( size_t k = 0 ; k < m_channels ; k++ )
        {
            r_rx_cma_fsm[k]             = RX_CMA_IDLE;
            r_rx_cma_run[k]             = false;
            r_rx_cma_src_index[k]       = 0;
            r_rx_cma_dst_index[k]       = 0;
            r_rx_cma_vci_req[k][0]      = false;
            r_rx_cma_vci_req[k][1]      = false;
            r_rx_cma_vci_req[k][2]      = false;
            r_rx_cma_vci_req[k][3]      = false;
            r_rx_cma_vci_rsp[k][0]      = false;
            r_rx_cma_vci_rsp[k][1]      = false;
            r_rx_cma_vci_rsp[k][2]      = false;
            r_rx_cma_vci_rsp[k][3]      = false;

            r_tx_cma_fsm[k]             = TX_CMA_IDLE;
            r_tx_cma_run[k]             = false;
            r_tx_cma_src_index[k]       = 0;
            r_tx_cma_dst_index[k]       = 0;
            r_tx_cma_vci_req[k][0]      = false;
            r_tx_cma_vci_req[k][1]      = false;
            r_tx_cma_vci_req[k][2]      = false;
            r_tx_cma_vci_req[k][3]      = false;
            r_tx_cma_vci_rsp[k][0]      = false;
            r_tx_cma_vci_rsp[k][1]      = false;
            r_tx_cma_vci_rsp[k][2]      = false;
            r_tx_cma_vci_rsp[k][3]      = false;
        }

        r_tx_dispatch_fsm               = TX_DISPATCH_IDLE;
        r_tx_dispatch_npkt_received     = 0;
        r_tx_dispatch_npkt_too_small    = 0;
        r_tx_dispatch_npkt_too_big      = 0;
        r_tx_dispatch_npkt_transmit     = 0;

        r_tx_ser_fsm                    = TX_SER_IDLE;
        r_tx_ser_ifg                    = m_gap;

        r_tx_s2g_fsm                    = TX_S2G_IDLE;
        r_tx_s2g_checksum               = 0;

        r_rx_fifo_stream.init();
        r_tx_fifo_stream.init();

        r_rx_fifo_multi.reset();
        r_tx_fifo_multi.reset();

        r_backend_rx->reset();
        r_backend_tx->reset();

        for ( size_t k = 0 ; k < m_channels ; k++ )
        {
            r_rx_irq[k] = false;
            r_tx_irq[k] = false;

            r_rx_chbuf[k]->reset();
            r_tx_chbuf[k]->reset();
        }

        r_total_cycles      = 0;
        r_total_len_rx_gmii = 0;
        r_total_len_rx_chan = 0;
        r_total_len_tx_chan = 0;
        r_total_len_tx_gmii = 0;

        return;
    } // end reset

    r_total_cycles = r_total_cycles.read() + 1;

    // rx_chbuf and tx_chbuf commands
    tx_chbuf_wcmd_t tx_chbuf_wcmd[m_channels];
    uint32_t        tx_chbuf_wdata0[m_channels];
    uint32_t        tx_chbuf_wdata1[m_channels];
    uint32_t        tx_chbuf_cont[m_channels];
    uint32_t        tx_chbuf_word[m_channels];
    tx_chbuf_rcmd_t tx_chbuf_rcmd[m_channels];

    rx_chbuf_wcmd_t rx_chbuf_wcmd[m_channels];
    uint32_t        rx_chbuf_wdata[m_channels];
    uint32_t        rx_chbuf_plen[m_channels];
    rx_chbuf_rcmd_t rx_chbuf_rcmd[m_channels];
    uint32_t        rx_chbuf_cont[m_channels];
    uint32_t        rx_chbuf_word[m_channels];

    // default values for rx_chbuf and tx chbuf commands
    for ( size_t k=0 ; k<m_channels ; k++)
        {
            tx_chbuf_wcmd[k] = TX_CHBUF_WCMD_NOP;
            tx_chbuf_rcmd[k] = TX_CHBUF_RCMD_NOP;
            rx_chbuf_wcmd[k] = RX_CHBUF_WCMD_NOP;
            rx_chbuf_rcmd[k] = RX_CHBUF_RCMD_NOP;
        }

    // multi_fifos commands
    fifo_multi_wcmd_t rx_fifo_multi_wcmd    = FIFO_MULTI_WCMD_NOP;
    uint32_t          rx_fifo_multi_wdata   = 0;
    uint32_t          rx_fifo_multi_padding = 0;
    fifo_multi_rcmd_t rx_fifo_multi_rcmd    = FIFO_MULTI_RCMD_NOP;

    fifo_multi_wcmd_t tx_fifo_multi_wcmd    = FIFO_MULTI_WCMD_NOP;
    uint32_t          tx_fifo_multi_wdata   = 0;
    uint32_t          tx_fifo_multi_padding = 0;
    fifo_multi_rcmd_t tx_fifo_multi_rcmd    = FIFO_MULTI_RCMD_NOP;

    // stream_fifos commands
    bool     rx_fifo_stream_read            = true;    // always  try to get data
    bool     rx_fifo_stream_write           = false;
    uint16_t rx_fifo_stream_wdata           = 0;

    bool     tx_fifo_stream_read            = false;
    bool     tx_fifo_stream_write           = false;
    uint16_t tx_fifo_stream_wdata           = 0;

    //////////////////////////////////////////////////////////////////////////////
    // This TGT_FSM controls the VCI TARGET port.
    // The VCI DATA field can be 32 or 64 bits.
    // The VCI PLEN field and the VCI ADDRESS field must be multiple of 4.
    // The configuration (write) and status (read) accesses must be one flit.
    // We acknowledge the VCI command, and decode it in the IDLE state.
    //////////////////////////////////////////////////////////////////////////////

    switch(r_tgt_fsm.read())
    {
        //////////////
        case TGT_IDLE:  // decode the VCI command
        {
            if (p_vci_tgt.cmdval.read() )
            {
                typename vci_param::addr_t	address = p_vci_tgt.address.read();
                typename vci_param::cmd_t	cmd     = p_vci_tgt.cmd.read();
                typename vci_param::plen_t  plen    = p_vci_tgt.plen.read();
                typename vci_param::be_t    be      = p_vci_tgt.be.read();

                bool found = false;
                std::list<soclib::common::Segment>::iterator seg;
                for ( seg = m_seglist.begin() ; seg != m_seglist.end() ; seg++ )
                {
                    if ( seg->contains(address) ) found = true;
                }

                assert ( found  and
                "ERROR in VCI_MASTER_NIC : ADDRESS is out of segment");

                assert( p_vci_tgt.eop.read() and
                "ERROR in VCI_MASTER_NIC : register access must be one flit");

                assert( ( ((vci_param::B==8) and ((be==0x0F) or (be==0xF0))) or 
                          ((vci_param::B==4) and (be==0xF)) ) and 
                "ERROR in VCI_MASTER_NIC : register write access must be 32 bits");


                uint32_t channel = (uint32_t)((address & 0x000000C0) >> 6);
                bool     global  =           ((address & 0x00000100) == 0);

                r_tgt_address  = (uint32_t)address;
                r_tgt_srcid	   = p_vci_tgt.srcid.read();
                r_tgt_trdid	   = p_vci_tgt.trdid.read();
                r_tgt_pktid	   = p_vci_tgt.pktid.read();
                r_tgt_wdata    = p_vci_tgt.wdata.read();
                r_tgt_be       = p_vci_tgt.be.read();

                // checking channel index
                if ( not global )
                {
                    assert( (channel < m_channels) and
                    "VCI_MASTER_NIC error : channel index larger than 3");
                }

                // decoding access type
                if ( global )                // global register read or write
                {
                    if ( cmd==vci_param::CMD_WRITE ) r_tgt_fsm = TGT_WRITE_GLOBAL_REG;
                    else                             r_tgt_fsm = TGT_READ_GLOBAL_REG;
                }
                else                        // channel register read or write
                {
                    if (cmd==vci_param::CMD_WRITE) r_tgt_fsm = TGT_WRITE_CHANNEL_REG;
                    else                           r_tgt_fsm = TGT_READ_CHANNEL_REG;
                }
            }
        }
        break;
        /////////////////////////
        case TGT_WRITE_GLOBAL_REG:
        {
            if ( p_vci_tgt.rspack.read() )
            {
                uint32_t address = r_tgt_address.read();
                uint32_t word    = (address & 0x000000FF) >> 2;
                uint32_t wdata;
                if      ( vci_param::B == 4 )
                {
                    wdata = r_tgt_wdata.read();
                }
                else // vci_param::B == 8
                {
                    if ( r_tgt_be.read() == 0x0F )  wdata = (uint32_t)r_tgt_wdata.read();
                    else                            wdata = (uint32_t)(r_tgt_wdata.read()>>32);
                }

                switch(word)
                {
                    case NIC_G_BC_ENABLE :
                        r_global_bc_enable = wdata;
                    break;
                    case NIC_G_PERIOD:
                        r_global_period = wdata;
                    break;
                    case NIC_G_MAC_4:
                        r_global_mac_4 = wdata;
                    break;
                    case NIC_G_MAC_2:
                        r_global_mac_2 = wdata;
                    break;
                    case NIC_G_NPKT_RESET:
                        r_rx_g2s_npkt_received          = 0;
                        r_rx_g2s_npkt_discarded         = 0;
                        r_rx_des_npkt_success           = 0;
                        r_rx_des_npkt_too_small         = 0;
                        r_rx_des_npkt_too_big           = 0;
                        r_rx_des_npkt_mfifo_full        = 0;
                        r_rx_des_npkt_cs_fail           = 0;
                        r_rx_dispatch_npkt_received     = 0;
                        r_rx_dispatch_npkt_broadcast    = 0;
                        r_rx_dispatch_npkt_dst_fail     = 0;
                        r_rx_dispatch_npkt_full         = 0;

                        r_tx_dispatch_npkt_received     = 0;
                        r_tx_dispatch_npkt_too_small    = 0;
                        r_tx_dispatch_npkt_too_big      = 0;
                        r_tx_dispatch_npkt_transmit     = 0;
                    break;

                    default:
                        std::cout << "ERROR in VCI_MASTER_NIC : illegal global register write"
                                  << " : address = " << std::hex << address << std::endl;
                        exit(0);
                }
                r_tgt_fsm = TGT_IDLE;
            }
        }
        break;
        ///////////////////////////
        case TGT_WRITE_CHANNEL_REG:  
        {
            if ( p_vci_tgt.rspack.read() )
            {
                uint32_t address = r_tgt_address.read();
                uint32_t word    = (address & 0x0000003F) >> 2;
                uint32_t channel = (address & 0x000000C0) >> 6;
                uint32_t wdata;

                if      ( vci_param::B == 4 )
                {
                    wdata = r_tgt_wdata.read();
                }
                else // vci_param::B == 8
                {
                    if ( r_tgt_be.read() == 0x0F )  wdata = (uint32_t)r_tgt_wdata.read();
                    else                            wdata = (uint32_t)(r_tgt_wdata.read()>>32);
                }

                switch(word)
                {
                    case NIC_RX_CHANNEL_RUN:     // RX channel activation/de-activation
                        r_rx_cma_run[channel] = (wdata != 0);
                    break;
                    case NIC_RX_CHBUF_DESC_LO:   // RX chbuf descriptor address 32 LSB bits
                        r_rx_cma_dst_desc[channel] = 
                        ((r_rx_cma_dst_desc[channel].read()>>32)<<32) + wdata;
                    break;
                    case NIC_RX_CHBUF_DESC_HI:   // RX chbuf descriptor address 32 MSB bits
                        r_rx_cma_dst_desc[channel] =
                        (r_rx_cma_dst_desc[channel].read() & 0xFFFFFFFF) + ((uint64_t)wdata << 32);
                    break;
                    case NIC_RX_CHBUF_NBUFS:     // RX chbuf depth
                        r_rx_cma_dst_nbufs[channel] = wdata;
                    break;

                    case NIC_TX_CHANNEL_RUN:     // RX channel activation/de-activation
                        r_tx_cma_run[channel] = (wdata != 0);
                    break;
                    case NIC_TX_CHBUF_DESC_LO:   // TX chbuf descriptor address 32 LSB bits
                        r_tx_cma_src_desc[channel] = 
                        ((r_tx_cma_src_desc[channel].read()>>32)<<32) + wdata;
                    break;
                    case NIC_TX_CHBUF_DESC_HI:   // TX chbuf descriptor address 32 MSB bits
                        r_tx_cma_src_desc[channel] =
                        (r_tx_cma_src_desc[channel].read() & 0xFFFFFFFF) + ((uint64_t)wdata << 32);
                    break;
                    case NIC_TX_CHBUF_NBUFS:     // TX chbut depth
                        r_tx_cma_src_nbufs[channel] = wdata;
                    break;
                    default:
                        std::cout << "ERROR in VCI_MASTER_NIC : illegal channel register write"
                                  << " : address = " << std::hex << address << std::endl;
                        exit(0);
                }
                r_tgt_fsm = TGT_IDLE;
            }
        }
        break;
        /////////////////////////
        case TGT_READ_GLOBAL_REG:   // send global register value in VCI response
        {
            if ( p_vci_tgt.rspack.read() )
            {
                r_tgt_fsm = TGT_IDLE;
            }
        }
        break;
        //////////////////////////
        case TGT_READ_CHANNEL_REG:  // send channel register value in VCI response
        {
            if ( p_vci_tgt.rspack.read() )
            {
                uint32_t address = r_tgt_address.read();
                uint32_t channel = (address & 0x000000C0) >> 6;
                uint32_t word    = (address & 0x0000003F) >> 2;
                
                // reset irq if STATUS read
                if( word == NIC_RX_CHANNEL_STATE ) r_rx_irq[channel] = false;
                if( word == NIC_TX_CHANNEL_STATE ) r_tx_irq[channel] = false;

                r_tgt_fsm = TGT_IDLE;
            }
        }
        break;
    } // end switch tgt_fsm

    ////////////////////////////////////////////////////////////////////////////
    // Get data from the backend (PHY) component and fill the rx_g2s pipe-line.
    ////////////////////////////////////////////////////////////////////////////

    bool    gmii_rx_dv = false;
    bool    gmii_rx_er = false;
    uint8_t gmii_rx_data;

    // get one byte from rx_backend
    r_backend_rx->get( &gmii_rx_dv,
                       &gmii_rx_er,
                       &gmii_rx_data);

    // update rx_g2s data pipe-line
    r_rx_g2s_dt0 = gmii_rx_data;
    r_rx_g2s_dt1 = r_rx_g2s_dt0.read();
    r_rx_g2s_dt2 = r_rx_g2s_dt1.read();
    r_rx_g2s_dt3 = r_rx_g2s_dt2.read();
    r_rx_g2s_dt4 = r_rx_g2s_dt3.read();
    r_rx_g2s_dt5 = r_rx_g2s_dt4.read();

#if RX_G2S_DEBUG
if(     gmii_rx_dv and not gmii_rx_er ) printf("\n  @@@ START : %x\n\n", gmii_rx_data );
if( not gmii_rx_dv and not gmii_rx_er ) printf("\n  @@@ END  : %x\n\n", gmii_rx_data );
if(     gmii_rx_dv and     gmii_rx_er ) printf("\n  @@@ ERROR : %x\n\n", gmii_rx_data );
if( not gmii_rx_dv and     gmii_rx_er ) printf("\n  @@@ EXTEND : %x\n\n", gmii_rx_data );
#endif

    ///////////////////////////////////////////////////////////////////////////
    // This RX_G2S module makes the BACKEND to STREAM format conversion.
    // It checks the checksum, and signals a possible error.
    // The input is the backend module.
    // The output is the rx_fifo_stream, but this fifo is only used for
    // clock boundary handling, and should never be full, as the consumer
    // (RX_DES module) read all available bytes at all cycles.
    ///////////////////////////////////////////////////////////////////////////

    assert( r_rx_fifo_stream.wok() and
            "ERROR in VCI_MASTER_NIC : the rs_fifo_stream should never be full");

    switch(r_rx_g2s_fsm.read())
    {
        /////////////////
        case RX_G2S_IDLE:   // waiting start of packet
        {
            if ( gmii_rx_dv and not gmii_rx_er ) // start of packet
            {
                r_rx_g2s_fsm           = RX_G2S_DELAY;
                r_rx_g2s_delay         = 0;
            }
            break;
        }
        //////////////////
        case RX_G2S_DELAY:  // entering bytes in the pipe (4 cycles)
        {
            if ( not gmii_rx_dv or gmii_rx_er ) // data invalid or error
            {
                r_rx_g2s_npkt_discarded = r_rx_g2s_npkt_discarded.read() + 1;
                r_rx_g2s_fsm            = RX_G2S_IDLE;
            }
            else if ( r_rx_g2s_delay.read() == 3 )
            {
                r_rx_g2s_fsm        = RX_G2S_LOAD;
                r_rx_g2s_checksum   = 0x00000000;      // reset checksum register
                r_total_len_rx_gmii = r_total_len_rx_gmii.read() + 1;
            }
            else
            {
                r_rx_g2s_delay      = r_rx_g2s_delay.read() + 1;
                r_total_len_rx_gmii = r_total_len_rx_gmii.read() + 1;
            }
            break;
        }
        /////////////////
        case RX_G2S_LOAD:   // initialize checksum accu
        {
            if ( gmii_rx_dv and not gmii_rx_er ) // data valid / no error
            {
                // update CRC
                r_rx_g2s_checksum   = m_crc.update( r_rx_g2s_checksum.read(),
                                                    (uint32_t)r_rx_g2s_dt4.read() );

                r_rx_g2s_fsm        = RX_G2S_SOS;
                r_total_len_rx_gmii = r_total_len_rx_gmii.read() + 1;
            }
            else
            {
                r_rx_g2s_npkt_discarded = r_rx_g2s_npkt_discarded.read() + 1;
                r_rx_g2s_fsm            = RX_G2S_IDLE;
            }
            break;
        }
        ////////////////
        case RX_G2S_SOS:    // write first byte in fifo_stream: SOS
        {
            if ( gmii_rx_dv and not gmii_rx_er ) // data valid / no error
            {
                rx_fifo_stream_write = true;
                rx_fifo_stream_wdata = r_rx_g2s_dt5.read() | (STREAM_TYPE_SOS << 8);

                // update CRC
                r_rx_g2s_checksum   = m_crc.update( r_rx_g2s_checksum.read(),
                                                    (uint32_t)r_rx_g2s_dt4.read() );

                r_rx_g2s_fsm        = RX_G2S_LOOP;
                r_total_len_rx_gmii = r_total_len_rx_gmii.read() + 1;
            }
            else
            {
                r_rx_g2s_npkt_discarded = r_rx_g2s_npkt_discarded.read() + 1;
                r_rx_g2s_fsm            = RX_G2S_IDLE;
            }
            break;
        }
        /////////////////
        case RX_G2S_LOOP:   // write one byte in fifo_stream : NEV
        {
            rx_fifo_stream_write = true;
            rx_fifo_stream_wdata = r_rx_g2s_dt5.read() | (STREAM_TYPE_NEV << 8);

            // update CRC
            r_rx_g2s_checksum   = m_crc.update( r_rx_g2s_checksum.read(),
                                                (uint32_t)r_rx_g2s_dt4.read() );

            r_total_len_rx_gmii = r_total_len_rx_gmii.read() + 1;

            if ( not gmii_rx_dv and not gmii_rx_er ) // end of paquet
            {
                r_rx_g2s_fsm = RX_G2S_END;
            }
            else if ( gmii_rx_dv and gmii_rx_er ) // error
            {
                r_rx_g2s_fsm = RX_G2S_FAIL;
            }
            else if ( not gmii_rx_dv and gmii_rx_er ) // error extend
            {
                r_rx_g2s_fsm = RX_G2S_ERR;
            }
            break;
        }
        ////////////////
        case RX_G2S_END:    // signal end of packet: EOS or ERR depending on checksum
        {
            uint32_t check = (uint32_t)r_rx_g2s_dt4.read() << 24 |
                             (uint32_t)r_rx_g2s_dt3.read() << 16 |
                             (uint32_t)r_rx_g2s_dt2.read() <<  8 |
                             (uint32_t)r_rx_g2s_dt1.read()       ;

            r_total_len_rx_gmii = r_total_len_rx_gmii.read() + 1;

            if ( r_rx_g2s_checksum.read() == check )
            {

#if RX_G2S_DEBUG
printf("  <NIC RX_G2S_END> : good packet at cycle %d / packet index = %d\n",
       r_total_cycles.read() , r_rx_g2s_npkt_received.read() );
#endif
                rx_fifo_stream_write = true;
                rx_fifo_stream_wdata = r_rx_g2s_dt5.read() | (STREAM_TYPE_EOS << 8);
            }
            else
            {

#if RX_G2S_DEBUG
printf("  <NIC RX_G2S_END> : error packet at cycle %d / expected crc = %x / received crc = %x\n",
       r_total_cycles.read(), r_rx_g2s_checksum.read(), check );
#endif
                rx_fifo_stream_write = true;
                rx_fifo_stream_wdata = r_rx_g2s_dt5.read() | (STREAM_TYPE_ERR << 8);
            }

            // increment number of received packets
            r_rx_g2s_npkt_received = r_rx_g2s_npkt_received.read() + 1;

            if ( gmii_rx_dv and not gmii_rx_er ) // start of packet / no error
            {
                r_rx_g2s_fsm           = RX_G2S_DELAY;
                r_rx_g2s_delay         = 0;
            }
            else
            {
                r_rx_g2s_fsm   = RX_G2S_IDLE;
            }
            break;
        }
        ////////////////
        case RX_G2S_ERR:  // signal error: ERR
        {
            rx_fifo_stream_write = true;
            rx_fifo_stream_wdata = r_rx_g2s_dt5.read() | (STREAM_TYPE_ERR << 8);

            if ( gmii_rx_dv and not gmii_rx_er ) // start of packet / no error
            {
                r_rx_g2s_npkt_received = r_rx_g2s_npkt_received.read() + 1;
                r_rx_g2s_fsm           = RX_G2S_DELAY;
                r_rx_g2s_delay         = 0;
            }
            else
            {
                r_rx_g2s_fsm   = RX_G2S_IDLE;
            }
            break;
        }
        /////////////////
        case RX_G2S_FAIL: // waiting end of paquet to signal error
        {
            if ( not gmii_rx_dv )
            {
                r_rx_g2s_fsm   = RX_G2S_ERR;
            }
            break;
        }
    } // end switch rx_g2s_fsm

    ///////////////////////////////////////////////////////////////////////////
    // This RX_DES module is in charge of deserialisation (4 bytes -> 1 word).
    // - The input is the rx_fifo_stream, respecting the stream format:
    //   8 bits data + 2 bits type.
    // - The output is the rx_fifo_multi that can store a full paquet.
    // It is also in charge of discarding input packets in four cases:
    // - if a packet is too small (64 - 4)B
    // - if a packet is too long (1518 - 4)B
    // - if a checksum error is reported by the RS_G2S FSM
    // - if there not space in the rx_fifo_multi
    // Implementation note:
    // - The FSM try to read a byte in rx_fifo_stream at all cycles.
    // - It test if the rx_fifo_multi is full in the state where it reads
    //   the last byte of a 32 bits word.
    ///////////////////////////////////////////////////////////i////////////////

    switch (r_rx_des_fsm.read())
    {
        /////////////////
        case RX_DES_IDLE:     // try to read first byte in rx_fifo_stream
        {
            uint16_t data = r_rx_fifo_stream.read();
            uint32_t type = (data >> 8) & 0x3;

            if ( r_rx_fifo_stream.rok() )   // do nothing if we cannot read
            {
                r_rx_des_data[0] = (uint8_t)(data & 0xFF);
                r_rx_des_counter_bytes = 1;
                if ( type == STREAM_TYPE_SOS ) r_rx_des_fsm = RX_DES_READ_1;
            }
            break;
        }
        ///////////////////
        case RX_DES_READ_1:     // read second byte in rx_fifo_stream
        {
            uint16_t data = r_rx_fifo_stream.read();
            uint32_t type = (data >> 8) & 0x3;

            if ( r_rx_fifo_stream.rok() )   // do nothing if we cannot read
            {
                r_rx_des_data[1] = (uint8_t)(data & 0xFF);
                r_rx_des_counter_bytes = r_rx_des_counter_bytes.read() + 1;
                if ( type == STREAM_TYPE_NEV )
                {
                    r_rx_des_fsm = RX_DES_READ_2;
                }
                else
                {
                    r_rx_des_npkt_too_small = r_rx_des_npkt_too_small.read() + 1;
                    r_rx_des_fsm = RX_DES_IDLE;
                }
            }
            break;
        }
        ///////////////////
        case RX_DES_READ_2:     // read third byte in rx_fifo_stream
        {
            uint16_t data = r_rx_fifo_stream.read();
            uint32_t type = (data >> 8) & 0x3;

            if ( r_rx_fifo_stream.rok() )   // do nothing if we cannot read
            {
                r_rx_des_data[2] = (uint8_t)(data & 0xFF);
                r_rx_des_counter_bytes = r_rx_des_counter_bytes.read() + 1;
                if ( type == STREAM_TYPE_NEV )
                {
                    r_rx_des_fsm = RX_DES_READ_3;
                }
                else
                {
                    r_rx_des_npkt_too_small = r_rx_des_npkt_too_small.read() + 1;
                    r_rx_des_fsm = RX_DES_IDLE;
                }
            }
            break;
        }
        ///////////////////
        case RX_DES_READ_3:    // read fourth byte in rx_fifo_stream
                               // and test if rx_fifo_multi can be written
        {
            uint16_t data = r_rx_fifo_stream.read();
            uint32_t type = (data >> 8) & 0x3;

            if ( r_rx_fifo_stream.rok() )   // do nothing if we cannot read
            {
                r_rx_des_data[3]	= (uint8_t)(data & 0xFF);
                r_rx_des_counter_bytes = r_rx_des_counter_bytes.read() + 1;

                if ( (type == STREAM_TYPE_NEV) and r_rx_fifo_multi.wok() )
                {
                    r_rx_des_fsm = RX_DES_READ_WRITE_0;
                }
                else if ( type != STREAM_TYPE_NEV )
                {
                    r_rx_des_npkt_too_small = r_rx_des_npkt_too_small.read() + 1;
                    r_rx_des_fsm = RX_DES_IDLE;
                }
                else  // fifo_multi full
                {
                    r_rx_des_npkt_mfifo_full = r_rx_des_npkt_mfifo_full.read() + 1;
                    r_rx_des_fsm = RX_DES_IDLE;
                }
            }
            break;
        }
        /////////////////////////
        case RX_DES_READ_WRITE_0:   // write previous word in rx_fifo_multi
                                    // and read first byte in rx_fifo_stream
        {
            // write previous word into fifo_multi (wok has been previouly checked)
            rx_fifo_multi_wcmd  = FIFO_MULTI_WCMD_WRITE;

            rx_fifo_multi_wdata = (uint32_t)(r_rx_des_data[0].read()      ) |
                                  (uint32_t)(r_rx_des_data[1].read() << 8 ) |
                                  (uint32_t)(r_rx_des_data[2].read() << 16) |
                                  (uint32_t)(r_rx_des_data[3].read() << 24) ;

            // Read new byte
            uint16_t data = r_rx_fifo_stream.read();
            uint32_t type = (data >> 8) & 0x3;
            uint32_t counter_bytes;

            if ( r_rx_fifo_stream.rok() )     // do nothing if we cannot read
            {
                r_rx_des_data[0]	   = (uint8_t)(data & 0xFF);
                counter_bytes          = r_rx_des_counter_bytes.read() + 1;
                r_rx_des_counter_bytes = counter_bytes;
                r_rx_des_padding	   = 3;

                if ( counter_bytes > (1518-4) )
                {
                    r_rx_des_npkt_too_big = r_rx_des_npkt_too_big.read() + 1;
                    r_rx_des_fsm = RX_DES_WRITE_CLEAR;
                }
                else if ( type == STREAM_TYPE_NEV )
                {
                    r_rx_des_fsm = RX_DES_READ_WRITE_1;
                }
                else if ( (type == STREAM_TYPE_EOS) )
                {
                    if ( not r_rx_fifo_multi.wok() )
                    {
                        r_rx_des_npkt_mfifo_full = r_rx_des_npkt_mfifo_full.read() + 1;
                        r_rx_des_fsm = RX_DES_WRITE_CLEAR;
                    }
                    else if ( counter_bytes < (64-4) )
                    {
                        r_rx_des_npkt_too_small = r_rx_des_npkt_too_small.read() + 1;
                        r_rx_des_fsm = RX_DES_WRITE_CLEAR;
                    }
                    else // success: EOS and WOK and not TOO_SMALL
                    {
                        r_rx_des_npkt_success = r_rx_des_npkt_success.read() + 1;
                        r_rx_des_fsm = RX_DES_WRITE_LAST;
                    }
                }
                else // TYPE == ERR or SOS
                {
                    r_rx_des_npkt_cs_fail = r_rx_des_npkt_cs_fail.read() + 1;
                    r_rx_des_fsm = RX_DES_WRITE_CLEAR;
                }
            }
            break;
        }
        /////////////////////////
       	case RX_DES_READ_WRITE_1:   // read second byte in rx_fifo_stream
        {
            uint16_t data = r_rx_fifo_stream.read();
            uint32_t type = (data >> 8) & 0x3;
            uint32_t counter_bytes;

            if ( r_rx_fifo_stream.rok() )     // do nothing if we cannot read
            {
                r_rx_des_data[1]	   = (uint8_t)(data & 0xFF);
                counter_bytes          = r_rx_des_counter_bytes.read() + 1;
                r_rx_des_counter_bytes = counter_bytes;
                r_rx_des_padding	   = 2;

                if ( counter_bytes > (1518-4) )
                {
                    r_rx_des_npkt_too_big = r_rx_des_npkt_too_big.read() + 1;
                    r_rx_des_fsm = RX_DES_WRITE_CLEAR;
                }
                else if ( type == STREAM_TYPE_NEV )
                {
                    r_rx_des_fsm = RX_DES_READ_WRITE_2;
                }
                else if ( (type == STREAM_TYPE_EOS) )
                {
                    if ( not r_rx_fifo_multi.wok() )
                    {
                        r_rx_des_npkt_mfifo_full = r_rx_des_npkt_mfifo_full.read() + 1;
                        r_rx_des_fsm = RX_DES_WRITE_CLEAR;
                    }
                    else if ( counter_bytes < (64-4) )
                    {
                        r_rx_des_npkt_too_small = r_rx_des_npkt_too_small.read() + 1;
                        r_rx_des_fsm = RX_DES_WRITE_CLEAR;
                    }
                    else // success: EOS and WOK and not TOO_SMALL
                    {
                        r_rx_des_npkt_success = r_rx_des_npkt_success.read() + 1;
                        r_rx_des_fsm = RX_DES_WRITE_LAST;
                    }
                }
                else // TYPE == ERR or SOS
                {
                    r_rx_des_npkt_cs_fail = r_rx_des_npkt_cs_fail.read() + 1;
                    r_rx_des_fsm = RX_DES_WRITE_CLEAR;
                }
            }
            break;
        }
        /////////////////////////
       	case RX_DES_READ_WRITE_2:   // read third byte in rx_fifo_stream
        {
            uint16_t data = r_rx_fifo_stream.read();
            uint32_t type = (data >> 8) & 0x3;
            uint32_t counter_bytes;

            if ( r_rx_fifo_stream.rok() )     // do nothing if we cannot read
            {
                r_rx_des_data[2]	   = (uint8_t)(data & 0xFF);
                counter_bytes          = r_rx_des_counter_bytes.read() + 1;
                r_rx_des_counter_bytes = counter_bytes;
                r_rx_des_padding	   = 1;

                if ( counter_bytes > (1518-4) )
                {
                    r_rx_des_npkt_too_big = r_rx_des_npkt_too_big.read() + 1;
                    r_rx_des_fsm = RX_DES_WRITE_CLEAR;
                }
                else if ( type == STREAM_TYPE_NEV )
                {
                    r_rx_des_fsm = RX_DES_READ_WRITE_3;
                }
                else if ( (type == STREAM_TYPE_EOS) )
                {
                    if ( not r_rx_fifo_multi.wok() )
                    {
                        r_rx_des_npkt_mfifo_full = r_rx_des_npkt_mfifo_full.read() + 1;
                        r_rx_des_fsm = RX_DES_WRITE_CLEAR;
                    }
                    else if ( counter_bytes < (64-4) )
                    {
                        r_rx_des_npkt_too_small = r_rx_des_npkt_too_small.read() + 1;
                        r_rx_des_fsm = RX_DES_WRITE_CLEAR;
                    }
                    else // success: EOS and WOK and not TOO_SMALL
                    {
                        r_rx_des_npkt_success = r_rx_des_npkt_success.read() + 1;
                        r_rx_des_fsm = RX_DES_WRITE_LAST;
                    }
                }
                else // TYPE == ERR or SOS
                {
                    r_rx_des_npkt_cs_fail = r_rx_des_npkt_cs_fail.read() + 1;
                    r_rx_des_fsm = RX_DES_WRITE_CLEAR;
                }
            }
            break;
        }
        /////////////////////////
        case RX_DES_READ_WRITE_3:    // read fourth byte in rx_fifo_stream
        {
            uint16_t data = r_rx_fifo_stream.read();
            uint32_t type = (data >> 8) & 0x3;
            uint32_t counter_bytes;

            if ( r_rx_fifo_stream.rok() )     // do nothing if we cannot read
            {
                r_rx_des_data[3]	   = (uint8_t)(data & 0xFF);
                counter_bytes          = r_rx_des_counter_bytes.read() + 1;
                r_rx_des_counter_bytes = counter_bytes;
                r_rx_des_padding	   = 0;

                if ( counter_bytes > (1518-4) )
                {
                    r_rx_des_npkt_too_big = r_rx_des_npkt_too_big.read() + 1;
                    r_rx_des_fsm = RX_DES_WRITE_CLEAR;
                }
                else if ( type == STREAM_TYPE_NEV )
                {
                    r_rx_des_fsm = RX_DES_READ_WRITE_0;
                }
                else if ( (type == STREAM_TYPE_EOS) )
                {
                    if ( not r_rx_fifo_multi.wok() )
                    {
                        r_rx_des_npkt_mfifo_full = r_rx_des_npkt_mfifo_full.read() + 1;
                        r_rx_des_fsm = RX_DES_WRITE_CLEAR;
                    }
                    else if ( counter_bytes < (64-4) )
                    {
                        r_rx_des_npkt_too_small = r_rx_des_npkt_too_small.read() + 1;
                        r_rx_des_fsm = RX_DES_WRITE_CLEAR;
                    }
                    else // success : EOS and WOK and not TOO_SMALL
                    {
                        r_rx_des_npkt_success = r_rx_des_npkt_success.read() + 1;
                        r_rx_des_fsm = RX_DES_WRITE_LAST;
                    }
                }
                else // TYPE == ERR or SOS
                {
                    r_rx_des_npkt_cs_fail = r_rx_des_npkt_cs_fail.read() + 1;
                    r_rx_des_fsm = RX_DES_WRITE_CLEAR;
                }
            }
            break;
        }
        ///////////////////////
        case RX_DES_WRITE_LAST:     // write last word in rx_fifo_multi
                                    // depending on r_rx_des_padding
        {
            rx_fifo_multi_wcmd    = FIFO_MULTI_WCMD_LAST;

            if ( r_rx_des_padding.read() == 0 )
            {
                rx_fifo_multi_wdata 	= (uint32_t)(r_rx_des_data[0].read()      ) |
                                          (uint32_t)(r_rx_des_data[1].read() << 8 ) |
                                          (uint32_t)(r_rx_des_data[2].read() << 16) |
                                          (uint32_t)(r_rx_des_data[3].read() << 24) ;
            }
            else if ( r_rx_des_padding.read() == 1 )
            {
                rx_fifo_multi_wdata 	= (uint32_t)(r_rx_des_data[0].read()      ) |
                                          (uint32_t)(r_rx_des_data[1].read() << 8 ) |
                                          (uint32_t)(r_rx_des_data[2].read() << 16) ;
            }
            else if ( r_rx_des_padding.read() == 2 )
            {
                rx_fifo_multi_wdata 	= (uint32_t)(r_rx_des_data[0].read()      ) |
                                          (uint32_t)(r_rx_des_data[1].read() << 8 ) ;
            }
            else  // padding = 3
            {
                rx_fifo_multi_wdata 	= (uint32_t)(r_rx_des_data[0].read()      ) ;
            }

            rx_fifo_multi_padding = r_rx_des_padding.read();
            r_rx_des_fsm = RX_DES_IDLE;
            break;
        }
        ////////////////////////
	    case RX_DES_WRITE_CLEAR:
        {
            rx_fifo_multi_wcmd  = FIFO_MULTI_WCMD_CLEAR;
            r_rx_des_fsm        = RX_DES_IDLE;
        }
    } // end swich rx_des_fsm

    /////////////////////////////////////////////////////////////////////////
    // The RX_DISPATCH FSM moves one Ethernet packet from the rx_fifo_multi 
    // to one RX chbuf selected by a hash key on the source IP address. 
    // This requires to store the 8 first words (Ethernet Header + IP header)
    // in temporary registers.
    // Packets with unexpected MAC address are discarded, but broadcast
    // packets are always accepted if broadcast mode is activated.
    /////////////////////////////////////////////////////////////////////////

    switch( r_rx_dispatch_fsm.read() )
    {
        //////////////////////
        case RX_DISPATCH_IDLE:  // wait RX fifo not empty
        {
            if ( r_rx_fifo_multi.rok() ) 
            {
                r_rx_dispatch_npkt_received = r_rx_dispatch_npkt_received.read() + 1;
                r_rx_dispatch_fsm = RX_DISPATCH_GET_PLEN;
            }
        }
        break;
        //////////////////////////
        case RX_DISPATCH_GET_PLEN: // get packet length from fifo_multi
        {
            uint32_t nbytes = r_rx_fifo_multi.plen();
            r_rx_dispatch_nbytes = nbytes;
            r_rx_dispatch_plen   = nbytes;
            r_rx_dispatch_index  = 0;
            r_rx_dispatch_fsm    = RX_DISPATCH_READ_HEADER;
        }
        break;
        ////////////////////////////
        case RX_DISPATCH_READ_HEADER: // read 8 first words from fifo
        {
            uint32_t idx = r_rx_dispatch_index.read();

            rx_fifo_multi_rcmd      = FIFO_MULTI_RCMD_READ;
            r_rx_dispatch_nbytes    = r_rx_dispatch_nbytes.read() - 4;
            r_rx_dispatch_index     = idx + 1;
            r_rx_dispatch_word[idx] = r_rx_fifo_multi.data();

            if ( idx == 7 ) r_rx_dispatch_fsm = RX_DISPATCH_SELECT;
        }
        break;
        ////////////////////////
        case RX_DISPATCH_SELECT:  // check destination MAC address,
                                  // decode source IP address to select the RX channel.
                                  // close container if not enough space or time
        {
            uint32_t key = (((MY_HTONL(r_rx_dispatch_word[6].read()) & 0x000000FF)      ) +
                            ((MY_HTONL(r_rx_dispatch_word[6].read()) & 0x0000FF00) >> 8 ) +
                            ((MY_HTONL(r_rx_dispatch_word[7].read()) & 0x00FF0000) >> 16) +
                            ((MY_HTONL(r_rx_dispatch_word[7].read()) & 0xFF000000) >> 24)) % m_channels;

            // compute match and broadcast from destination MAC address check this
            uint32_t dst_mac_2 =  (MY_HTONL(r_rx_dispatch_word[0].read()) & 0xFFFF0000) >> 16; 

            uint32_t dst_mac_4 = ((MY_HTONL(r_rx_dispatch_word[0].read()) & 0x0000FFFF) << 16) |     
                                 ((MY_HTONL(r_rx_dispatch_word[1].read()) & 0xFFFF0000) >> 16) ;
     
            bool     bc        = ( dst_mac_4 == 0xFFFFFFFF ) && 
                                 ( dst_mac_2 == 0XFFFF ) && 
                                 r_global_bc_enable.read();

            bool     match     = ( dst_mac_4 == r_global_mac_4.read() ) &&
                                 ( dst_mac_2 == r_global_mac_2.read() );

            // analyse selected chbuf status
            uint32_t nbytes    = r_rx_dispatch_plen.read();
            bool     wok       = r_rx_chbuf[key]->wok();
            bool     space     = (r_rx_chbuf[key]->space() > nbytes);
            bool     time      = (r_rx_chbuf[key]->time() > (nbytes>>2));

            if ( bc or match )      // matching channel found
            {
                r_rx_dispatch_channel = key;

                if ( not wok )  // container full => discard packet
                {

#if RX_DISPATCH_DEBUG
printf("  <NIC RX_DISPATCH_SELECT> at cycle %d : channel %d full / skip packet\n",
       r_total_cycles.read(), (int)key );
#endif
                    r_rx_dispatch_fsm = RX_DISPATCH_PACKET_SKIP;
                    r_rx_dispatch_npkt_full = r_rx_dispatch_npkt_full.read() + 1;

                }
                else if (space and time)    // transfer possible
                {

#if RX_DISPATCH_DEBUG
printf("  <NIC RX_DISPATCH_SELECT> at cycle %d : write packet to channel %d / length = %d\n",
       r_total_cycles.read(), (int)key , r_rx_dispatch_plen.read() ); 
#endif
                    r_rx_dispatch_index = 0;
                    r_rx_dispatch_fsm   = RX_DISPATCH_WRITE_HEADER;
                }
                else    // not enough space or time => close container and retry
                {
                    r_rx_dispatch_fsm = RX_DISPATCH_CLOSE_CONT;
                }
            }
            else                // no matching channel found => discard packet
            {

#if RX_DISPATCH_DEBUG
printf("  <NIC RX_DISPATCH_SELECT> at cycle %d : unexpected MAC address / skip packet\n"
       "    plen = %d / received dst_mac = %x|%x / expected = %x|%x\n",
       r_total_cycles.read() , r_rx_dispatch_plen.read(),
       dst_mac_2 , dst_mac_4 , r_global_mac_2.read() , r_global_mac_4.read() );
#endif
                r_rx_dispatch_fsm = RX_DISPATCH_PACKET_SKIP;
                r_rx_dispatch_npkt_dst_fail = r_rx_dispatch_npkt_dst_fail.read() + 1;
            }
        }
        break;
        /////////////////////////////
        case RX_DISPATCH_PACKET_SKIP:	// clear an unexpected packet in fifo_multi
        {
            rx_fifo_multi_rcmd = FIFO_MULTI_RCMD_SKIP;
            r_rx_dispatch_fsm  = RX_DISPATCH_IDLE;
        }
        break;
        ////////////////////////////
        case RX_DISPATCH_CLOSE_CONT:  // Not enough space or time to write: close container
        {
            uint32_t channel       = r_rx_dispatch_channel.read();
            rx_chbuf_wcmd[channel] = RX_CHBUF_WCMD_RELEASE;
            r_rx_dispatch_fsm      = RX_DISPATCH_SELECT;

#if RX_DISPATCH_DEBUG
printf("  <NIC RX_DISPATCH_CLOSE_CONT> at cycle %d for channel %d\n",  
       r_total_cycles.read(), channel ); 
r_rx_chbuf[channel]->print_trace( channel , 64 );
#endif

        }
        break;
        //////////////////////////////
        case RX_DISPATCH_WRITE_HEADER:    // write 8 first words to selected channel 
        {
            uint32_t channel       = r_rx_dispatch_channel.read();
            uint32_t idx           = r_rx_dispatch_index.read();

            // move one word from word[idx] to chbuf per cycle
            rx_chbuf_wcmd[channel]  = RX_CHBUF_WCMD_WRITE;
            rx_chbuf_wdata[channel] = r_rx_dispatch_word[idx].read();

            r_rx_dispatch_index    = idx + 1;
            r_total_len_rx_chan    = r_total_len_rx_chan.read() + 4;

            // if last word, move one more word from fifo to word[7]
            if ( idx == 7 ) 
            {
                r_rx_dispatch_word[7] = r_rx_fifo_multi.data();
                rx_fifo_multi_rcmd    = FIFO_MULTI_RCMD_READ;
                r_rx_dispatch_nbytes  = r_rx_dispatch_nbytes.read() - 4;

                r_rx_dispatch_fsm     = RX_DISPATCH_READ_WRITE;
            }
        }
        break;
        ////////////////////////////
        case RX_DISPATCH_READ_WRITE: // move data[6] to selected channel 
                                     // move a new word from fifo to data[6]
        {
            uint32_t channel       = r_rx_dispatch_channel.read();

            // move one word to selected channel 
            rx_chbuf_wcmd[channel]  = RX_CHBUF_WCMD_WRITE;
            rx_chbuf_wdata[channel] = r_rx_dispatch_word[7].read();

            // move one word from fifo to data[6]
            r_rx_dispatch_word[7] = r_rx_fifo_multi.data();
            if (r_rx_dispatch_nbytes.read() <= 4) rx_fifo_multi_rcmd = FIFO_MULTI_RCMD_LAST;
            else                                  rx_fifo_multi_rcmd = FIFO_MULTI_RCMD_READ;

            r_total_len_rx_chan = r_total_len_rx_chan.read() + 4;

            if (r_rx_dispatch_nbytes.read() <= 4)   // last word
            {
                r_rx_dispatch_fsm = RX_DISPATCH_WRITE_LAST;
            }
            else                                    // not the last word
            {
                r_rx_dispatch_nbytes = r_rx_dispatch_nbytes.read() - 4;
            }
        }
         break;
        ////////////////////////////
        case RX_DISPATCH_WRITE_LAST:  // write last word & packet length to selected channel
        {
            uint32_t channel       = r_rx_dispatch_channel.read();

            // move last word & plen to selected channel 
            rx_chbuf_wcmd[channel]  = RX_CHBUF_WCMD_LAST;
            rx_chbuf_wdata[channel] = r_rx_dispatch_word[7].read();
            rx_chbuf_plen[channel]  = r_rx_dispatch_plen.read();
 
            r_total_len_rx_chan = r_total_len_rx_chan.read() + r_rx_dispatch_nbytes.read();

            r_rx_dispatch_fsm   = RX_DISPATCH_IDLE;
        }
        break;
    } // end switch r_rx_dispatch_fsm

    /////////////////////////////////////////////////////////////////////
    // The TX_DISPATCH FSM moves all Ethernet packets contained in a
    // single container from a tx_chbuf[k] to the multi-buffer tx_fifo.
    // As there is up to 4 clients chbufs, it use a round-robin policy.
    // A new channel is selected when a complete container has been
    // transmitted, and the TX_DISPATCH FSM is IDLE.
    /////////////////////////////////////////////////////////////////////

    switch( r_tx_dispatch_fsm.read() )
    {
        //////////////////////
        case TX_DISPATCH_IDLE:  // ready to start a new container transfer
                                // channel allocation is done for a full container
        {
            for ( size_t x = 0 ; x < m_channels ; x++ )
            {
                uint32_t channel = (x + 1 + r_tx_dispatch_channel.read()) % m_channels;
                if ( r_tx_chbuf[channel]->rok() )
                {
                    r_tx_dispatch_channel = channel;
                    r_tx_dispatch_fsm     = TX_DISPATCH_GET_NPKT;
                    break;
                }
            }
        }
        break;
        //////////////////////////
        case TX_DISPATCH_GET_NPKT: // get packets number from tx_chbuf
        {
            uint32_t    channel   = r_tx_dispatch_channel.read();
            uint32_t    npkt      = r_tx_chbuf[channel]->npkt();
            r_tx_dispatch_packets = npkt;

            if ((npkt == 0) or (npkt > 66))  r_tx_dispatch_fsm = TX_DISPATCH_RELEASE_CONT; 
            else                             r_tx_dispatch_fsm = TX_DISPATCH_GET_PLEN;
            break;
        }
        //////////////////////////
        case TX_DISPATCH_GET_PLEN: // get packet length from tx_chbuf
        {
            uint32_t channel    = r_tx_dispatch_channel.read();
            uint32_t plen       = r_tx_chbuf[channel]->plen();

            r_tx_dispatch_bytes = plen & 0x3;
            if ( (plen & 0x3) == 0 ) r_tx_dispatch_nwords = plen >> 2;
            else                     r_tx_dispatch_nwords = (plen >> 2) + 1;

            r_tx_dispatch_npkt_received = r_tx_dispatch_npkt_received.read()+1;
            if (plen < 60 ) // pkt too small
            {

#if TX_DISPATCH_DEBUG
printf("  <NIC TX_DISPATCH_GET_PLEN> at cycle %d : packet too small for channel %d / length = %d\n",
       r_total_cycles.read(), channel , plen );
#endif
                r_tx_dispatch_fsm            = TX_DISPATCH_SKIP_PKT;
                r_tx_dispatch_npkt_too_small = r_tx_dispatch_npkt_too_small.read() + 1;
            }
            else if (plen > 1514) // pkt too long
            {

#if TX_DISPATCH_DEBUG
printf("  <NIC TX_DISPATCH_GET_PLEN> at cycle %d : packet too long for channel %d / length = %d\n",
       r_total_cycles.read(), channel , plen );
#endif
                r_tx_dispatch_fsm            = TX_DISPATCH_SKIP_PKT;
                r_tx_dispatch_npkt_too_big   = r_tx_dispatch_npkt_too_big.read() + 1;
            }
            else
            {

#if TX_DISPATCH_DEBUG
printf("  <NIC TX_DISPATCH_GET_PLEN> at cycle %d : get packet for channel %d / length = %d\n",
       r_total_cycles.read(), channel , plen );
#endif
                r_tx_dispatch_fsm  = TX_DISPATCH_READ_FIRST;
            }
            break;
        }
        //////////////////////////
        case TX_DISPATCH_SKIP_PKT:  // discard too small or too large packets
        {
            uint32_t  channel = r_tx_dispatch_channel.read();
            uint32_t  packets = r_tx_dispatch_packets.read();

            tx_chbuf_rcmd[channel] = TX_CHBUF_RCMD_SKIP;
            r_tx_dispatch_packets  = packets - 1;

            if(packets == 1)
            {
                r_tx_dispatch_fsm = TX_DISPATCH_RELEASE_CONT;
            }
            else
            {
                r_tx_dispatch_fsm = TX_DISPATCH_GET_PLEN;
            }
            break;
        }
        ////////////////////////////
        case TX_DISPATCH_READ_FIRST:   // read first word from tx_chbuf
        {
            uint32_t  channel = r_tx_dispatch_channel.read();

            r_tx_dispatch_word     = r_tx_chbuf[channel]->data();
            tx_chbuf_rcmd[channel] = TX_CHBUF_RCMD_READ;
            r_tx_dispatch_nwords   = r_tx_dispatch_nwords.read() - 1;
            r_tx_dispatch_fsm      = TX_DISPATCH_READ_WRITE;
            break;
        }
        ////////////////////////////
        case TX_DISPATCH_READ_WRITE: // write previous word in multi_fifos
                                     // and read a new word from selected chbuf
        {
            uint32_t  channel = r_tx_dispatch_channel.read();

            if ( r_tx_fifo_multi.wok() )
            {
                // write word (i-1)
                tx_fifo_multi_wcmd  = FIFO_MULTI_WCMD_WRITE;
                tx_fifo_multi_wdata = r_tx_dispatch_word.read();

                // read word (i)
                if ( r_tx_dispatch_nwords.read() == 1 )  // last word in packet
                {
                    tx_chbuf_rcmd[channel] = TX_CHBUF_RCMD_LAST;
                    r_tx_dispatch_word     = r_tx_chbuf[channel]->data();
                    r_tx_dispatch_fsm      = TX_DISPATCH_WRITE_LAST;
                }
                else                                   // not the last word
                {
                    tx_chbuf_rcmd[channel] = TX_CHBUF_RCMD_READ;
                    r_tx_dispatch_word     = r_tx_chbuf[channel]->data();
                    r_tx_dispatch_nwords   = r_tx_dispatch_nwords.read() - 1;
                }
            }
            break;
        }
        ////////////////////////////
        case TX_DISPATCH_WRITE_LAST:  // write last word in multi_fifo
        {
            uint32_t  bytes        = r_tx_dispatch_bytes.read();

            if ( r_tx_fifo_multi.wok() )
            {
                tx_fifo_multi_wcmd  = FIFO_MULTI_WCMD_LAST;
                tx_fifo_multi_wdata = r_tx_dispatch_word.read();

                if ( bytes == 0 ) tx_fifo_multi_padding = 0;
                else              tx_fifo_multi_padding = 4 - bytes;

                if ( r_tx_dispatch_packets.read() == 1 )  // last packet in container
                {
                    r_tx_dispatch_fsm = TX_DISPATCH_RELEASE_CONT;
                }
                else                              // not the last packet
                {
                    r_tx_dispatch_packets = r_tx_dispatch_packets.read() - 1;
                    r_tx_dispatch_fsm     = TX_DISPATCH_GET_PLEN;
                }

                r_tx_dispatch_npkt_transmit = r_tx_dispatch_npkt_transmit.read() + 1;

#if TX_DISPATCH_DEBUG
printf("  <NIC TX_DISPATCH_WRITE_LAST> at cycle %d : completes packet for channel = %d\n",
       r_total_cycles.read(), (int)r_tx_dispatch_channel.read() );
#endif
            }
            break;
        }
        //////////////////////////////
        case TX_DISPATCH_RELEASE_CONT: // release the container in tx_chbuf
        {
            uint32_t channel       = r_tx_dispatch_channel.read();
            tx_chbuf_rcmd[channel] = TX_CHBUF_RCMD_RELEASE;
            r_tx_dispatch_fsm      = TX_DISPATCH_IDLE;

            break;
        }
    } // end switch tx_dispatch_fsm

    ////////////////////////////////////////////////////////////////////////////
    // This TX_SER FSM performs the serialisation (1 word => 4 bytes),
    // The input is the tx_fifo_multi.
    // The output is the tx_fifo_stream.
    ////////////////////////////////////////////////////////////////////////////

    switch(r_tx_ser_fsm.read())
    {
        /////////////////
        case TX_SER_IDLE:   // get packet length (bytes) if fifo_multi not empty
        {
            if ( r_tx_fifo_multi.rok() )
            {
                uint32_t plen = r_tx_fifo_multi.plen();

                r_tx_ser_bytes = plen & 0x3;
                if ( (plen & 0x3) == 0 ) r_tx_ser_words = plen>>2;
                else                     r_tx_ser_words = (plen>>2) + 1;
                r_tx_ser_fsm = TX_SER_READ_FIRST;
            }
            break;
        }
        ///////////////////////
        case TX_SER_READ_FIRST: // read first word
        {
            tx_fifo_multi_rcmd = FIFO_MULTI_RCMD_READ;
            r_tx_ser_words     = r_tx_ser_words.read() - 1;
            r_tx_ser_data      = r_tx_fifo_multi.data();
            r_tx_ser_first     = true;
            r_tx_ser_fsm       = TX_SER_WRITE_B0;
            break;
        }
        /////////////////////
        case TX_SER_WRITE_B0: // write first byte from current word
        {
            if ( r_tx_fifo_stream.wok() )
            {
                uint32_t words = r_tx_ser_words.read();
                uint32_t bytes = r_tx_ser_bytes.read();
                bool     first = r_tx_ser_first.read();

                if ( first )                              // first byte in packet
                {
                    tx_fifo_stream_write = true;
                    tx_fifo_stream_wdata = (uint16_t)((r_tx_ser_data.read() & 0x000000FF)
                                                                  | (STREAM_TYPE_SOS << 8));
                    r_tx_ser_fsm = TX_SER_WRITE_B1;
                }
                else if ( (words == 0) and (bytes == 1) ) // last byte in packet
                {
                    tx_fifo_stream_write = true;
                    tx_fifo_stream_wdata = (uint16_t)((r_tx_ser_data.read() & 0x000000FF)
                                                                  | (STREAM_TYPE_EOS << 8));
                    r_tx_ser_fsm = TX_SER_GAP;
                }
                else                                     // simple byte
                {
                    tx_fifo_stream_write = true;
                    tx_fifo_stream_wdata = (uint16_t)((r_tx_ser_data.read() & 0x000000FF)
                                                                  | (STREAM_TYPE_NEV << 8));
                    r_tx_ser_fsm = TX_SER_WRITE_B1;
                }
            }
            break;
        }
        /////////////////////
        case TX_SER_WRITE_B1:  // write second byte from current word
        {
            if ( r_tx_fifo_stream.wok() )
            {
                uint32_t words = r_tx_ser_words.read();
                uint32_t bytes = r_tx_ser_bytes.read();

                if ( (words == 0) and (bytes == 2) ) // last byte in packet
                {
                    tx_fifo_stream_write = true;
                    tx_fifo_stream_wdata = (uint16_t)((r_tx_ser_data.read() & 0x0000FF00)>>8
                                                                  | (STREAM_TYPE_EOS << 8));
                    r_tx_ser_fsm = TX_SER_GAP;
                }
                else                                 // simple byte
                {
                    tx_fifo_stream_write = true;
                    tx_fifo_stream_wdata = (uint16_t)((r_tx_ser_data.read() & 0x0000FF00)>>8
                                                                  | (STREAM_TYPE_NEV << 8));
                    r_tx_ser_fsm = TX_SER_WRITE_B2;
                }
            }
            break;
        }
        ////////////////////
        case TX_SER_WRITE_B2:  // write third byte from current word
        {
            if ( r_tx_fifo_stream.wok() )
            {
                uint32_t words = r_tx_ser_words.read();
                uint32_t bytes = r_tx_ser_bytes.read();

                if ( (words == 0) and (bytes == 3) ) // last byte in packet
                {
                    tx_fifo_stream_write = true;
                    tx_fifo_stream_wdata = (uint16_t)((r_tx_ser_data.read() & 0x00FF0000)>>16
                                                                  | (STREAM_TYPE_EOS << 8));
                    r_tx_ser_fsm = TX_SER_GAP;
                }
                else                                     // simple byte
                {
                    tx_fifo_stream_write = true;
                    tx_fifo_stream_wdata = (uint16_t)((r_tx_ser_data.read() & 0x00FF0000)>>16
                                                                  | (STREAM_TYPE_NEV << 8));
                    r_tx_ser_fsm = TX_SER_READ_WRITE;
                }
            }
            break;
        }
        ///////////////////////
        case TX_SER_READ_WRITE:  // write fourth byte from current word
                                 // and read next word in tx_fifo_multi
                                 // if packet not completed
        {
            if ( r_tx_fifo_stream.wok() )
            {
                uint32_t words = r_tx_ser_words.read();

                if ( words == 0 )  // last byte in packet
                {
                    tx_fifo_stream_write = true;
                    tx_fifo_stream_wdata = (uint16_t)((r_tx_ser_data.read() & 0xFF000000)>>24
                                                                  | (STREAM_TYPE_EOS << 8));
                    r_tx_ser_fsm = TX_SER_GAP;
                }
                else                                     // simple byte
                {
                    // read next word from fifo_multi
                    if ( words == 1 ) tx_fifo_multi_rcmd = FIFO_MULTI_RCMD_LAST;
                    else              tx_fifo_multi_rcmd = FIFO_MULTI_RCMD_READ;
                    r_tx_ser_words     = words - 1;
                    r_tx_ser_data      = r_tx_fifo_multi.data();
                    r_tx_ser_first     = false;
                    r_tx_ser_fsm       = TX_SER_WRITE_B0;
                    tx_fifo_stream_write = true;
                    tx_fifo_stream_wdata = (uint16_t)((r_tx_ser_data.read() & 0xFF000000)>>24
                                                                  | (STREAM_TYPE_NEV << 8));
                }
            }
            break;
        }
        ////////////////
        case TX_SER_GAP: // Inter Frame Gap delay
        {
            r_tx_ser_ifg = r_tx_ser_ifg.read() - 1;
            if (r_tx_ser_ifg.read() == 1)
            {
                r_tx_ser_ifg = m_gap;
                r_tx_ser_fsm = TX_SER_IDLE;

#if TX_SER_DEBUG
printf("  <NIC TX_SER_GAP> at cycle %d completes packet\n", 
       r_total_cycles.read() );
#endif
            }
            break;
        }
    } // end switch r_tx_ser_fsm

    ////////////////////////////////////////////////////////////////////////////
    // This TX_S2G FSM performs the STREAM to GMII format conversion,
    // computes the checksum, and append this checksum to the ETH/IP/UDP packet.
    // The input is the tx_fifo_stream.
    // The output is the r_backend_tx module.
    ////////////////////////////////////////////////////////////////////////////

    switch(r_tx_s2g_fsm.read())
    {
        /////////////////
        case TX_S2G_IDLE:           // read one byte from stream fifo
        {
            if ( r_tx_fifo_stream.rok() )
            {
                uint32_t data = r_tx_fifo_stream.read();
                uint32_t type = (data >> 8) & 0x3;

                assert ( (type == STREAM_TYPE_SOS) and
                "ERROR in VCI_MASTER_NIC : illegal type received in TX_S2G_IDLE");

                tx_fifo_stream_read = true;
                r_tx_s2g_fsm        = TX_S2G_WRITE_DATA;
                r_tx_s2g_data       = data & 0xFF;
                r_tx_s2g_checksum   = 0x00000000;      // reset checksum register
            }

            // no data written
            r_backend_tx->put( false, 0 );
            break;
        }
        //////////////////////
        case TX_S2G_WRITE_DATA:     // write data[i-1] into gmii_tx
                                    // and read data[i] from fifo_stream
        {
            if ( r_tx_fifo_stream.rok() )
            {
                // write data[i-1]
                r_backend_tx->put( true, r_tx_s2g_data.read() );

                // read data[i]
                uint32_t data = r_tx_fifo_stream.read();
                uint32_t type = (data >> 8) & 0x3;

                assert ( (type != STREAM_TYPE_SOS) and (type != STREAM_TYPE_ERR) and
                "ERROR in VCI_MASTER_NIC : illegal type received in TX_S2G_WRITE_DATA");

                tx_fifo_stream_read = true;
                r_tx_s2g_data       = data & 0xFF;

                // update CRC
                r_tx_s2g_checksum   = m_crc.update( r_tx_s2g_checksum.read(),
                                                    (uint32_t)r_tx_s2g_data.read() );

                r_total_len_tx_gmii = r_total_len_tx_gmii.read() + 1;

                if ( type == STREAM_TYPE_EOS )
                {
                    r_tx_s2g_fsm = TX_S2G_WRITE_LAST_DATA;
                }
            }
            else
            {
                assert (false and
                "ERROR in VCI_MASTER_NIC : tx_fifo should not be empty");
            }
            break;
        }
        ////////////////////////////
        case TX_S2G_WRITE_LAST_DATA:
        {
            // write last data
            r_backend_tx->put( true, r_tx_s2g_data.read() );

            // update CRC
            r_tx_s2g_checksum   = m_crc.update( r_tx_s2g_checksum.read(),
                                                (uint32_t)r_tx_s2g_data.read() );

            r_total_len_tx_gmii = r_total_len_tx_gmii.read() + 1 + m_gap;

            r_tx_s2g_index = 0;
            r_tx_s2g_fsm   = TX_S2G_WRITE_CS;
            break;
        }
        /////////////////////
        case TX_S2G_WRITE_CS:       // write one cs byte into gmii_out
        {
            uint8_t gmii_data;
            if ( r_tx_s2g_index.read() == 0 )
            {
                gmii_data      = r_tx_s2g_checksum.read() & 0xFF;
                r_tx_s2g_index = 1;
            }
            else if ( r_tx_s2g_index.read() == 1 )
            {
                gmii_data      = (r_tx_s2g_checksum.read() >> 8) & 0xFF;
                r_tx_s2g_index = 2;
            }
            else if ( r_tx_s2g_index.read() == 2 )
            {
                gmii_data      = (r_tx_s2g_checksum.read() >> 16) & 0xFF;
                r_tx_s2g_index = 3;
            }
            else // r_tx_s2g_index == 3
            {
                gmii_data      = (r_tx_s2g_checksum.read() >> 24) & 0xFF;
                r_tx_s2g_fsm   = TX_S2G_IDLE;
                r_tx_s2g_checksum = 0x00000000;

#if TX_S2G_DEBUG
printf("  <NIC TX_S2G_WRITE_CS> at cycle %d : completes checksum\n",
       r_total_cycles.read() );
#endif
            }

            r_backend_tx->put( true, gmii_data );
            break;
        }
    } // end switch tx_s2g_fsm

    ///////////////////////////////////////////////////////////////////////
    // These RX_CMA_FSMs define the transfer state for each RX channel.
    // Each FSM implements two nested loops:
    //
    // - In external loop, we get the destination container and status 
    //   addresses, from the external chbuf descriptor, we transfer
    //   a container (internal loop), and release SRC & DST containers.
    // - In the internal loop, 4 bursts of size m_burst_length
    //   are pipelined per iteration.
    //
    // Each RX_CMA_FSM set the r_rx_cma_vci_req[k][b] registers, and
    // the r_rx_cma_vci_req_type[k][b] to request a VCI transaction 
    // to the CMD FSM. The CMD FSM uses the request type to build the 
    // VCI command. The channel index [k] and sub-channel index [b]
    // are transported in the VCI TRDID. The RX/TX type is transported
    // in the VCI PKTID field.
    //
    // The RSP FSM analyses the TRDID & PKTID fields, and the request type
    // to write the data in the relevant register or buffer. It set the 
    // r_rx_cma_vci_rsp[k][b] to signal transaction completion.
    ////////////////////////////////////////////////////////////////////////

    for ( uint32_t k=0 ; k<m_channels ; k++ )
    {
        switch( r_rx_cma_fsm[k].read() )
        {
            /////////////////
            case RX_CMA_IDLE:  // do nothing if SRC container not full
            {
                uint32_t index = r_rx_cma_src_index[k].read();
                if( r_rx_cma_run[k].read() && r_rx_chbuf[k]->full( index ) )
                {
                    r_rx_cma_fsm[k] = RX_CMA_READ_DESC;
                }
            }
            break;

            // get DST buffer and status addresses from DST chbuf descriptor

            //////////////////////
            case RX_CMA_READ_DESC:   // request VCI READ for DST buffer descriptor 
            {
                r_rx_cma_vci_req[k][0]      = true;
                r_rx_cma_vci_req_type[k][0] = REQ_READ_DESC;
                r_rx_cma_fsm[k]             = RX_CMA_READ_DESC_WAIT;
            }
            break;
            ///////////////////////////
            case RX_CMA_READ_DESC_WAIT:  // wait response for DST buffer descriptor
            {
                if ( r_rx_cma_vci_rsp[k][0].read() )
                {
                    r_rx_cma_vci_rsp[k][0] = false;

                    if ( r_rx_cma_vci_rsp_err[k][0].read() )   
                    {
                        std::cout << "DST_DESC_ERROR in VCI_MASTER_NIC for channel "
                        << std::dec << k << " at cycle " << r_total_cycles.read() << std::endl;

                        r_rx_irq[k]     = true;
                        r_rx_cma_fsm[k] = RX_CMA_ERROR;
                        break;
                    }

#if RX_CMA_DEBUG
printf("  <NIC RX_CMA_READ_DESC_WAIT> channel = %d / index = %d / sts = %x / buf = %x / ext = %x\n", 
       k , r_rx_cma_dst_index[k].read() , 
       r_rx_cma_dst_sts[k].read(),
       r_rx_cma_dst_buf[k].read(),
       r_rx_cma_dst_ext[k].read() ); 
#endif
                    // check soft reset
                    if ( r_rx_cma_run[k].read() == false )  r_rx_cma_fsm[k] = RX_CMA_IDLE;
                    else                                    r_rx_cma_fsm[k] = RX_CMA_READ_STATUS;
                }
            }
            break;
            ////////////////////////
            case RX_CMA_READ_STATUS:   // request VCI READ for DST buffer status
            {
                r_rx_cma_vci_req[k][0]      = true;
                r_rx_cma_vci_req_type[k][0] = REQ_READ_STATUS;
                r_rx_cma_fsm[k]             = RX_CMA_READ_STATUS_WAIT;
            }
            break;
            /////////////////////////////
            case RX_CMA_READ_STATUS_WAIT:  // wait response for DST buffer status
            {
                if ( r_rx_cma_vci_rsp[k][0].read() ) 
                {
                    r_rx_cma_vci_rsp[k][0] = false;

                    if ( r_rx_cma_vci_rsp_err[k][0].read() )   
                    {
                        std::cout << "DST_STATUS_ERROR in VCI_MASTER_NIC for channel "
                        << std::dec << k << " at cycle " << r_total_cycles.read() << std::endl;

                        r_rx_irq[k]     = true;
                        r_rx_cma_fsm[k] = RX_CMA_ERROR;
                        break;
                    }

                    if ( r_rx_cma_run[k].read() == false )  // soft reset
                    {
                        r_rx_cma_fsm[k] = RX_CMA_IDLE;
                    }
                    else if ( r_rx_cma_dst_full[k].read() )     // buffer full => delay & retry
                    {
                        r_rx_cma_fsm[k]   = RX_CMA_READ_STATUS_DELAY;
                        r_rx_cma_timer[k] = r_global_period.read();
                    }
                    else                                        // buffer empty => move container
                    {
                        r_rx_cma_data_error[k]  = false;
                        r_rx_cma_bytes_count[k] = 0; 
                        r_rx_cma_burst_count[k] = 0; 
                        r_rx_cma_fsm[k]         = RX_CMA_MOVE_DATA;
                    }
                }
            }
            break;
            //////////////////////////////
            case RX_CMA_READ_STATUS_DELAY:  // delay to access DST buffer status
                                            // return to IDLE in case of soft reset
            {
                if ( r_rx_cma_run[k].read() == false )    // soft reset
                {
                    r_rx_cma_fsm[k] = RX_CMA_IDLE;
                }
                else if ( r_rx_cma_timer[k].read() == 0 ) 
                {
                    r_rx_cma_fsm[k]   = RX_CMA_READ_STATUS;
                }
                else
                {
                    r_rx_cma_timer[k] = r_rx_cma_timer[k].read() - 1;
                }
            }
            break;

            // move data from internal container to DST buffer (internal loop)
            // in each iteration 4 pipelined write transactions 

            //////////////////////
            case RX_CMA_MOVE_DATA:	 // request 4 pipelined WRITE transactions 
                                     // post one request per cycle
            {
                uint32_t b = r_rx_cma_burst_count[k].read();

                // set CMD READ request for (k,b)
                r_rx_cma_vci_req[k][b]      = true;
                r_rx_cma_vci_req_type[k][b] = REQ_WRITE_DATA;

                // test if last READ command sent
                if ( b >= 3 ) r_rx_cma_fsm[k] = RX_CMA_MOVE_DATA_WAIT;
                else          r_rx_cma_burst_count[k] = b + 1;
            }
            break;

            ///////////////////////////
            case RX_CMA_MOVE_DATA_WAIT:  // wait completion of 4 WRITE transactions 
                                         // handle at most one response per cycle
            {
                // scan all burst responses 
                bool  done = false;
                for ( uint32_t b = 0 ; b < 4 ; b++ )
                {
                    if ( r_rx_cma_vci_rsp[k][b].read() ) // response received
                    {
                        // reset RSP for (k,b)
                        r_rx_cma_vci_rsp[k][b] = false;
   
                        // register error reported for (k,b)
                        if ( r_rx_cma_vci_rsp_err[k][b].read() )      
                        {
                            std::cout << "DATA_WRITE_ERROR in VCI_MASTER_NIC for channel "
                            << std::dec << k << " at cycle " << r_total_cycles.read() << std::endl;

                            r_rx_cma_data_error[k] = true;
                        }
                        
                        // test if last response 
                        if ( r_rx_cma_burst_count[k].read() == 0 ) done = true;
                        else  r_rx_cma_burst_count[k] = r_rx_cma_burst_count[k].read() - 1;

                        // exit loop as soon as one response found
                        break;
                    }
                }

                // test if all responses received 
                if ( done )
                {
                    r_rx_cma_bytes_count[k] = r_rx_cma_bytes_count[k].read() + (m_burst_length*4);
                    r_rx_cma_fsm[k]         = RX_CMA_MOVE_DATA_END;
                }
            }
            break;
            //////////////////////////
            case RX_CMA_MOVE_DATA_END:   // check error 
                                         // check if the buffer is completed
            {
                if      ( r_rx_cma_data_error[k].read() )        // error reported
                {
                    r_rx_irq[k] = true;
                    r_rx_cma_fsm[k] = RX_CMA_ERROR;
                }
                else if ( r_rx_cma_bytes_count[k].read() == 4096 )   // buffer completed
                {
                    r_rx_cma_fsm[k] = RX_CMA_WRITE_STATUS;
                }
                else                                              // next multi-burst
                {
                    r_rx_cma_fsm[k] = RX_CMA_MOVE_DATA;
                }
            }
            break;

            // Release SRC & DST buffers and increment buffer indexes 

            /////////////////////////
            case RX_CMA_WRITE_STATUS:  // request VCI transaction to release DST container 
            {
                r_rx_cma_vci_req[k][0]      = true;
                r_rx_cma_vci_req_type[k][0] = REQ_WRITE_STATUS;
                r_rx_cma_fsm[k]             = RX_CMA_WRITE_STATUS_WAIT;
            }
            break;
            //////////////////////////////
            case RX_CMA_WRITE_STATUS_WAIT:   // wait completion of DST release
                                             // and release local SRC container 
            {
                if ( r_rx_cma_vci_rsp[k][0].read() )
                {
                    r_rx_cma_vci_rsp[k][0] = false;

                    if( r_rx_cma_vci_rsp_err[k][0].read() )
                    {
                        r_rx_irq[k]     = true;
                        r_rx_cma_fsm[k] = RX_CMA_ERROR; 
                    }
                    else
                    {
                        r_rx_cma_fsm[k] = RX_CMA_NEXT;
                    }

                    // release local SRC container 
                    rx_chbuf_rcmd[k] = RX_CHBUF_RCMD_RELEASE;
                    rx_chbuf_cont[k] = r_rx_cma_src_index[k].read();
                }
            }
            break;

            /////////////////
            case RX_CMA_NEXT:  // update SRC & DST containers index
            {
                // update SRC index
                r_rx_cma_src_index[k] = 1 - r_rx_cma_src_index[k].read();

                // update DST index
                if ( r_rx_cma_dst_index[k].read() == (r_rx_cma_dst_nbufs[k].read() - 1) )
                {
                    r_rx_cma_dst_index[k] = 0;
                }
                else
                {
                    r_rx_cma_dst_index[k] = r_rx_cma_dst_index[k].read() + 1;
                } 

                // signal container transfer completion
                r_rx_irq[k] = true;
                r_rx_cma_fsm[k] = RX_CMA_IDLE;
            }
            break; 

            //////////////////
            case RX_CMA_ERROR:
            {
                if ( r_rx_cma_run[k].read() == false )	 // soft reset
                {
                    r_rx_cma_fsm[k] = RX_CMA_IDLE;
                }
            }
            break; 
        }
    } // end switch r_rx_cma_fsm[k]
        
                
    ///////////////////////////////////////////////////////////////////////
    // These TX_CMA_FSMs define the transfer state for each TX channel.
    // Each FSM implements two nested loops:
    //
    // - In external loop, we get the source container and status 
    //   addresses, from the external chbuf descriptor, we transfer
    //   a container (internal loop), and release SRC & DST containers.
    // - In the internal loop, 4 bursts of size m_burst_length
    //   are pipelined per iteration. 
    //
    // Each TX_CMA_FSM set the r_tx_cma_vci_req[k][b] registers, and
    // the r_tx_cma_vci_req_type[k][b] to request a VCI transaction 
    // to the CMD FSM. The CMD FSM uses the request type to build the 
    // VCI command. The channel index [k] and sub-channel index [b]
    // are transported in the VCI TRDID. The RX/TX type is transported
    // in the VCI PKTID field.
    //
    // The RSP FSM analyses the TRDID & PKTID fields, and the request type
    // to write the data in the relevant register or buffer. It set the 
    // r_tx_cma_vci_rsp[k][b] to signal transaction completion.
    ////////////////////////////////////////////////////////////////////////

    for ( uint32_t k=0 ; k<m_channels ; k++ )
    {
        switch( r_tx_cma_fsm[k].read() )
        {
            /////////////////
            case TX_CMA_IDLE:  // do nothing if DST container not empty
            {
                uint32_t index = r_tx_cma_dst_index[k].read();
                if( r_tx_cma_run[k].read() && (r_tx_chbuf[k]->full( index ) == false) )
                {
                    r_tx_cma_fsm[k] = TX_CMA_READ_DESC;
                }
            }
            break;

            // get SRC buffer and status addresses from SRC chbuf descriptor

            //////////////////////
            case TX_CMA_READ_DESC:   // request VCI READ for SRC buffer descriptor 
            {
                r_tx_cma_vci_req[k][0]      = true;
                r_tx_cma_vci_req_type[k][0] = REQ_READ_DESC;
                r_tx_cma_fsm[k]             = TX_CMA_READ_DESC_WAIT;
            }
            break;
            ///////////////////////////
            case TX_CMA_READ_DESC_WAIT:  // wait response for SRC buffer descriptor
            {
                if ( r_tx_cma_vci_rsp[k][0].read() )
                {
                    r_tx_cma_vci_rsp[k][0] = false;

                    if ( r_tx_cma_vci_rsp_err[k][0].read() )   
                    {
                        std::cout << "DST_DESC_ERROR in VCI_MASTER_NIC for channel "
                        << std::dec << k << " at cycle " << r_total_cycles.read() << std::endl;

                        r_tx_irq[k]     = true;
                        r_tx_cma_fsm[k] = TX_CMA_ERROR;
                        break;
                    }

                    // check soft reset
                    if ( r_tx_cma_run[k].read() == false )  r_tx_cma_fsm[k] = TX_CMA_IDLE;
                    else                                 r_tx_cma_fsm[k] = TX_CMA_READ_STATUS;
                }
            }
            break;
            ////////////////////////
            case TX_CMA_READ_STATUS:   // request VCI READ for SRC buffer status
            {
                r_tx_cma_vci_req[k][0]      = true;
                r_tx_cma_vci_req_type[k][0] = REQ_READ_STATUS;
                r_tx_cma_fsm[k]             = TX_CMA_READ_STATUS_WAIT;
            }
            break;
            /////////////////////////////
            case TX_CMA_READ_STATUS_WAIT:  // wait response for SRC buffer status
            {
                if ( r_tx_cma_vci_rsp[k][0].read() ) 
                {
                    r_tx_cma_vci_rsp[k][0] = false;

                    if ( r_tx_cma_vci_rsp_err[k][0].read() )   
                    {
                        std::cout << "DST_STATUS_ERROR in VCI_MASTER_NIC for channel "
                        << std::dec << k << " at cycle " << r_total_cycles.read() << std::endl;

                        r_tx_irq[k]     = true;
                        r_tx_cma_fsm[k] = TX_CMA_ERROR;
                        break;
                    }

                    if ( r_tx_cma_run[k].read() == false )  // soft reset
                    {
                        r_tx_cma_fsm[k] = TX_CMA_IDLE;
                    }
                    else if ( r_tx_cma_src_full[k].read() == 0 )  // buffer empty => delay & retry
                    {
                        r_tx_cma_fsm[k]   = TX_CMA_READ_STATUS_DELAY;
                        r_tx_cma_timer[k] = r_global_period.read();
                    }
                    else                                          // buffer full => move container
                    {
                        r_tx_cma_data_error[k]  = false;
                        r_tx_cma_bytes_count[k] = 0; 
                        r_tx_cma_burst_count[k] = 0; 
                        r_tx_cma_fsm[k]         = TX_CMA_MOVE_DATA;
                    }
                }
            }
            break;
            //////////////////////////////
            case TX_CMA_READ_STATUS_DELAY:  // delay to access DST buffer status
                                            // return to IDLE in case of soft reset
            {
                if ( r_tx_cma_run[k].read() == false )    // soft reset
                {
                    r_tx_cma_fsm[k] = TX_CMA_IDLE;
                }
                else if ( r_tx_cma_timer[k].read() == 0 ) 
                {
                    r_tx_cma_fsm[k]   = TX_CMA_READ_STATUS;
                }
                else
                {
                    r_tx_cma_timer[k] = r_tx_cma_timer[k].read() - 1;
                }
            }
            break;

            // move data from internal container to DST buffer (internal loop)
            // in each iteration 4 pipelined write transactions 

            //////////////////////
            case TX_CMA_MOVE_DATA:	 // request 4 pipelined READ transactions 
                                     // post one request per cycle
            {
                uint32_t b = r_tx_cma_burst_count[k].read();

                // set CMD READ request for (k,b)
                r_tx_cma_vci_req[k][b]      = true;
                r_tx_cma_vci_req_type[k][b] = REQ_READ_DATA;

                // test if last READ command sent
                if ( b >= 3 ) r_tx_cma_fsm[k] = TX_CMA_MOVE_DATA_WAIT;
                else          r_tx_cma_burst_count[k] = b + 1;
            }
            break;

            ///////////////////////////
            case TX_CMA_MOVE_DATA_WAIT:  // wait completion of 4 READ transactions 
                                         // handle at most one response per cycle
            {
                // scan all burst responses 
                bool  done = false;
                for ( uint32_t b = 0 ; b < 4 ; b++ )
                {
                    if ( r_tx_cma_vci_rsp[k][b].read() ) // response received
                    {
                        // reset RSP for (k,b)
                        r_tx_cma_vci_rsp[k][b] = false;
   
                        // register error reported for (k,b)
                        if ( r_tx_cma_vci_rsp_err[k][b].read() )      
                        {
                            std::cout << "DATA_WRITE_ERROR in VCI_MASTER_NIC for channel "
                            << std::dec << k << " at cycle " << r_total_cycles.read() << std::endl;
                            r_tx_cma_data_error[k] = true;
                        }
                        
                        // test if last WRITE response 
                        if ( r_tx_cma_burst_count[k].read() == 0 ) done = true;
                        else  r_tx_cma_burst_count[k] = r_tx_cma_burst_count[k].read() - 1;

                        // exit loop as soon as one response found
                        break;
                    }
                }

                // test if all responses received 
                if ( done )
                {
                    r_tx_cma_bytes_count[k] = r_tx_cma_bytes_count[k].read() + (m_burst_length*4);
                    r_tx_cma_fsm[k]         = TX_CMA_MOVE_DATA_END;
                }
            }
            break;
            //////////////////////////
            case TX_CMA_MOVE_DATA_END:  // check error 
                                        // check if the buffer is completed
            {
                if      ( r_tx_cma_data_error[k].read() )        // error reported
                {
                    r_tx_irq[k] = true;
                    r_tx_cma_fsm[k] = TX_CMA_ERROR;
                }
                else if ( r_tx_cma_bytes_count[k].read() == 4096 )   // buffer completed
                {
                    r_tx_cma_fsm[k] = TX_CMA_WRITE_STATUS;

#if TX_CMA_DEBUG
printf("  <NIC TX_CMA_MOVE_DATA_END> at cycle %d for channel %d\n",  
       r_total_cycles.read(), k ); 
r_tx_chbuf[k]->print_trace( k , 64 );
#endif

                }
                else                                              // next multi-burst
                {
                    r_tx_cma_fsm[k] = TX_CMA_MOVE_DATA;
                }
            }
            break;

            // Release SRC & DST buffers and increment buffer indexes 

            /////////////////////////
            case TX_CMA_WRITE_STATUS:  // request VCI transaction to release SRC container 
            {
                r_tx_cma_vci_req[k][0]      = true;
                r_tx_cma_vci_req_type[k][0] = REQ_WRITE_STATUS;
                r_tx_cma_fsm[k]             = TX_CMA_WRITE_STATUS_WAIT;
            }
            break;
            //////////////////////////////
            case TX_CMA_WRITE_STATUS_WAIT:   // wait completion of SRC release
                                             // and release local DST container
            {
                if ( r_tx_cma_vci_rsp[k][0].read() )
                {
                    r_tx_cma_vci_rsp[k][0] = false;

                    if( r_tx_cma_vci_rsp_err[k][0].read() )
                    {
                        r_tx_irq[k]     = true;
                        r_tx_cma_fsm[k] = TX_CMA_ERROR; 
                    }
                    else
                    {
                        r_tx_cma_fsm[k] = TX_CMA_NEXT;
                    }

                    // release local DST container 
                    tx_chbuf_wcmd[k] = TX_CHBUF_WCMD_RELEASE;
                    tx_chbuf_cont[k] = r_tx_cma_dst_index[k].read();
                }
            }
            break;

            /////////////////
            case TX_CMA_NEXT:  // update SRC & DST containers index
            {
                // update DST index
                r_tx_cma_dst_index[k] = 1 - r_tx_cma_dst_index[k].read();

                // update SRC index
                if ( r_tx_cma_src_index[k].read() == (r_tx_cma_src_nbufs[k].read() - 1) )
                {
                    r_tx_cma_src_index[k] = 0;
                }
                else
                {
                    r_tx_cma_src_index[k] = r_tx_cma_src_index[k].read() + 1;
                } 

                // signal container transfer completion
                r_tx_irq[k] = true;
                r_tx_cma_fsm[k] = TX_CMA_IDLE;
            }
            break;

            //////////////////
            case TX_CMA_ERROR:
            {
                if ( r_tx_cma_run[k].read() == false )	 // soft reset
                {
                    r_tx_cma_fsm[k] = TX_CMA_IDLE;
                }
            }
            break; 
        }
    } // end switch r_tx_cma_fsm[k]
        
                
    ////////////////////////////////////////////////////////////////////////////
    // This CMD_FSM controls the VCI INIT command port.
    // The clients are the m_channels RX_CMA_FSMs & the m_channels TX_CMA_FSMs.
    // Arbitration is done in the CMD_IDLE state, and the command is actually 
    // sent in the others READ/WRITE states.
    ////////////////////////////////////////////////////////////////////////////
    switch(r_cmd_fsm.read()) 
    {
        //////////////
        case CMD_IDLE:   // make arbitration but don't send command in this state
        {
            // round-robin arbitration between RX and TX channels to send a command
            bool not_found = true;

            // loop on channels
            for( uint32_t n = 0 ; (n < m_channels) and not_found ; n++ )
            {
                uint32_t k = (r_cmd_channel.read() + n) % m_channels;

                // loop on bursts
                for ( uint32_t b = 0 ; (b < 4) and not_found ; b++ )
                {
                    if ( r_rx_cma_vci_req[k][b].read() )  // pending RX request
                    {
                        not_found               = false;
                        r_cmd_channel           = k;
                        r_cmd_burst             = b;
                        r_cmd_is_rx             = true;
                        r_cmd_bytes             = 0;

                        switch ( r_rx_cma_vci_req_type[k][b].read() )
                        {
                            case REQ_READ_DESC:
                            {
                                r_cmd_address = r_rx_cma_dst_desc[k].read() + 
                                                (r_rx_cma_dst_index[k].read() << 3);
                                r_cmd_type    = REQ_READ_DESC;
                                r_cmd_fsm     = CMD_READ;
                            }
                            break;
                            case REQ_READ_STATUS:
                            {
                                r_cmd_address = ((uint64_t)r_rx_cma_dst_ext[k].read() << 32) +
                                                r_rx_cma_dst_sts[k].read();
                                r_cmd_type    = REQ_READ_STATUS;
                                r_cmd_fsm     = CMD_READ;
                            }
                            break;
                            case REQ_WRITE_DATA:
                            {
                                r_cmd_address = ((uint64_t)r_rx_cma_dst_ext[k].read() << 32) +
                                                r_rx_cma_dst_buf[k].read() +
                                                r_rx_cma_bytes_count[k].read() +
                                                b*m_burst_length;
                                r_cmd_type    = REQ_WRITE_DATA; 
                                r_cmd_fsm     = CMD_WRITE_DATA;
              
                                // read first word (32/64 bits) from local chbuf
                                rx_chbuf_rcmd[k] = RX_CHBUF_RCMD_READ;
                                rx_chbuf_cont[k] = r_rx_cma_src_index[k].read();
                                rx_chbuf_word[k] = ( r_rx_cma_bytes_count[k].read() +
                                                     b*m_burst_length ) >> 2;
                            }
                            break;
                            case REQ_WRITE_STATUS:
                            {
                                r_cmd_address = ((uint64_t)r_rx_cma_dst_ext[k].read() << 32) +
                                                r_rx_cma_dst_sts[k].read();
                                r_cmd_type    = REQ_WRITE_STATUS;
                                r_cmd_fsm     = CMD_WRITE_STATUS;
                            }
                            break;
                        } // end switch on req_type
                    } // end RX request
                    else if ( r_tx_cma_vci_req[k][b].read() )  // pending TX request
                    {
                        not_found               = false;
                        r_cmd_channel           = k;
                        r_cmd_burst             = b;
                        r_cmd_is_rx             = false;
                        r_cmd_bytes             = 0;

                        switch ( r_tx_cma_vci_req_type[k][b].read() )
                        {
                            case REQ_READ_DESC:
                            {
                                r_cmd_address = r_tx_cma_src_desc[k].read() + 
                                                (r_tx_cma_src_index[k].read() << 3);
                                r_cmd_type    = REQ_READ_DESC;
                                r_cmd_fsm     = CMD_READ;
                            }
                            break;
                            case REQ_READ_STATUS:
                            {
                                r_cmd_address = ((uint64_t)r_tx_cma_src_ext[k].read() << 32) +
                                                r_tx_cma_src_sts[k].read();
                                r_cmd_type    = REQ_READ_STATUS;
                                r_cmd_fsm     = CMD_READ;
                            }
                            break;
                            case REQ_READ_DATA:
                            {
                                r_cmd_address = ((uint64_t)r_tx_cma_src_ext[k].read() << 32) +
                                                r_tx_cma_src_buf[k].read() +
                                                r_tx_cma_bytes_count[k].read() +
                                                b*m_burst_length;
                                r_cmd_type    = REQ_READ_DATA;
                                r_cmd_fsm     = CMD_READ;
                            }
                            break;
                            case REQ_WRITE_STATUS:
                            {
                                r_cmd_address = ((uint64_t)r_tx_cma_src_ext[k].read() << 32) +
                                                r_tx_cma_src_sts[k].read();
                                r_cmd_type    = REQ_WRITE_STATUS;
                                r_cmd_fsm     = CMD_WRITE_STATUS;
                            }
                            break;
                        } // end switch on req_type 
                    } // end TX request
                } // end for sub-channels
            } // end for channels
            break;
        }
        //////////////
        case CMD_READ:        // This VCI command is always one single flit
        {
            if ( p_vci_ini.cmdack.read() )
            {
                uint32_t k     = r_cmd_channel.read();
                uint32_t b     = r_cmd_burst.read();
                bool     is_rx = r_cmd_is_rx.read();

                if( is_rx ) r_rx_cma_vci_req[k][b] = false;
                else        r_tx_cma_vci_req[k][b] = false;
                r_cmd_fsm = CMD_IDLE;
            }
        }
        break;
        //////////////////////
        case CMD_WRITE_STATUS:  // This VCI command is one single flit
        {
            if ( p_vci_ini.cmdack.read() )
            {
                uint32_t k     = r_cmd_channel.read();
                uint32_t b     = r_cmd_burst.read();
                bool     is_rx = r_cmd_is_rx.read();

                if( is_rx ) r_rx_cma_vci_req[k][b] = false;
                else        r_tx_cma_vci_req[k][b] = false;
                r_cmd_fsm = CMD_IDLE;
            }
        }
        break;
        /////////////////////
        case CMD_WRITE_DATA:   // This VCI command is multi-flits : at each cycle, 
                               // we move word[i-1] from local RX_CHBUF to VCI port
                               // and we send address for word[i] to local RX_CHBUF
        {
            if ( p_vci_ini.cmdack.read() )
            {
                uint32_t k     = r_cmd_channel.read();
                uint32_t b     = r_cmd_burst.read();

                assert( (r_cmd_is_rx.read() == true ) and
                "ERROR in VCI_MASTER_NIC : WRITE in a TX QUEUE...");
        
                // compute number of bytes per flit
                uint32_t nbytes;
                if (vci_param::B==4)  nbytes = 4;
                else                  nbytes = 8;

                // test last flit in burst
                if ( r_cmd_bytes.read() == (m_burst_length - nbytes) )  // last flit
                {
                    r_rx_cma_vci_req[k][b] = false;
                    r_cmd_fsm = CMD_IDLE;
                }
                else                                     // not the last flit
                {
                    // read word[i] (32/64 bits) from local chbuf
                    rx_chbuf_rcmd[k] = RX_CHBUF_RCMD_READ;
                    rx_chbuf_cont[k] = r_rx_cma_src_index[k].read();
                    rx_chbuf_word[k] = ( r_rx_cma_bytes_count[k].read() +
                                         b*m_burst_length + r_cmd_bytes.read() + nbytes ) >> 2;
                }

                // update bytes count
                r_cmd_bytes = r_cmd_bytes.read() + nbytes;
            }
        }
        break;
    } // end switch cmd_fsm

    ///////////////////////////////////////////////////////////////////////////
    // This RSP_FSM controls the VCI INIT response port.
    // It get the channel and burst indexes [k][b] from the VCI TRDID.
    // It get the the RX/TX direction from VCI SRCID.
    // IT writes into the relevant register or buffer if required.
    // It set r_**_cma_vci_rsp[k][b], r_**_cma_vci_rsp_err[k][b] bits
    // to signal the VCI transaction completion.
    ///////////////////////////////////////////////////////////////////////////
    switch(r_rsp_fsm.read()) 
    {
        //////////////
        case RSP_IDLE:
        {
            if ( p_vci_ini.rspval.read() )
            {
                uint32_t k     = (uint32_t)((p_vci_ini.rtrdid.read()>>2) & 0x3);
                uint32_t b     = (uint32_t)(p_vci_ini.rtrdid.read() & 0x3);
                bool     is_rx = (p_vci_ini.rsrcid.read() == m_rx_srcid);

                r_rsp_channel = k;
                r_rsp_burst   = b;
                r_rsp_is_rx   = is_rx;
                r_rsp_bytes   = 0;

                if( is_rx )  // RX transaction
                {
                    if      ( r_rx_cma_vci_req_type[k][b].read() == REQ_READ_DESC )
                    {
                        r_rsp_fsm = RSP_READ_DESC;
                    }
                    else if ( r_rx_cma_vci_req_type[k][b].read() == REQ_READ_STATUS )
                    {
                        r_rsp_fsm = RSP_READ_STATUS;
                    }
                    else if ( r_rx_cma_vci_req_type[k][b].read() == REQ_WRITE_DATA )
                    {
                        r_rsp_fsm = RSP_WRITE; 
                    }
                    else  // r_rx_cma_vci_req_type[k][b] == REQ_WRITE_STATUS 
                    {
                        r_rsp_fsm = RSP_WRITE;
                    }
                }
                else         // TX transaction
                {
                    if      ( r_tx_cma_vci_req_type[k][b].read() == REQ_READ_DESC )
                    {
                        r_rsp_fsm = RSP_READ_DESC;
                    }
                    else if ( r_tx_cma_vci_req_type[k][b].read() == REQ_READ_STATUS )
                    {
                        r_rsp_fsm = RSP_READ_STATUS;
                    }
                    else if ( r_tx_cma_vci_req_type[k][b].read() == REQ_READ_DATA )
                    {
                        r_rsp_fsm = RSP_READ_DATA; 
                    }
                    else  // r_tx_cma_vci_req_type[k][b] == REQ_WRITE_STATUS 
                    {
                        r_rsp_fsm = RSP_WRITE;
                    }
                }
            }
        } 
        break;
        ///////////////////
        case RSP_READ_DESC:  // set status and buffer physical addresses
        {
            if ( p_vci_ini.rspval.read() )
            {
                uint32_t k     = r_rsp_channel.read();
                bool     is_rx = r_rsp_is_rx.read();

                if (vci_param::B==4)   // VCI DATA on 32 bits
                {
                    uint32_t rdata = (uint32_t)p_vci_ini.rdata.read();

                    if ( r_rsp_bytes.read() == 0 ) // read bits[31:0] of descriptor
                    {
                        // bits[31:0] of buffer descriptor are bits[31:0] of status address
                        if( is_rx ) r_rx_cma_dst_sts[k] = rdata;
                        else        r_tx_cma_src_buf[k] = rdata;
                    }
                    else                          // read bits[63:32] of descriptor
                    {
                        // bits[63:52] of buffer descriptor (<=> bits [31:20] of rdata) are
                        // address extension of buffer address and buffer status address
                        if( is_rx ) r_rx_cma_dst_ext[k] = (rdata >> 20);
                        else        r_tx_cma_src_ext[k] = (rdata >> 20);

                        // bits[51:32] of buffer descriptor (<=> bits [19:0] of rdata) are
                        // bits[31:12] of buffer address
                        if( is_rx ) r_rx_cma_dst_buf[k] = (rdata << 12);
                        else        r_tx_cma_src_buf[k] = (rdata << 12);
                    }

                    r_rsp_bytes = r_rsp_bytes.read() + 4;
                }
                else                  // VCI DATA on 64 bits
                {
                    uint64_t rdata = (uint64_t)p_vci_ini.rdata.read();

                    // bits[63:52] of buffer descriptor are address extension of buffer address
                    // and buffer status address
                    if( is_rx ) r_rx_cma_dst_ext[k] = (rdata >> 52);
                    else        r_tx_cma_src_ext[k] = (rdata >> 52);

                    // bits[51:32] of buffer descriptor are bits[31:12] of buffer address
                    if( is_rx ) r_rx_cma_dst_buf[k] = (rdata & 0x000FFFFF00000000ULL) >> 20;
                    else        r_tx_cma_src_buf[k] = (rdata & 0x000FFFFF00000000ULL) >> 20;

                    // bits[31:0] of buffer descriptor are bits[31:0] of buffer status address
                    if( is_rx ) r_rx_cma_dst_sts[k] = rdata;
                    else        r_tx_cma_src_sts[k] = rdata;
                }

                if ( p_vci_ini.reop.read() )
                {
                    if( is_rx )
                    {
                        r_rx_cma_vci_rsp[k][0] = true;
                        r_rx_cma_vci_rsp_err[k][0] = ((p_vci_ini.rerror.read()&0x1) != 0);
                    }
                    else
                    {
                        r_tx_cma_vci_rsp[k][0] = true;
                        r_tx_cma_vci_rsp_err[k][0] = ((p_vci_ini.rerror.read()&0x1) != 0);
                    }

                    r_rsp_fsm = RSP_IDLE;
                } 
            }
        } 
        break;
        /////////////////////
        case RSP_READ_STATUS:   // set container status
        {
            if ( p_vci_ini.rspval.read() )
            {
                uint32_t k     = r_rsp_channel.read();
                bool     is_rx = r_rsp_is_rx.read();
                uint32_t rdata = (uint32_t)p_vci_ini.rdata.read();

                if( is_rx ) 
                {
                    r_rx_cma_dst_full[k]        = (rdata != 0);
                    r_rx_cma_vci_rsp[k][0]      = true;
                    r_rx_cma_vci_rsp_err[k][0]  = ((p_vci_ini.rerror.read()&0x1) != 0);
                }
                else
                {
                    r_tx_cma_src_full[k]        = (rdata != 0);
                    r_tx_cma_vci_rsp[k][0]      = true;
                    r_tx_cma_vci_rsp_err[k][0]  = ((p_vci_ini.rerror.read()&0x1) != 0);
                }

                r_rsp_fsm = RSP_IDLE;
            }
        }
        break;
        ///////////////////
        case RSP_READ_DATA:
        {
            if ( p_vci_ini.rspval.read() )
            {
                uint32_t k     = r_rsp_channel.read();  
                uint32_t b     = r_rsp_burst.read();   
                bool     is_rx = r_rsp_is_rx.read();
                
                assert( (is_rx == false ) and
                "ERROR in VCI_MASTER_NIC : READ in a RX QUEUE...");

                tx_chbuf_cont[k] = r_tx_cma_dst_index[k].read();
                tx_chbuf_word[k] = ( r_tx_cma_bytes_count[k].read() +
                                     b*m_burst_length +
                                     r_rsp_bytes.read() ) >> 2;

                if (vci_param::B==4)   // VCI DATA on 32 bits
                {
                    tx_chbuf_wdata0[k] = p_vci_ini.rdata.read();
                    tx_chbuf_wcmd[k]   = TX_CHBUF_WCMD_WRITE;
                    r_rsp_bytes        = r_rsp_bytes.read() + 4;
                }
                else                   // VCI DATA on 64 bits 
                {
                    tx_chbuf_wdata0[k] = (uint32_t)p_vci_ini.rdata.read();
                    tx_chbuf_wdata1[k] = (uint32_t)(p_vci_ini.rdata.read()>>32);
                    tx_chbuf_wcmd[k]   = TX_CHBUF_WCMD_WRITE_64;
                    r_rsp_bytes        = r_rsp_bytes.read() + 8;
                }

                if ( p_vci_ini.reop.read() )
                {
                    r_tx_cma_vci_rsp[k][b]      = true;
                    r_tx_cma_vci_rsp_err[k][b]  = ((p_vci_ini.rerror.read()&0x1) != 0);
                    r_rsp_fsm                    = RSP_IDLE;
                }
            }
        }
        break; 
        ///////////////
        case RSP_WRITE:
        {
            if ( p_vci_ini.rspval.read() )
            {
                assert( (p_vci_ini.reop.read() == true) and
                "VCI_MASTER_NIC error : write response packed contains more than one flit");  

                uint32_t k     = r_rsp_channel.read();
                uint32_t b     = r_rsp_burst.read();
                bool     is_rx = r_rsp_is_rx.read();

                if( is_rx )
                {
                    r_rx_cma_vci_rsp[k][b]      = true;
                    r_rx_cma_vci_rsp_err[k][b]  = ((p_vci_ini.rerror.read()&0x1) != 0);
                }
                else
                {
                    r_tx_cma_vci_rsp[k][b]      = true;
                    r_tx_cma_vci_rsp_err[k][b]  = ((p_vci_ini.rerror.read()&0x1) != 0);
                }

                r_rsp_fsm                    = RSP_IDLE;
            }
        }
        break;
    } // end switch rsp_fsm

    //////////////////////////////////////////////////////////////////////////////////////
    // Finally we update the various FIFOs and the embedded CHBUFs
    //////////////////////////////////////////////////////////////////////////////////////

    r_rx_fifo_multi.update( rx_fifo_multi_wcmd,
                            rx_fifo_multi_rcmd,
                            rx_fifo_multi_wdata,
                            rx_fifo_multi_padding );

    r_tx_fifo_multi.update( tx_fifo_multi_wcmd,
                            tx_fifo_multi_rcmd,
                            tx_fifo_multi_wdata,
                            tx_fifo_multi_padding );

    r_rx_fifo_stream.update( rx_fifo_stream_read,
                             rx_fifo_stream_write,
                             rx_fifo_stream_wdata );

    r_tx_fifo_stream.update( tx_fifo_stream_read,
                             tx_fifo_stream_write,
                             tx_fifo_stream_wdata );

    // update rx_chbuf for all channels

    for ( size_t k = 0 ; k < m_channels ; k++ )
    {
        r_rx_chbuf[k]->update( rx_chbuf_wcmd[k],
                               rx_chbuf_wdata[k],
                               rx_chbuf_plen[k],
                               rx_chbuf_rcmd[k],
                               rx_chbuf_cont[k],
                               rx_chbuf_word[k] );
    }

    // update tx_chbuf for all channels

    for ( size_t k = 0 ; k < m_channels ; k++ )
    {
        r_tx_chbuf[k]->update( tx_chbuf_wcmd[k],
                               tx_chbuf_wdata0[k],
                               tx_chbuf_wdata1[k],
                               tx_chbuf_cont[k],
                               tx_chbuf_word[k],
                               tx_chbuf_rcmd[k] );
    }
} // end transition


//////////////////////
tmpl(void)::genMoore()
{
    ///////////  Interrupts ////////

    for ( size_t k = 0 ; k < m_channels ; k++ )
    {
        p_rx_irq[k] =  r_rx_irq[k].read();
        p_tx_irq[k] =  r_tx_irq[k].read();
    }

    /////// VCI INIT CMD port //////

    switch( r_cmd_fsm.read() ) 
    {
        case CMD_IDLE:
        {   
            p_vci_ini.cmdval  = false;
            p_vci_ini.address = 0;
            p_vci_ini.wdata   = 0;
            p_vci_ini.be      = 0;
            p_vci_ini.plen    = 0;
            p_vci_ini.cmd     = vci_param::CMD_WRITE;
            p_vci_ini.trdid   = 0;
            p_vci_ini.pktid   = 0;
            p_vci_ini.srcid   = 0;
            p_vci_ini.cons    = false;
            p_vci_ini.wrap    = false;
            p_vci_ini.contig  = false;
            p_vci_ini.clen    = 0;
            p_vci_ini.cfixed  = false;
            p_vci_ini.eop     = false;
        }
        break;
        case CMD_READ:
        {   
            uint32_t k     = r_cmd_channel.read();
            uint32_t b     = r_cmd_burst.read();
            bool     is_rx = r_cmd_is_rx.read();

            uint32_t be;
            uint32_t trdid;
            uint32_t srcid;
            uint32_t plen;

            trdid = (((k & 0x3)<<2) | (b & 0x3));	

            if( r_cmd_type.read() == REQ_READ_DESC   ) plen = 8;
            if( r_cmd_type.read() == REQ_READ_STATUS ) plen = 4;
            if( r_cmd_type.read() == REQ_READ_DATA   ) plen = m_burst_length;
            
            if( (vci_param::B == 4) or (plen == 4) ) be = 0x0F;                                    
            else                                     be = 0xFF;
            
            if( is_rx ) srcid = m_rx_srcid;
            else        srcid = m_tx_srcid;

            p_vci_ini.cmdval  = true;
            p_vci_ini.address = (typename vci_param::fast_addr_t)r_cmd_address.read();
            p_vci_ini.wdata   = 0;
            p_vci_ini.be      = (typename vci_param::be_t)be;      
            p_vci_ini.plen    = (typename vci_param::plen_t)plen;              
            p_vci_ini.cmd     = vci_param::CMD_READ;
            p_vci_ini.trdid   = (typename vci_param::trdid_t)trdid;
            p_vci_ini.pktid   = 0;
            p_vci_ini.srcid   = (typename vci_param::srcid_t)srcid;
            p_vci_ini.cons    = false;
            p_vci_ini.wrap    = false;
            p_vci_ini.contig  = true;
            p_vci_ini.clen    = 0;
            p_vci_ini.cfixed  = false;
            p_vci_ini.eop     = true;
        }
        break;
        case CMD_WRITE_DATA:
        case CMD_WRITE_STATUS:
        {
            uint32_t k     = r_cmd_channel.read();
            uint32_t b     = r_cmd_burst.read(); 
            bool     is_rx = r_cmd_is_rx.read();

            uint32_t srcid;
            uint32_t trdid;
            uint32_t plen; 
            uint32_t be;
            uint64_t wdata;
            bool     eop;
            
            trdid = (((k & 0x3)<<2) | (b & 0x3));	

            if( is_rx ) srcid = m_rx_srcid;
            else        srcid = m_tx_srcid;

            if (vci_param::B == 4)                           // Data width 32 bits
            {  
                if ( r_cmd_type.read() == REQ_WRITE_STATUS ) 
                {
                    plen  = 4;
                    be    = 0xF;
                    eop   = true;
                    if( is_rx ) wdata = 0x1;     // RX container full
                    else        wdata = 0x0;     // TX container empty
                }
                else //  r_cmd_type.read() == REQ_WRITE_DATA 
                {
                    plen  = m_burst_length;
                    be    = 0xF;
                    eop   = ( r_cmd_bytes.read() == plen - 4 );
                    wdata = r_rx_chbuf[k]->data();
                }
            }
            else                                           // Data width 64 bits
            {
                if( r_cmd_type.read() == REQ_WRITE_STATUS ) 
                {
                    plen  = 4;
                    be    = 0x0F;
                    eop   = true;
                    if( is_rx ) wdata = 0x1;     // RX container full
                    else        wdata = 0x0;     // TX container empty
                }
                else //  r_cmd_type.read() == REQ_WRITE_DATA 
                {
                    plen  = m_burst_length;
                    be    = 0xFF;
                    eop   = ( r_cmd_bytes.read() == plen - 8 );
                    wdata = r_rx_chbuf[k]->data64();
                }
            }

            p_vci_ini.cmdval  = true;
            p_vci_ini.address = (typename vci_param::fast_addr_t)r_cmd_address.read() +
                                                                       r_cmd_bytes.read();
            p_vci_ini.wdata   = (typename vci_param::data_t)wdata;
            p_vci_ini.be      = (typename vci_param::be_t)be;      
            p_vci_ini.plen    = (typename vci_param::plen_t)plen;              
            p_vci_ini.cmd     = vci_param::CMD_WRITE;
            p_vci_ini.trdid   = (typename vci_param::trdid_t)trdid;
            p_vci_ini.pktid   = 0;
            p_vci_ini.srcid   = (typename vci_param::srcid_t)srcid;
            p_vci_ini.cons    = false;
            p_vci_ini.wrap    = false;
            p_vci_ini.contig  = true;
            p_vci_ini.clen    = 0;
            p_vci_ini.cfixed  = false;
            p_vci_ini.eop     = eop;
        }
        break;
    } // end switch cmd_fsm

    ////// VCI INI RSP port //////

    if ( r_rsp_fsm.read() == RSP_IDLE ) p_vci_ini.rspack = false;
    else                                p_vci_ini.rspack = true;

    ////// VCI TARGET port ///////

    switch( r_tgt_fsm.read() ) 
    {
        case TGT_IDLE:
        {
            p_vci_tgt.cmdack = true;
            p_vci_tgt.rspval = false;
            break;
        }
        case TGT_WRITE_GLOBAL_REG:
        case TGT_WRITE_CHANNEL_REG:
        {
            p_vci_tgt.cmdack = false;
            p_vci_tgt.rspval = true;
            p_vci_tgt.rdata  = 0;
            p_vci_tgt.rerror = vci_param::ERR_NORMAL;
            p_vci_tgt.rsrcid = r_tgt_srcid.read();
            p_vci_tgt.rtrdid = r_tgt_trdid.read();
            p_vci_tgt.rpktid = r_tgt_pktid.read();
            p_vci_tgt.reop   = true;
            break;
        }
        case TGT_READ_GLOBAL_REG:
        case TGT_READ_CHANNEL_REG:
        {
            uint32_t rdata;
            if ( r_tgt_fsm.read() == TGT_READ_GLOBAL_REG )
            {
                rdata = read_global_register( r_tgt_address.read() );
            }
            else
            {
                rdata = read_channel_register( r_tgt_address.read() );
            }

            p_vci_tgt.cmdack = false;
            p_vci_tgt.rspval = true;
            p_vci_tgt.rerror = vci_param::ERR_NORMAL;
            p_vci_tgt.rdata  = (typename vci_param::data_t)rdata;
            p_vci_tgt.rsrcid = r_tgt_srcid.read();
            p_vci_tgt.rtrdid = r_tgt_trdid.read();
            p_vci_tgt.rpktid = r_tgt_pktid.read();
            p_vci_tgt.reop   = true;
            break;
        }
        case TGT_ERROR:
        {
            p_vci_tgt.cmdack = false;
            p_vci_tgt.rspval = true;
            p_vci_tgt.rdata  = 0;
            p_vci_tgt.rerror = vci_param::ERR_GENERAL_DATA_ERROR;
            p_vci_tgt.rsrcid = r_tgt_srcid.read();
            p_vci_tgt.rtrdid = r_tgt_trdid.read();
            p_vci_tgt.rpktid = r_tgt_pktid.read();
            p_vci_tgt.reop   = true;
            break;
        }
    } // end switch tgt_fsm
} // end genMore()

//////////////////////////////////////
tmpl(void)::print_trace(uint32_t mode)
{
    static const char* tgt_state_str[] =
    {
        "TGT_IDLE",
        "TGT_WRITE_GLOBAL_REG",
        "TGT_WRITE_CHANNEL_REG",
        "TGT_READ_GLOBAL_REG",
        "TGT_READ_CHANNEL_REG",
        "TGT_ERROR",
    };
    static const char* cmd_state_str[] =
    {
        "CMD_IDLE",
        "CMD_READ",
        "CMD_WRITE_STATUS",
        "CMD_WRITE_DATA",
    };
    static const char* rsp_state_str[] =
    {
        "RSP_IDLE",
        "RSP_READ_DESC",
        "RSP_READ_STATUS",
        "RSP_READ_DATA",
        "RSP_WRITE",
    };
    static const char* rx_g2s_state_str[] =
    {
        "RX_G2S_IDLE",
        "RX_G2S_DELAY",
        "RX_G2S_LOAD",
        "RX_G2S_SOS",
        "RX_G2S_LOOP",
        "RX_G2S_END",
        "RX_G2S_ERR",
        "RX_G2S_FAIL",
    };
    static const char* rx_des_state_str[] =
    {
        "RX_DES_IDLE",
        "RX_DES_READ_1",
        "RX_DES_READ_2",
        "RX_DES_READ_3",
        "RX_DES_READ_WRITE_0",
        "RX_DES_READ_WRITE_1",
        "RX_DES_READ_WRITE_2",
        "RX_DES_READ_WRITE_3",
        "RX_DES_WRITE_LAST",
        "RX_DES_WRITE_CLEAR",
    };
    static const char* rx_dispatch_state_str[] =
    {
        "RX_DISPATCH_IDLE",
        "RX_DISPATCH_GET_PLEN",
        "RX_DISPATCH_READ_HEADER",
        "RX_DISPATCH_SELECT",
        "RX_DISPATCH_PACKET_SKIP",
        "RX_DISPATCH_CLOSE_CONT",
        "RX_DISPATCH_WRITE_HEADER",
        "RX_DISPATCH_READ_WRITE",
        "RX_DISPATCH_WRITE_LAST",
    };
    static const char* rx_cma_state_str[] =
    {
        "RX_CMA_IDLE",
        "RX_CMA_ERROR",
        "RX_CMA_READ_DESC",
        "RX_CMA_READ_DESC_WAIT",
        "RX_CMA_READ_STATUS",
        "RX_CMA_READ_STATUS_WAIT",
        "RX_CMA_READ_STATUS_DELAY",
        "RX_CMA_MOVE_DATA",
        "RX_CMA_MOVE_DATA_WAIT",
        "RX_CMA_MOVE_DATA_END", 
        "RX_CMA_WRITE_STATUS",
        "RX_CMA_WRITE_STATUS_WAIT",
        "RX_CMA_NEXT",
    };
    static const char* tx_cma_state_str[] =
    {
        "TX_CMA_IDLE",
        "TX_CMA_ERROR",
        "TX_CMA_READ_DESC",
        "TX_CMA_READ_DESC_WAIT",
        "TX_CMA_READ_STATUS",
        "TX_CMA_READ_STATUS_WAIT",
        "TX_CMA_READ_STATUS_DELAY",
        "TX_CMA_MOVE_DATA",
        "TX_CMA_MOVE_DATA_WAIT",
        "TX_CMA_MOVE_DATA_END", 
        "TX_CMA_WRITE_STATUS",
        "TX_CMA_WRITE_STATUS_WAIT",
        "TX_CMA_NEXT",
    };
    static const char* tx_dispatch_state_str[] =
    {
        "TX_DISPATCH_IDLE",
        "TX_DISPATCH_GET_NPKT",
        "TX_DISPATCH_GET_PLEN",
        "TX_DISPATCH_SKIP_PKT",
        "TX_DISPATCH_READ_FIRST",
        "TX_DISPATCH_READ_WRITE",
        "TX_DISPATCH_WRITE_LAST",
        "TX_DISPATCH_RELEASE_CONT",
    };
    static const char* tx_ser_state_str[] =
    {
        "TX_SER_IDLE",
        "TX_SER_READ_FIRST",
        "TX_SER_WRITE_B0",
        "TX_SER_WRITE_B1",
        "TX_SER_WRITE_B2",
        "TX_SER_READ_WRITE",
        "TX_SER_GAP",
    };
    static const char* tx_s2g_state_str[] =
    {
        "TX_S2G_IDLE",
        "TX_S2G_WRITE_DATA",
        "TX_S2G_WRITE_LAST_DATA",
        "TX_S2G_WRITE_CS",
    };

    std::cout << "MASTER_NIC " << name() << " : " 
              << tgt_state_str[r_tgt_fsm.read()]                 << " | "
              << cmd_state_str[r_cmd_fsm.read()]                 << " | "
              << rsp_state_str[r_rsp_fsm.read()]                 << std::endl << "  "
              << rx_g2s_state_str[r_rx_g2s_fsm.read()]           << " | "
              << rx_des_state_str[r_rx_des_fsm.read()]           << " | "
              << rx_dispatch_state_str[r_rx_dispatch_fsm.read()] << " | "
              << rx_cma_state_str[r_rx_cma_fsm[0].read()]        << std::endl << "  " 
              << tx_s2g_state_str[r_tx_s2g_fsm.read()]           << " | " 
              << tx_ser_state_str[r_tx_ser_fsm.read()]           << " | "
              << tx_dispatch_state_str[r_tx_dispatch_fsm.read()] << " | "
              << tx_cma_state_str[r_tx_cma_fsm[0].read()]        << std::endl;

    if ( mode & 0x0001 ) // configuration registers
    {
        std::cout << "---- Global config Registers" << std::hex              << std::endl
                  << "r_global_bc_enable  : " << r_global_bc_enable.read()   << std::endl
                  << "r_global_period     : " << r_global_period.read()      << std::endl
                  << "r_global_mac_2      : " << r_global_mac_2.read()       << std::endl
                  << "r_global_mac_4      : " << r_global_mac_4.read()       << std::endl;

        for (size_t k = 0; k < m_channels; k++)
        {
            std::cout << "---- Channel[" << std::hex << k << "] config registers" << std::endl
                      << "r_rx_cma_dst_desc  : " << r_rx_cma_dst_desc[k].read()   << std::endl
                      << "r_rx_cma_dst_nbufs : " << r_rx_cma_dst_nbufs[k].read()  << std::endl
                      << "r_tx_cma_src_desc  : " << r_tx_cma_src_desc[k].read()   << std::endl
                      << "r_tx_cma_src_nbufs : " << r_tx_cma_src_nbufs[k].read()  << std::endl;
        }
    }
    if ( mode & 0x0010 ) // display RX_G2S registers
    {
        std::cout << "---- RX_G2S Registers" << std::hex                          << std::endl
                  << "r_rx_g2s_checksum      : " << r_rx_g2s_checksum.read()      << std::endl
                  << "r_rx_g2s_dt0           : " << (uint32_t)r_rx_g2s_dt0.read() << std::endl
                  << "r_rx_g2s_dt1           : " << (uint32_t)r_rx_g2s_dt1.read() << std::endl
                  << "r_rx_g2s_dt2           : " << (uint32_t)r_rx_g2s_dt2.read() << std::endl
                  << "r_rx_g2s_dt3           : " << (uint32_t)r_rx_g2s_dt3.read() << std::endl
                  << "r_rx_g2s_dt4           : " << (uint32_t)r_rx_g2s_dt4.read() << std::endl
                  << "r_rx_g2s_dt5           : " << (uint32_t)r_rx_g2s_dt5.read() << std::endl
                  << "r_rx_g2s_delay         : " << r_rx_g2s_delay.read()         << std::endl;
    }
    if ( mode & 0x0020 ) // display RX_DES registers
    {
        std::cout << "---- RX_DES Registers" << std::hex                              << std::endl
                  << "r_rx_des_counter       : " << r_rx_des_counter_bytes.read()     << std::endl
                  << "r_rx_des_padding       : " << r_rx_des_padding.read()           << std::endl
                  << "r_rx_des_data[0]       : " << (uint32_t)r_rx_des_data[0].read() << std::endl
                  << "r_rx_des_data[1]       : " << (uint32_t)r_rx_des_data[1].read() << std::endl
                  << "r_rx_des_data[2]       : " << (uint32_t)r_rx_des_data[2].read() << std::endl
                  << "r_rx_des_data[3]       : " << (uint32_t)r_rx_des_data[3].read() << std::endl;
    }
    if ( mode & 0x0040 ) // display RX_MULTI_FIFO
    {
        std::cout << "---- RX_MULTI_FIFO" << std::endl;
        r_rx_fifo_multi.print_trace( 0 );
    }
    if ( mode & 0x0080 ) // display RX_DISPATCH registers
    {
        std::cout << "---- RX_DISPATCH Registers" << std::hex                     << std::endl
                  << "r_rx_dispatch_word_0   : " << r_rx_dispatch_word[0].read()  << std::endl
                  << "r_rx_dispatch_word_1   : " << r_rx_dispatch_word[1].read()  << std::endl
                  << "r_rx_dispatch_word_2   : " << r_rx_dispatch_word[2].read()  << std::endl
                  << "r_rx_dispatch_word_3   : " << r_rx_dispatch_word[3].read()  << std::endl
                  << "r_rx_dispatch_word_4   : " << r_rx_dispatch_word[4].read()  << std::endl
                  << "r_rx_dispatch_word_5   : " << r_rx_dispatch_word[5].read()  << std::endl
                  << "r_rx_dispatch_word_6   : " << r_rx_dispatch_word[6].read()  << std::endl
                  << "r_rx_dispatch_plen     : " << r_rx_dispatch_plen.read()     << std::endl
                  << "r_rx_dispatch_channel  : " << r_rx_dispatch_channel.read()  << std::endl;
    }
    if ( mode & 0x0800 ) // display TX_DISPATCH registers
    {
        std::cout << "---- TX_DISPATCH Registers" << std::hex                     << std::endl
                  << "r_tx_dispatch_channel  : " << r_tx_dispatch_channel.read()  << std::endl
                  << "r_rx_dispatch_word_0   : " << r_rx_dispatch_word[0].read()  << std::endl
                  << "r_rx_dispatch_word_1   : " << r_rx_dispatch_word[1].read()  << std::endl
                  << "r_rx_dispatch_word_2   : " << r_rx_dispatch_word[2].read()  << std::endl
                  << "r_rx_dispatch_word_3   : " << r_rx_dispatch_word[3].read()  << std::endl
                  << "r_rx_dispatch_word_4   : " << r_rx_dispatch_word[4].read()  << std::endl
                  << "r_rx_dispatch_word_5   : " << r_rx_dispatch_word[5].read()  << std::endl
                  << "r_rx_dispatch_word_6   : " << r_rx_dispatch_word[6].read()  << std::endl
                  << "r_tx_dispatch_packets  : " << r_tx_dispatch_packets.read()  << std::endl
                  << "r_tx_dispatch_bytes    : " << r_tx_dispatch_bytes.read()    << std::endl;
    }
    if ( mode & 0x0400 ) // display TX_MULTI_FIFO 
    {
        std::cout << "---- TX_MULTI_FIFO" << std::endl;
        r_tx_fifo_multi.print_trace( 0 );
    }
    if ( mode & 0x0200 ) // display TX_SER registers
    {
        std::cout << "---- TX_SER Registers" << std::hex                          << std::endl
                  << "r_tx_ser_words         : " << r_tx_ser_words.read()         << std::endl
                  << "r_tx_ser_bytes         : " << r_tx_ser_bytes.read()         << std::endl
                  << "r_tx_ser_first         : " << r_tx_ser_first.read()         << std::endl
                  << "r_tx_ser_ifg           : " << r_tx_ser_ifg.read()           << std::endl
                  << "r_tx_ser_data          : " << r_tx_ser_data.read()          << std::endl;
    }
    if ( mode & 0x0100 ) // display TX_S2G registers
    {
        std::cout << "---- TX_S2G Registers" << std::hex                           << std::endl
                  << "r_tx_s2g_checksum      : " << r_tx_s2g_checksum.read()       << std::endl
                  << "r_tx_s2g_data          : " << (uint32_t)r_tx_s2g_data.read() << std::endl
                  << "r_tx_s2g_index         : " << r_tx_s2g_index.read()          << std::endl;
    }

    if ( mode & 0x1000 ) // display RX and TX chbuf 
    {
        for ( size_t k = 0 ; k < m_channels ; k++ )
        {
            std::cout << "---- RX_CHBUF[" << std::dec << k << "] state" << std::endl;
            r_rx_chbuf[k]->print_trace( k , 0 );
            std::cout << "---- TX_CHBUF[" << std::dec << k << "] state" << std::endl;
            r_tx_chbuf[k]->print_trace( k , 0 );
        }
    }
} // end print_trace()

////////////////////////////////////////////////////////////////////
//        Constructor
////////////////////////////////////////////////////////////////////
tmpl(/**/)::VciMasterNic( sc_core::sc_module_name 		        name,
                          const soclib::common::MappingTable    &mt,
                          const soclib::common::IntTab 		    &rx_srcid,
                          const soclib::common::IntTab 		    &tx_srcid,
                          const soclib::common::IntTab 		    &tgtid,
                          const size_t 				            channels,
                          const uint32_t			            burst_length,
                          const uint32_t                        mac_4,
                          const uint32_t                        mac_2,
                          const int                             mode,
                          const uint32_t                        gap )
           : caba::BaseModule(name),

           r_total_cycles("r_total_cycles"),
           r_total_len_rx_gmii("r_total_len_rx_gmii"),
           r_total_len_rx_chan("r_total_len_rx_chan"),
           r_total_len_tx_chan("r_total_len_tx_chan"),
           r_total_len_tx_gmii("r_total_len_tx_gmii"),

           r_global_bc_enable("r_global_bc_enable"),
           r_global_channels("r_global_channels"),
           r_global_period("r_global_period"),
           r_global_mac_4("r_global_mac_4"),
           r_global_mac_2("r_global_mac_2"),

           r_tgt_fsm("r_tgt_fsm"),
           r_tgt_srcid("r_tgt_srcid"),
           r_tgt_trdid("r_tgt_trdid"),
           r_tgt_pktid("r_tgt_pktid"),
           r_tgt_wdata("r_tgt_wdata"),
           r_tgt_be("r_tgt_be"), 
           r_tgt_address("r_tgt_address"),

           r_cmd_fsm("r_cmd_fsm"),
           r_cmd_bytes("r_cmd_bytes"),
           r_cmd_address("r_cmd_address"),
           r_cmd_type("r_cmd_type"),
           r_cmd_channel("r_cmd_channel"),
           r_cmd_burst("r_cmd_burst"),
           r_cmd_is_rx("r_cmd_is_rx"),

           r_rsp_fsm("r_rsp_fsm"),
           r_rsp_bytes("r_rsp_bytes"),
           r_rsp_channel("r_rsp_channel"),
           r_rsp_burst("r_rsp_burst"),
           r_rsp_is_rx("r_rsp_is_rx"),

           r_rx_g2s_fsm("r_rx_g2s_fsm"),
           r_rx_g2s_checksum("r_rx_g2s_checksum"),
           r_rx_g2s_dt0("r_rx_g2s_dt0"),
           r_rx_g2s_dt1("r_rx_g2s_dt1"),
           r_rx_g2s_dt2("r_rx_g2s_dt2"),
           r_rx_g2s_dt3("r_rx_g2s_dt3"),
           r_rx_g2s_dt4("r_rx_g2s_dt4"),
           r_rx_g2s_dt5("r_rx_g2s_dt5"),
           r_rx_g2s_delay("r_rx_g2s_delay"),

           r_rx_g2s_npkt_received("r_rx_g2s_npkt_received"),
           r_rx_g2s_npkt_discarded("r_rx_g2s_npkt_discarded"),

           r_rx_des_fsm("r_rx_des_fsm"),
           r_rx_des_counter_bytes("r_rx_des_counter_bytes"),
           r_rx_des_padding("r_rx_des_padding"),
           r_rx_des_data(soclib::common::alloc_elems<sc_signal<uint8_t> >
                         ("r_rx_des_data", 4)),

           r_rx_des_npkt_success("r_rx_des_npkt_success"),
           r_rx_des_npkt_too_small("r_rx_des_npkt_too_small"),
           r_rx_des_npkt_too_big("r_rx_des_npkt_too_big"),
           r_rx_des_npkt_mfifo_full("r_rx_des_npkt_mfifo_full"),
           r_rx_des_npkt_cs_fail("r_rx_des_npkt_cs_fail"),

           r_rx_dispatch_fsm("r_rx_dispatch_fsm"),
           r_rx_dispatch_word(soclib::common::alloc_elems<sc_signal<uint32_t> >
                         ("r_rx_dispatch_word", 8)),

           r_rx_dispatch_index("r_rx_dispatch_index"),
           r_rx_dispatch_nbytes("r_rx_dispatch_nbytes"),
           r_rx_dispatch_dest("r_rx_dispatch_dest"),
           r_rx_dispatch_channel("r_rx_dispatch_channel"),
           r_rx_dispatch_plen("r_rx_dispatch_plen"),

           r_rx_dispatch_npkt_received("r_rx_dispatch_npkt_received"),
           r_rx_dispatch_npkt_broadcast("r_rx_dispatch_npkt_broadcast"),
           r_rx_dispatch_npkt_dst_fail("r_rx_dispatch_npkt_dst_fail"),
           r_rx_dispatch_npkt_full("r_rx_dispatch_npkt_full"),

           r_tx_dispatch_fsm("r_tx_dispatch_fsm"),
           r_tx_dispatch_channel("r_tx_dispatch_channel"),
           r_tx_dispatch_word("r_tx_dispatch_word"),
           r_tx_dispatch_packets("r_tx_dispatch_packets"),
           r_tx_dispatch_nwords("r_tx_dispatch_nwords"),
           r_tx_dispatch_bytes("r_tx_dispatch_bytes"),

           r_tx_dispatch_npkt_received("r_tx_dispatch_npkt_received"),
           r_tx_dispatch_npkt_too_small("r_tx_dispatch_npkt_too_small"),
           r_tx_dispatch_npkt_too_big("r_tx_dispatch_npkt_too_big"),
           r_tx_dispatch_npkt_transmit("r_tx_dispatch_npkt_transmit"),

           r_tx_ser_fsm("r_tx_ser_fsm"),
           r_tx_ser_words("r_tx_ser_words"),
           r_tx_ser_bytes("r_tx_ser_bytes"),
           r_tx_ser_first("r_tx_ser_first"),
           r_tx_ser_ifg("r_tx_ser_ifg"),
           r_tx_ser_data("r_tx_ser_data"),

           r_tx_s2g_fsm("r_tx_s2g_fsm"),
           r_tx_s2g_checksum("r_tx_s2g_checksum"),
           r_tx_s2g_data("r_tx_s2g_data"),
           r_tx_s2g_index("r_tx_s2g_index"),

           r_rx_fifo_stream("r_rx_fifo_stream" , 2 ),
           r_tx_fifo_stream("r_tx_fifo_stream" , 2 ),

           r_rx_fifo_multi("r_rx_fifo_multi", 32 , 32 ),
           r_tx_fifo_multi("r_tx_fifo_multi", 32 , 32 ),

           m_seglist( mt.getSegmentList(tgtid) ),
           m_rx_srcid( mt.indexForId(rx_srcid) ),
           m_tx_srcid( mt.indexForId(tx_srcid) ),
           m_channels( channels ),
           m_burst_length( burst_length ),
           m_default_mac_4( mac_4 ),
           m_default_mac_2( mac_2 ),
           m_gap( gap ),

           p_clk("p_clk"),
           p_resetn("p_resetn"),
           p_vci_tgt("p_vci_tgt"),
           p_vci_ini("p_vci_ini"),
           p_rx_irq(soclib::common::alloc_elems<sc_core::sc_out<bool> >("p_rx_irq", channels)),
           p_tx_irq(soclib::common::alloc_elems<sc_core::sc_out<bool> >("p_tx_irq", channels))
{
    assert( ((mode == NIC_MODE_FILE) || (mode == NIC_MODE_SYNTHESIS) || (mode == NIC_MODE_TAP))
    and "VCI_MASTER_NIC error : Illegal mode for backend");

    assert( (channels <= 8 ) and
    "VCI_MASTER_NIC error : No more than 8 channels");

    assert( (gap >= 12 ) and
    "VCI_MASTER_NIC error : Inter frame gap cannot be smaller than 12 cycles");

    if ( mode == NIC_MODE_FILE )
    {
        std::cout << "  - Building VciMasterNic - File version " << name << std::endl;

        r_backend_rx = new NicRxGmii( gap, true );
        r_backend_tx = new NicTxGmii();
    }

    // allocating one RX_CHBUF and one TX_CHBUF per channel
    for ( size_t k = 0 ; k < channels ; k++ )
    {
        r_rx_chbuf[k] = (NicRxChbuf*)new NicRxChbuf( RX_TIMEOUT );
        r_tx_chbuf[k] = (NicTxChbuf*)new NicTxChbuf( );
    }

    // allocating RX and TX backend
    if ( mode == NIC_MODE_SYNTHESIS )
    {
        std::cout << "  - Building VciMasterNic - Synthesis version " << name << std::endl;

        srandom( 0xBABEF00D );  // for reproductible synthesis

        r_backend_rx = new NicRxGmii( gap, false );
        r_backend_tx = new NicTxGmii();
    }
    if ( mode == NIC_MODE_TAP )
    {
        std::cout << "  - Building VciMasterNic - TAP version " << name << std::endl;

#if !defined(__APPLE__) || !defined(__MACH__)
        r_backend_rx = new NicRxTap( gap );
        r_backend_tx = new NicTxTap();

        // get a TAP fd
        uint32_t tmp_fd = open_tap_fd();

        if (tmp_fd < 0) 
        {
            std::cerr << name << " : Cannot open the TAP interface " << std::endl;
            exit(-1);
        }

        dynamic_cast<NicRxTap*>(r_backend_rx)->set_fd(tmp_fd);
        dynamic_cast<NicRxTap*>(r_backend_rx)->set_ifr(&m_tap_ifr);

        dynamic_cast<NicTxTap*>(r_backend_tx)->set_fd(tmp_fd);
        dynamic_cast<NicTxTap*>(r_backend_tx)->set_ifr(&m_tap_ifr);
#else
        std::cout << "     ... but the TAP backend is not supported" << std::endl;
        exit(0);
#endif
    }
    
    size_t nbsegs = 0;

    std::list<soclib::common::Segment>::iterator seg;
    for ( seg = m_seglist.begin() ; seg != m_seglist.end() ; seg++ )
    {
        nbsegs++;
        assert( ( (seg->baseAddress() & 0x1FF) == 0 ) and
                  "VCI_MASTER_NIC Error : segment base must be multiple of 512 bytes");

        assert( ( seg->size() >= 0x200 ) and
                  "VCI_MASTER_NIC Error : segment size cannot be smaller than 512 bytes");

        std::cout << "    => segment " << seg->name()
                  << " / base = " << std::hex << seg->baseAddress()
                  << " / size = " << seg->size() << std::endl;
    }

    assert ( (nbsegs != 0) and
             "VCI_MASTER_NIC error : no segment allocated");

    assert( ((vci_param::B == 4) or (vci_param::B == 8)) and
            "VCI_MASTER_NIC error : The VCI DATA field must be 32 or 64 bits");

    assert( (channels <= 8)  and
            "VCI_MASTER_NIC error : The number of channels cannot be larger than 8");

    SC_METHOD(transition);
    dont_initialize();
    sensitive << p_clk.pos();

    SC_METHOD(genMoore);
    dont_initialize();
    sensitive << p_clk.neg();
}

#if !defined(__APPLE__) || !defined(__MACH__)

////////////////////////////
tmpl(int32_t)::open_tap_fd()
{
    int32_t tap_fd = -1;

    tap_fd = open("/dev/net/tun", O_RDWR);

    if (tap_fd < 0) // error
    {
        std::cerr << name() << ": Unable to open /dev/net/tun" << std::endl;
    }
    else // we can continue
    {
        int flags = fcntl(tap_fd, F_GETFL, 0);
        fcntl(tap_fd, F_SETFL, flags | O_NONBLOCK);
        memset((void*)&m_tap_ifr, 0, sizeof(m_tap_ifr));
        m_tap_ifr.ifr_flags = IFF_TAP | IFF_NO_PI;

        if (ioctl(tap_fd, TUNSETIFF, (void *) &m_tap_ifr) < 0)
        {
            ::close(tap_fd);
            tap_fd = -1;
            std::cerr << name() << ": Unable to setup tap interface, check privileges."
#ifdef __linux__
                              << " (try: sudo setcap cap_net_admin=eip ./system.x)"
#endif
                              << std::endl;
            return -1;
        }
        else
        {
            std::cout << name() << ": TAP interface succesfully created: " << m_tap_ifr.ifr_name << "\n";
        }
    }
    return tap_fd;
} // end open_tap_fd

#endif  /*  !defined(__APPLE__) || !defined(__MACH__) */


////////////////////////////
//  Destructor
////////////////////////////
tmpl(/**/)::~VciMasterNic() 
{
    soclib::common::dealloc_elems<sc_core::sc_out<bool> >(p_rx_irq, m_channels);
    soclib::common::dealloc_elems<sc_core::sc_out<bool> >(p_tx_irq, m_channels);
}

}}


// Local Variables:
// tab-width: 4
// c-basic-offset: 4
// c-file-offsets:((innamespace . 0)(inline-open . 0))
// indent-tabs-mode: nil
// End:

// vim: filetype=cpp:expandtab:shiftwidth=4:tabstop=4:softtabstop=4

