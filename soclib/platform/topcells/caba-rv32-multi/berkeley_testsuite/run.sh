#!/bin/bash
##
#  @author      Cesar Fuguet Tortolero (c.sarfuguet <at> gmail.com)
#  @file        run.sh
#  @description simple wrapper script to run berkeley's RISC-V ISA tests 
#               on the caba_rv32_simple platform.

TESTDIR="$(soclib-cc --getpath)/soclib/soft/berkeley-riscv-tests/isa"
SIMULATOR="../system.x"
MAXCYCLES="50000"

function run_test() {
	${SIMULATOR} --soft $1 --ncycles ${MAXCYCLES} 2>/dev/null
}

function print_result() {
	if [ $2 -eq 1 ]; then
		printf "@ %-30s \e[32mSUCCESS\e[0m (retval %d)\n" $1 $2
	else
		printf "@ %-30s \e[31mFAILURE\e[0m (retval %d)\n" $1 $2
	fi
}

function run_and_check_test() {
	export SOCLIB_TTY=PTS
	export SC_COPYRIGHT_MESSAGE=DISABLE

	match_pattern="Simulation exiting, retval=\(.*\)"
	match=$(run_test $1 | sed -n "s/${match_pattern}/\1/p")
	retval=$(( 10#${match} ))

	print_result $(basename $1) ${retval}
}

function main() {
	if [ -d ${TESTDIR} ]; then
		make -C ${TESTDIR} XLEN=32
	else
		echo "error: bad directory path to the RISCV tests"
		echo
		exit -1
	fi

	if [ ! -x ${SIMULATOR} ]; then
		echo "error: the simulator executable does not exists"
		echo
		exit -1
	fi

	i=0
	testgroups[i++]="rv32ui-p"
	testgroups[i++]="rv32uc-p"
	testgroups[i++]="rv32uf-p"
	testgroups[i++]="rv32um-p"
	testgroups[i++]="rv32ua-p"

	echo "Tests directory: ${TESTDIR}"
	for t in ${testgroups[@]}; do
		TESTS=$(find ${TESTDIR} -type f -name "$t-*" \
			| egrep -v "\.dump")
		for f in ${TESTS}; do
			run_and_check_test $f
		done
	done
}

main
