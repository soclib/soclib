# SoCLib Cycle-Accurate, RISC-V 32-bit, multi-core, simulation platform

## Memory address space

<table style="text-align: left; width: 80%">
<tr>
<th>Short name</th>
<th>Long name</th>
<th>Base address</th>
<th>Size (bytes)</th>
</tr>
<tr>
<td>CLINT</td>
<td>Core Local Interrupt Controller</td>
<td>0x00200000</td>
<td>0x0000c000</td>
</tr>
<tr>
<td>MTTY</td>
<td>System Terminal</td>
<td>0x10000000</td>
<td>0x00001000</td>
</tr>
<tr>
<td>SIMH</td>
<td>Simulation Helper</td>
<td>0x20000000</td>
<td>0x00001000</td>
</tr>
<tr>
<td>FBUF</td>
<td>Framebuffer</td>
<td>0x30000000</td>
<td>0x01000000</td>
</tr>
<tr>
<td>RAMC</td>
<td>RAM cached</td>
<td>0x80000000</td>
<td>0x60000000</td>
</tr>
<tr>
<td>RAMU</td>
<td>RAM uncached</td>
<td>0xe0000000</td>
<td>0x20000000</td>
</tr>
</table>


## Compilation

### Compile the platform
```sh
make <options>
```

Makefile options:
```sh
J=<jobs>             Maximum number of CPUs used for the compilation
DEBUG=1              Compile in debug mode (add debug symbols on the simulator executable)
VERBOSE=1            Enable verbose mode during compilation
DEBUG_MODULE=<name>  Enable debugging of hardware module <name>
```

### Create tags file for the simulation platform
```sh
make tags
```

### Clean all generated files

```sh
make clean
```

## Execution

```sh
./system.x   --soft <pathname> \
            [--ncycles <simulation_cycles>] \
            [--ncores <number_of_cores>] \
            [--debug <debug_from_cycle>] \
            [--fbwidth <framebuffer_width>] \
            [--fbheight <framebuffer_height>] \
            [--icacheparams <sets:ways:words>] \
            [--dcacheparams <sets:ways:words>]
```

Only the --soft argument is mandatory. It provides the path to the cross-compiled RISC-V 32 ELF file to run in the platform.

The --ncycles argument indicates the maximum number of clock cycles to run on the simulation. After that number, the simulation stops.

The --ncores argument indicates the number of simulated RISC-V, 32-bit cores. The default value is 1 core.

The --debug argument enable debug messages from the simulation platform. It takes an integer value that corresponds to the clock cycle when debug messages will be enabled.

The --fbwidth and --fbheight arguments enable the Framebuffer in the platform. They corresponds to the width and the height of the Framebuffer respectively.

The --icacheparams and --dcacheparams arguments allow to define the number of sets, ways and words of the L1 instruction cache and L1 data cache, respectively, of all cores in the platform.

## Runtime parameters

This platform takes additional parameters through values in specific environment variables. These are considered by the platform when running the ./system.x executable.

### SOCLIB_ISS_TRACE

When set to F or f, the RISC-V 32 ISS instances will trace all executed instructions in a file with the following name: soclib_trace_hart\<id\>.txt. The \<id\> corresponds to the HART ID of each core in the platform.

When set to E or e, the RISC-V 32 ISS instances will trace all executed instructions on the standard error output.

When unset, instruction tracing is disabled.


### SOCLIB_GDB

The SOCLIB_GDB environment variable may contain zero, one or more of the following flag letters:

 - X (dont break on except)
 - S (wait connect on except)
 - F (start frozen)
 - C (functions branch trace)
 - Z (functions entry trace)
 - D (gdb protocol debug),
 - W (dont break on watchpoints)
 - T (exit simulation on trap)
 - E (exit on fault)

It is particularly useful to use the F flag when you want to debug your embedded application. This flag makes the simulator to wait GDB to be connected before running the application.

Once you execute the ./system.x simulator, you will get the GDB listening port on the terminal (it usually starts at 2346).


### SOCLIB_TTY

By default, the simulation platform will create an Xterm which acts as the system console. In this mode it provides a way to output messages written in the mtty system terminal, and it also provides a way to pass user inputs to the platform.

In some cases, it is convenient to log output messages in files. In this case, you can set the SOCLIB_TTY variable to FILES.
