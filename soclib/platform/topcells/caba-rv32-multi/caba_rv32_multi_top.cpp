/**
 *  Cycle-Accurate RISCV32 multi-core platform
 *
 *  Copyright (C) 2019 Cesar Fuguet Tortolero
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  @file           caba_rv32_multi_top.cpp
 *  @author         Cesar Fuguet Tortolero (c.sarfuguet <at> gmail.com)
 *  @date           May, 2019
 */
#include <iostream>
#include <string>
#include <vector>
#include <cstdint>
#include <ctime>
#include <systemc>

#include "gdbserver.h"
#include "rv32.h"
#include "vci_param.h"
#include "vci_xcache_wrapper.h"
#include "vci_vgmn.h"
#include "vci_multi_tty.h"
#include "vci_simple_ram.h"
#include "vci_simhelper.h"
#include "vci_clint.h"
#include "vci_framebuffer.h"
#include "mapping_table.h"

/* VCI parameters */
#define VCI_PARAMS \
    4  , /* CELLSIZE */ \
    8  , /* PLENSIZE */ \
    32 , /* ADDRSIZE */ \
    1  , /* ERRORSIZE */ \
    1  , /* CLENSIZE */ \
    1  , /* FLAGSIZE */ \
    4  , /* SRCIDSIZE */ \
    4  , /* PKTIDSIZE */ \
    4  , /* TRDIDSIZE */ \
    1    /* WRPLENSIZE */

/* Maximum number of cores in the platform */
#define MAX_CORES      16

namespace soclib
{
namespace caba
{
    class Riscv32MultiTop
    {
        typedef VciParams<VCI_PARAMS>
            PlatformVciParams;
        typedef GdbServer<::soclib::common::Rv32Iss>
            Rv32Iss;
        typedef VciXcacheWrapper<PlatformVciParams,Rv32Iss>
            VciXcacheRv32;

        public:

        struct SimParams {
            std::string soft;
            int ncores;
            unsigned long long ncycles;
            unsigned long long debug_from;
            unsigned long fbwidth;
            unsigned long fbheight;
            unsigned long icache_sets;
            unsigned long icache_ways;
            unsigned long icache_words;
            unsigned long dcache_sets;
            unsigned long dcache_ways;
            unsigned long dcache_words;
        };

        static void parseParams(unsigned int argc, char *argv[], SimParams &params)
        {
            for (size_t n = 1; n < argc; n = n + 2)
            {
                if ((strcmp(argv[n], "--soft") == 0) && ((n + 1) < argc))
                {
                    params.soft = std::string(argv[n + 1]);
                }
                else if ((strcmp(argv[n], "--ncycles") == 0) && ((n + 1) < argc))
                {
                    params.ncycles = strtoull(argv[n + 1], NULL, 10);
                }
                else if ((strcmp(argv[n], "--ncores") == 0) && ((n + 1) < argc))
                {
                    params.ncores = strtoul(argv[n + 1], NULL, 10);
                }
                else if ((strcmp(argv[n], "--debug") == 0) && ((n + 1) < argc))
                {
                    params.debug_from = strtoull(argv[n + 1], NULL, 10);
                }
                else if ((strcmp(argv[n], "--fbwidth") == 0) && ((n + 1) < argc))
                {
                    params.fbwidth = strtoul(argv[n + 1], NULL, 10);
                }
                else if ((strcmp(argv[n], "--fbheight") == 0) && ((n + 1) < argc))
                {
                    params.fbheight = strtoul(argv[n + 1], NULL, 10);
                }
                else if ((strcmp(argv[n], "--icacheparams") == 0) && ((n + 1) < argc))
                {
                    int ret = std::sscanf(argv[n + 1], "%lu:%lu:%lu",
                            &params.icache_sets,
                            &params.icache_ways,
                            &params.icache_words);
                    if (ret != 3) {
                        std::cout << "error: --icacheparams bad format" << std::endl << std::endl;
                        printParamsHelp();
                        std::exit(1);
                    }
                }
                else if ((strcmp(argv[n], "--dcacheparams") == 0) && ((n + 1) < argc))
                {
                    int ret = std::sscanf(argv[n + 1], "%lu:%lu:%lu",
                            &params.dcache_sets,
                            &params.dcache_ways,
                            &params.dcache_words);
                    if (ret != 3) {
                        std::cout << "error: --dcacheparams bad format" << std::endl << std::endl;
                        printParamsHelp();
                        std::exit(1);
                    }
                }
                else
                {
                    printParamsHelp();
                    std::exit(0);
                }
            }

            /* check parameters */
            if (params.soft == "") {
                std::cout << "--soft is not optional" << std::endl;
                std::cout << std::endl;
                printParamsHelp();
                std::exit(1);
            }
            if (params.ncores < 1) {
                std::cout << "--ncores must be greater than 0" << std::endl;
                std::cout << std::endl;
                printParamsHelp();
                std::exit(1);
            }
            if (params.ncores > MAX_CORES) {
                std::cout << "--ncores must be less than or equal to "
                    << MAX_CORES << std::endl;
                std::cout << std::endl;
                printParamsHelp();
                std::exit(1);
            }

            printParams(params);
        }

        static void printParams(const SimParams &params)
        {
            std::cout << "  soft         = " << params.soft << std::endl;
            std::cout << "  ncores       = " << params.ncores << std::endl;
            if (params.ncycles > 0)
                std::cout << "  ncycles      = " << params.ncycles << std::endl;
            if (params.debug_from > 0)
                std::cout << "  debug        = " << params.debug_from << std::endl;
            if ((params.fbwidth > 0) && (params.fbheight > 0)) {
                std::cout << "  fbwidth      = " << params.fbwidth << std::endl;
                std::cout << "  fbheight     = " << params.fbheight << std::endl;
            }
            std::cout << "  icache_sets  = " << params.icache_sets << std::endl;
            std::cout << "  icache_ways  = " << params.icache_ways << std::endl;
            std::cout << "  icache_words = " << params.icache_words << std::endl;
            std::cout << "  dcache_sets  = " << params.dcache_sets << std::endl;
            std::cout << "  dcache_ways  = " << params.dcache_ways << std::endl;
            std::cout << "  dcache_words = " << params.dcache_words << std::endl;
            std::cout << std::endl;
        }

        static void printParamsHelp()
        {
            std::cout << "Accepted arguments are :" << std::endl;
            std::cout << "--soft <pathname>" << std::endl;
            std::cout << "[--ncycles <simulation_cycles>]" << std::endl;
            std::cout << "[--ncores <number_of_cores>]" << std::endl;
            std::cout << "[--debug <debug_from_cycle>]" << std::endl;
            std::cout << "[--fbwidth <framebuffer_width>]" << std::endl;
            std::cout << "[--fbheight <framebuffer_height>]" << std::endl;
            std::cout << "[--icacheparams <sets:ways:words>]" << std::endl;
            std::cout << "[--dcacheparams <sets:ways:words>]" << std::endl;
        }

        protected:

        const sc_time CLK_PERIOD;
        static const unsigned int NOC_LATENCY = 4;
        static const unsigned int RAM_LATENCY = 4;

        size_t n_noc_ini;
        size_t n_noc_tgt;

        ::sc_core::sc_clock        s_clk;
        ::sc_core::sc_signal<bool> s_resetn;
        ::sc_core::sc_signal<bool> s_false;
        ::sc_core::sc_signal<bool> *s_irq_sip;
        ::sc_core::sc_signal<bool> *s_irq_tip;
        ::sc_core::sc_signal<bool> *s_irq_eip;

        VciSignals<PlatformVciParams> s_vci_tty;
        VciSignals<PlatformVciParams> s_vci_ram;
        VciSignals<PlatformVciParams> s_vci_sim;
        VciSignals<PlatformVciParams> s_vci_clint;
        VciSignals<PlatformVciParams> *s_vci_fbuf;
        VciSignals<PlatformVciParams> *s_vci_core;

        VciSimpleRam<PlatformVciParams> *ram;
        VciVgmn<PlatformVciParams> *noc;
        VciMultiTty<PlatformVciParams> *tty;
        VciSimhelper<PlatformVciParams> *sim;
        VciClint<PlatformVciParams> *clint;
        VciFrameBuffer<PlatformVciParams> *fbuf;
        VciXcacheRv32 *core;

        ::soclib::common::MappingTable *mt;
        ::soclib::common::Loader loader;

        public:

        Riscv32MultiTop(const SimParams &params) :
            CLK_PERIOD(sc_time(1000, SC_PS)),

            n_noc_ini(0),
            n_noc_tgt(0),

            s_clk("clock", CLK_PERIOD),
            s_resetn("resetn"),
            s_false("s_false"),

            s_irq_sip(NULL),
            s_irq_tip(NULL),
            s_irq_eip(NULL),

            s_vci_tty("s_vci_tty"),
            s_vci_ram("s_vci_ram"),
            s_vci_sim("s_vci_sim"),
            s_vci_clint("s_vci_clint"),
            s_vci_fbuf(NULL),
            s_vci_core(NULL),

            ram(NULL),
            noc(NULL),
            tty(NULL),
            sim(NULL),
            clint(NULL),
            fbuf(NULL),
            core(NULL),

            mt(NULL),
            loader(params.soft)
        {
            //  check if the frame buffer needs to be instantiated
            bool use_fbuf = (params.fbwidth > 0) && (params.fbheight > 0);

            //  instantiate the mapping table
            using ::soclib::common::MappingTable;
            using ::soclib::common::IntTab;
            using ::soclib::common::Loader;

            mt = new MappingTable(PlatformVciParams::N,
                    IntTab(8), IntTab(4),
                    0xE0000000);

            mt->add(Segment("seg_clint"       , 0x00200000ul, 0x0000c000ul, IntTab(3), 0));
            mt->add(Segment("seg_mtty"        , 0x10000000ul, 0x00001000ul, IntTab(1), 0));
            mt->add(Segment("seg_simh"        , 0x20000000ul, 0x00001000ul, IntTab(2), 0));
            if (use_fbuf)
                mt->add(Segment("seg_fbuf"    , 0x30000000ul, 0x01000000ul, IntTab(4), 0));
            mt->add(Segment("seg_ram_cached"  , 0x80000000ul, 0x60000000ul, IntTab(0), 1));
            mt->add(Segment("seg_ram_uncached", 0xe0000000ul, 0x20000000ul, IntTab(0), 0));

            //  initialize the memory with some non-zero data pattern
            loader.memory_default(0xca);

            std::cout << loader << std::endl;

            //
            //  Netlist
            //

            //  instantiate the cores
            alloc_and_init_cores(params);

            //  instantiate the ram
            ram = new VciSimpleRam<PlatformVciParams>(
                    "ram", IntTab(0), *mt, loader, RAM_LATENCY);

            ram->p_clk(s_clk);
            ram->p_resetn(s_resetn);
            ram->p_vci(s_vci_ram);

            n_noc_tgt++;

            //  instantiate the tty
            std::vector<std::string> ttyNames;
            ttyNames.push_back("tty0");
            tty = new VciMultiTty<PlatformVciParams>(
                    "tty", IntTab(1), *mt, ttyNames);

            tty->p_clk(s_clk);
            tty->p_resetn(s_resetn);
            tty->p_vci(s_vci_tty);
            tty->p_irq[0](s_irq_eip[0]);

            n_noc_tgt++;

            //  instantiate the sim helper
            sim = new VciSimhelper<PlatformVciParams>(
                    "sim", IntTab(2), *mt);

            sim->p_clk(s_clk);
            sim->p_resetn(s_resetn);
            sim->p_vci(s_vci_sim);

            n_noc_tgt++;

            //  instantiate the CLINT (Core Local Interrupt Controller)
            clint = new VciClint<PlatformVciParams>(
                    "clint", *mt, IntTab(3),
                    params.ncores);

            clint->p_clk(s_clk);
            clint->p_resetn(s_resetn);
            clint->p_vci(s_vci_clint);
            for (int i = 0; i < params.ncores; i++) {
                clint->p_msip[i](s_irq_sip[i]);
                clint->p_mtip[i](s_irq_tip[i]);
            }

            n_noc_tgt++;

            //  instantiate the FrameBuffer (if needed)
            if (use_fbuf) {
                fbuf = new VciFrameBuffer<PlatformVciParams>(
                        "fbuf", IntTab(4), *mt,
                        params.fbwidth, params.fbheight,
                        FbController::RGB_32);

                s_vci_fbuf = new VciSignals<PlatformVciParams>("s_vci_fbuf");

                fbuf->p_clk(s_clk);
                fbuf->p_resetn(s_resetn);
                fbuf->p_vci(*s_vci_fbuf);

                n_noc_tgt++;
            }

            //  instantiate the NoC
            noc = new VciVgmn<PlatformVciParams>(
                    "noc", *mt, n_noc_ini, n_noc_tgt, NOC_LATENCY, 2, 0);

            noc->p_clk(s_clk);
            noc->p_resetn(s_resetn);
            for (size_t i = 0; i < n_noc_ini; i++)
                noc->p_to_initiator[i](s_vci_core[i]);
            noc->p_to_target[0](s_vci_ram);
            noc->p_to_target[1](s_vci_tty);
            noc->p_to_target[2](s_vci_sim);
            noc->p_to_target[3](s_vci_clint);
            if (use_fbuf)
                noc->p_to_target[4](*s_vci_fbuf);
        }

        void simulate(const SimParams& params)
        {
            using namespace ::sc_core;

            //  start elaboration
            sc_start(sc_core::SC_ZERO_TIME);

            //  stuck-at false signal
            s_false = false;
            for (int i = 1; i < params.ncores; i++) {
                s_irq_eip[i] = false;
            }

            //  reset
            s_resetn = false;
            for (int cycle = 0; cycle < 2; ++cycle) {
                sc_start(CLK_PERIOD);
            }
            s_resetn = true;

            //  execute
            clock_t startTime, endTime;

            std::cout << std::dec;
            startTime = clock();
            for (uint64_t cycle = 0; cycle < params.ncycles; ++cycle) {
                sc_start(CLK_PERIOD);

                if (cycle >= params.debug_from) {
                    std::cout << "< SIMULATION CYCLE " << cycle << " >";
                    std::cout << std::endl << std::endl;
                    core->print_trace(0);
                    s_vci_ram.print_trace("s_vci_ram");
                    s_vci_clint.print_trace("s_vci_clint");
                    std::cout << std::endl;
                }

                if (cycle && !(cycle & 0x3fffff)) {
                    uint64_t timelapse = (1000*(clock() - startTime))/CLOCKS_PER_SEC;

                    std::cout << "Simulation frequency (mean): ";
                    std::cout << cycle/timelapse << " KHz";
                    std::cout << std::endl;
                }
            }
            endTime = clock();

            //  quit simulation
            uint64_t elapsedTime = (1000*(endTime - startTime))/CLOCKS_PER_SEC;
            std::cout << std::endl;

            std::cout << "Elapsed wall time                : ";
            std::cout << elapsedTime << " ms";
            std::cout << std::endl;

            std::cout << "Simulation frequency (mean): ";
            std::cout << params.ncycles/elapsedTime << " KHz";
            std::cout << std::endl;

            sc_stop();
        }

        ~Riscv32MultiTop()
        {
            if (ram)
                delete ram;
            if (tty)
                delete tty;
            if (noc)
                delete noc;
            if (sim)
                delete sim;
            if (clint)
                delete clint;
            if (fbuf)
                delete fbuf;
            if (core)
                delete [] core;
            if (s_vci_fbuf)
                delete s_vci_fbuf;
            if (s_vci_core)
                delete [] s_vci_core;
            if (s_irq_sip)
                delete [] s_irq_sip;
            if (s_irq_tip)
                delete [] s_irq_tip;
            if (s_irq_eip)
                delete [] s_irq_eip;
            if (mt)
                delete mt;
        }

        private:

        void alloc_and_init_cores(const SimParams &params)
        {
            //  link the ELF loader to the ISS
            Rv32Iss::set_loader(loader);

            //  instantiate the cores
            core = (VciXcacheRv32*)calloc(params.ncores, sizeof(VciXcacheRv32));
            if (core == NULL) {
                std::cerr << "error: bad allocation of cores" << std::endl;
                std::exit(1);
            }

            s_vci_core = (VciSignals<PlatformVciParams>*)
                calloc(params.ncores, sizeof(VciSignals<PlatformVciParams>));
            if (s_vci_core == NULL) {
                std::cerr << "error: bad allocation of VCI signals from cores" << std::endl;
                std::exit(1);
            }

            s_irq_sip = (::sc_core::sc_signal<bool>*)
                calloc(params.ncores, sizeof(::sc_core::sc_signal<bool>));
            if (s_irq_sip == NULL) {
                std::cerr << "error: bad allocation of IRQ software signals to cores" << std::endl;
                std::exit(1);
            }

            s_irq_tip = (::sc_core::sc_signal<bool>*)
                calloc(params.ncores, sizeof(::sc_core::sc_signal<bool>));
            if (s_irq_tip == NULL) {
                std::cerr << "error: bad allocation of IRQ timer signals to cores" << std::endl;
                std::exit(1);
            }

            s_irq_eip = (::sc_core::sc_signal<bool>*)
                calloc(params.ncores, sizeof(::sc_core::sc_signal<bool>));
            if (s_irq_eip == NULL) {
                std::cerr << "error: bad allocation of IRQ external signals to cores" << std::endl;
                std::exit(1);
            }

            for (int c = 0; c < params.ncores; c++) {
                char __name[32];

                snprintf(__name, sizeof(__name), "riscv%d", c);
                new (&core[c])
                    VciXcacheRv32(__name, c, *mt, IntTab(c),
                            params.icache_ways, params.icache_sets, params.icache_words,
                            params.dcache_ways, params.dcache_sets, params.dcache_words);

                snprintf(__name, sizeof(__name), "s_vci_core%d", c);
                new (&s_vci_core[c])
                    VciSignals<PlatformVciParams>(__name);

                snprintf(__name, sizeof(__name), "s_irq_sip%d", c);
                new (&s_irq_sip[c])
                    ::sc_core::sc_signal<bool>(__name);

                snprintf(__name, sizeof(__name), "s_irq_tip%d", c);
                new (&s_irq_tip[c])
                    ::sc_core::sc_signal<bool>(__name);

                snprintf(__name, sizeof(__name), "s_irq_eip%d", c);
                new (&s_irq_eip[c])
                    ::sc_core::sc_signal<bool>(__name);

                core[c].p_clk(s_clk);
                core[c].p_resetn(s_resetn);
                core[c].p_vci(s_vci_core[c]);
                core[c].p_irq[0](s_irq_sip[c]);
                core[c].p_irq[1](s_irq_tip[c]);
                core[c].p_irq[2](s_irq_eip[c]);

                n_noc_ini++;
            }
        }

    };
} // end namespace caba
} // end namespace soclib


using soclib::caba::Riscv32MultiTop;

int sc_main(int argc, char *argv[])
{
    std::cout << std::endl;
    std::cout << "Platform: caba_riscv32_multi" << std::endl;
    std::cout << std::endl;

    std::cout << "Parsing command line arguments" << std::endl;
    std::cout << std::endl;
    Riscv32MultiTop::SimParams params = {
        .soft = "",
        .ncores = 1,
        .ncycles = 0xffffffffffULL,
        .debug_from = 0xffffffffffULL,
        .icache_sets = 64,
        .icache_ways = 4,
        .icache_words = 8,
        .dcache_sets = 64,
        .dcache_ways = 4,
        .dcache_words = 8
    };
    Riscv32MultiTop::parseParams(argc, argv, params);
    std::cout << std::endl;

    std::cout << "Creating the netlist" << std::endl;
    std::cout << std::endl;
    Riscv32MultiTop top(params);
    std::cout << std::endl;

    std::cout << "Starting the simulation" << std::endl;
    std::cout << std::endl;
    top.simulate(params);
    std::cout << std::endl;

    std::cout << "End of the simulation" << std::endl;
    std::cout << "Press enter to quit" << std::endl;
    std::cin.get();
    return 0;
}

// vim: tabstop=4 : softtabstop=4 : shiftwidth=4 : expandtab
