/*
 * SoCLib is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; version 2.1 of the License.
 * 
 * SoCLib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with SoCLib; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include <strings.h>

#include "aicu.h"
#include "register.h"
#include "arithmetics.h"
#include "alloc_elems.h"
#include "../include/vci_aicu.h"

#define tmpl(t) template<typename vci_param> t VciAicu<vci_param>

using namespace std;
using namespace soclib;
using namespace soclib::caba;

namespace soclib
{
  namespace caba
  {
    tmpl(/**/)::VciAicu (
        sc_core::sc_module_name name,
        const MappingTable &mt,
        const IntTab &index,
        size_t irq_count,
        size_t global_count,
        size_t local_count
        ) : caba::BaseModule (name),
    m_vci_fsm (p_vci, mt.getSegmentList (index)),
    m_reset_registers (false),
    m_irq_count (irq_count),
    m_global_count (global_count),
    m_local_count (local_count),
    r_control (0),
    r_handlers (new uint32_t[32]),
    r_status (new uint32_t[irq_count]),
    r_mask (new uint32_t[irq_count]),
    r_address (new uint32_t[irq_count]),
    r_active_id (new uint32_t[irq_count]),
    p_clk ("clk"),
    p_resetn ("resetn"),
    p_vci ("vci"),
    p_irq_in (soclib::common::alloc_elems<sc_core::sc_in<bool> >
        ("irq_in", irq_count * local_count + global_count)),
    p_irq_out (soclib::common::alloc_elems<sc_core::sc_out<bool> >
        ("irq_out", irq_count))
    {
      m_vci_fsm.on_read_write (on_read, on_write);

      SC_METHOD (transition);
      dont_initialize ();
      sensitive << p_clk . pos ();

      SC_METHOD (genMoore);
      dont_initialize ();
      sensitive << p_clk . neg ();
    }

    tmpl(/**/)::~VciAicu ()
    {
      delete r_mask;
      delete r_status;
      delete r_address;
      delete r_active_id;
      soclib::common::dealloc_elems (p_irq_in,
          m_irq_count * m_local_count + m_global_count);
      soclib::common::dealloc_elems (p_irq_out, m_irq_count);
    }
  }
}
