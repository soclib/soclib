/*
 * SoCLib is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; version 2.1 of the License.
 * 
 * SoCLib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with SoCLib; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include <cstring>

#include "aicu.h"
#include "register.h"
#include "arithmetics.h"
#include "alloc_elems.h"
#include "../include/vci_aicu.h"

#define tmpl(t) template<typename vci_param> t VciAicu<vci_param>

using namespace std;
using namespace soclib;
using namespace soclib::caba;

namespace soclib
{
  namespace caba
  {
    tmpl(void)::transition ()
    {
      /*
       * Deal with the reset state.
       */

      if (! p_resetn . read () || m_reset_registers)
      {
        memset (r_handlers, 0, 32 * sizeof (uint32_t));
        memset (r_mask, 0, m_irq_count * sizeof (uint32_t));
        memset (r_status, 0, m_irq_count * sizeof (uint32_t));
        memset (r_address, 0, m_irq_count * sizeof (uint32_t));
        m_reset_registers = false;
      }

      if (! p_resetn . read ())
      {
        r_control = 0;
        m_vci_fsm . reset ();
        return;
      }

      /*
       * If the component is running, propagate the
       * interrupt status.
       */

      if (r_control & 0x1)
      {
        uint32_t irq_offset = m_irq_count * m_local_count;

        for (size_t i = 0; i < m_irq_count; i += 1)
        {
          bool irq_has_been_set = false;

          for (size_t j = 0; j < m_local_count; j += 1)
          {
            /*
             * Check the local interrupts.
             */

            if (p_irq_in[j * m_irq_count + i])
            {
              r_status[i] |= 1 << j;

              if ((r_mask[i] & (1 << j)) != 0 && ! irq_has_been_set)
              {
                irq_has_been_set = true;
                r_address[i] = r_handlers[j];
                r_active_id[i] = j;
              }
            }
            else
            {
              r_status[i] &= ~(1 << j);
            }
          }

          /*
           * Check the global interrupts.
           */

          for (size_t j = 0; j < m_global_count; j += 1)
          {
            uint32_t idx = m_local_count + j;

            if (p_irq_in[irq_offset + j])
            {
              r_status[i] |= 1 << idx;

              if ((r_mask[i] & (1 << idx)) != 0 && ! irq_has_been_set)
              {
                irq_has_been_set = true;
                r_address[i] = r_handlers[idx];
                r_active_id[i] = idx;
              }
            }
            else
            {
              r_status[i] &= ~(1 << idx);
            }
          }
        }
      }

      m_vci_fsm . transition();
    }
  }
}
