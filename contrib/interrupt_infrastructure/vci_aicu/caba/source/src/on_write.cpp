/*
 * SoCLib is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; version 2.1 of the License.
 * 
 * SoCLib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with SoCLib; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include <strings.h>

#include "aicu.h"
#include "register.h"
#include "arithmetics.h"
#include "alloc_elems.h"
#include "../include/vci_aicu.h"

#define tmpl(t) template<typename vci_param> t VciAicu<vci_param>

using namespace std;
using namespace soclib;
using namespace soclib::caba;

namespace soclib
{
  namespace caba
  {
    tmpl(bool)::on_write (int seg, typename vci_param::addr_t addr,
        typename vci_param::data_t data, int be)
    {
      size_t cell = (size_t)(addr & 0xFF) / vci_param::B;
      bool is_local_register = (addr & 0xFFFFFF00) != 0;

#ifdef SOCLIB_MODULE_DEBUG
      cout << "VciAicu : write @ " << hex << addr << ", cell = " << cell << endl;
#endif

      if ( be != 0xf ) return false;

      if (! is_local_register)
      {
        if (cell == 0)
        {
          m_reset_registers = (r_control & 0x1) != (data & 0x1);
          r_control = data;
          return true;
        }
        else if (cell >= 4 && cell < 36)
        {
          r_handlers[cell - 4] = data;
          return true;
        }

        return false;
      }
      else
      {
        size_t function = cell % 4, index = cell >> 2;

#ifdef SOCLIB_MODULE_DEBUG
        cout << "VciAicu : function = " << function << ", index = " << index << endl;
#endif

        switch (function)
        {
          case AICU_STATUS :
            r_status[index] &= ~data;
            return true;

          case AICU_MASK :
            r_mask[index] = data;
            return true;
        }
      }

      return false;
    }
  }
}
