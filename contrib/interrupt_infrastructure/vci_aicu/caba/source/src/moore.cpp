/*
 * SoCLib is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; version 2.1 of the License.
 * 
 * SoCLib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with SoCLib; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include <strings.h>

#include "aicu.h"
#include "register.h"
#include "arithmetics.h"
#include "alloc_elems.h"
#include "../include/vci_aicu.h"

#define tmpl(t) template<typename vci_param> t VciAicu<vci_param>

using namespace std;
using namespace soclib;
using namespace soclib::caba;

namespace soclib
{
  namespace caba
  {
    tmpl(void)::genMoore ()
    {
      m_vci_fsm . genMoore();

      for (size_t i = 0; i < m_irq_count; i += 1)
      {
        p_irq_out[i] = (r_status[i] & r_mask[i]) != 0;
      }
    }
  }
}
