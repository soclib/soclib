/*
 * SoCLib is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; version 2.1 of the License.
 * 
 * SoCLib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with SoCLib; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include <strings.h>

#include "mailbox.h"
#include "register.h"
#include "arithmetics.h"
#include "alloc_elems.h"
#include "../include/vci_mailbox.h"

#define tmpl(t) template<typename vci_param> t VciMailbox<vci_param>

using namespace std;
using namespace soclib;
using namespace soclib::caba;

namespace soclib
{
  namespace caba
  {

    /*
     * Component constructor.
     */

    tmpl(/**/)::VciMailbox (
        sc_core::sc_module_name name,
        const MappingTable &mt,
        const IntTab &index,
        size_t irq_count
        ) : caba::BaseModule (name),
    m_vci_fsm (p_vci, mt.getSegmentList (index)),
    m_irq_count (irq_count),
    r_command (new uint32_t[irq_count]),
    r_data (new uint32_t[irq_count]),
    r_reset (new uint32_t[irq_count]),
    p_clk ("clk"),
    p_resetn ("resetn"),
    p_vci ("vci"),
    p_irq (soclib::common::alloc_elems<sc_core::sc_out<bool> >("irq", irq_count))
    {
      /*
       * Connection of the FSM hooks.
       */

      m_vci_fsm.on_read_write (on_read, on_write);

      /*
       * Connection of the SystemC methods.
       */

      SC_METHOD (transition);
      dont_initialize ();
      sensitive << p_clk . pos ();

      SC_METHOD (genMoore);
      dont_initialize ();
      sensitive << p_clk . neg ();
    }

    /*
     * Component destructor.
     */

    tmpl(/**/)::~VciMailbox ()
    {
      delete r_command;
      delete r_data;
      delete r_reset;
      soclib::common::dealloc_elems (p_irq, m_irq_count);
    }
  }
}
