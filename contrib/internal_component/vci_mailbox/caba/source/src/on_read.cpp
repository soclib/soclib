/*
 * SoCLib is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; version 2.1 of the License.
 * 
 * SoCLib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with SoCLib; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include <strings.h>

#include "mailbox.h"
#include "register.h"
#include "arithmetics.h"
#include "alloc_elems.h"
#include "../include/vci_mailbox.h"

#define tmpl(t) template<typename vci_param> t VciMailbox<vci_param>

using namespace std;
using namespace soclib;
using namespace soclib::caba;

namespace soclib
{
  namespace caba
  {

    /*
     * Component on_read hook.
     */

    tmpl(bool)::on_read (int seg, typename vci_param::addr_t addr,
        typename vci_param::data_t &data)
    {
      size_t cell = (size_t)addr / vci_param::B;
      size_t reg = cell % 4;
      size_t index = cell >> 2;

#ifdef SOCLIB_MODULE_DEBUG
      cout << "VciMailbox : read register " << reg << " @ index " << index << endl;
#endif

      switch (reg)
      {
        case MAILBOX_COMMAND :
          data = r_command[index];
          return true;

        case MAILBOX_DATA :
          data = r_data[index];
          return true;

        case MAILBOX_RESET :
          data = r_reset[index];
          return true;
      }

      return false;
    }
  }
}
