/*
 * SoCLib is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; version 2.1 of the License.
 * 
 * SoCLib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with SoCLib; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef SOCLIB_VCI_MAILBOX_H
#define SOCLIB_VCI_MAILBOX_H

#include <systemc>
#include "vci_target_fsm.h"
#include "caba_base_module.h"
#include "mapping_table.h"

namespace soclib
{
    namespace caba
    {
        template<typename vci_param> class VciMailbox : public caba::BaseModule
        {
            private:
                soclib::caba::VciTargetFsm<vci_param, true> m_vci_fsm;

                bool on_write (int seg, typename vci_param::addr_t addr,
                        typename vci_param::data_t data, int be);
                bool on_read (int seg, typename vci_param::addr_t addr,
                        typename vci_param::data_t &data);

                void transition();
                void genMoore();

                const size_t m_irq_count;

                uint32_t * r_command;
                uint32_t * r_data;
                uint32_t * r_reset;

            protected:
                SC_HAS_PROCESS(VciMailbox);

            public:
                sc_core::sc_in<bool> p_clk;
                sc_core::sc_in<bool> p_resetn;
                soclib::caba::VciTarget<vci_param> p_vci;
                sc_core::sc_out<bool> *p_irq;

                VciMailbox(
                        sc_core::sc_module_name name,
                        const soclib::common::MappingTable &mt,
                        const soclib::common::IntTab &index,
                        size_t irq_count
                        );

                ~VciMailbox ();

                soclib_static_assert(vci_param::B == 4);
        };
    }
}

#endif
